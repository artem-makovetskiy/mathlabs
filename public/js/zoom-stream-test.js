(function() {
    console.log(signature);
    console.log("checkSystemRequirements");
    console.log(JSON.stringify(ZoomMtg.checkSystemRequirements()));

    // ZoomMtg.setZoomJSLib(appUrl + "/vendor/zoomus/websdk/dist/lib", "/av");
    // ZoomMtg.setZoomJSLib("https://dmogdx0jrul3u.cloudfront.net/1.7.10/lib", "/av");
    // ZoomMtg.setZoomJSLib("https://source.zoom.us/1.7.10/lib", "/av");

    ZoomMtg.preLoadWasm();
    ZoomMtg.prepareJssdk();

    console.log(zoomConfig);

    const meetConfig = {
        apiKey: zoomConfig.api_key,
        apiSecret: zoomConfig.api_secret,
        meetingNumber: meetingNumber,
        leaveUrl: leaveUrl,
        userName: userName,
        // userEmail: zoomConfig.user_email, // required for webinar
        passWord: meetingPassword, // if required
        role: meetingRole // 1 for host; 0 for attendee or webinar
    };

    console.log(meetConfig);

    // const SIGNATURE_ENDPOINT = '';

    ZoomMtg.init({
        leaveUrl: meetConfig.leaveUrl,
        // isSupportAV: true,
        success() {
            ZoomMtg.join({
                meetingNumber: meetConfig.meetingNumber,
                userName: meetConfig.userName,
                signature: signature,
                apiKey: meetConfig.apiKey,
                passWord: meetConfig.passWord,
                success() {
                    // ZoomMtg.reRender({ lang: "ru-RU" });
                    $.i18n.reload("ru-RU");
                    ZoomMtg.showJoinAudioFunction({
                        show: true
                    });
                    // $('#nav-tool').hide();
                    console.log("join meeting success");
                },
                error(res) {
                    console.log(res);
                }
            });
        },
        error(res) {
            console.log(res);
        }
    });
})();

<div x-data="siteData()" x-on:scroll.window="atTop = (window.pageYOffset > 10) ? false : true;">
    <header class="header header--lite">
      <div class="header__container">
        <div class="header__col header__col--left"><a class="header__logo" href="{{ route('site.home') }}"></a><a class="header__btn-back" href="{{ route('site.home') }}">Вернуться на главную</a></div>
        <div class="header__col header__col--right"><a class="header__text" href="tel:+74994904467">+7 (499) 490-44-67</a>
        @guest
          <a x-on:click.prevent="openAuthModal" class="header__link" href="#" data-modal="0"><span class="header__link-text">Войти</span></a>
        @endguest
        @if(Auth::user() && Auth::user()->hasRole('client'))
          <a class="header__link" href="{{ route('account.myLessons') }}" data-modal="0"><span class="header__link-text">Кабинет</span></a>
        @endif
        @if(Auth::user() && Auth::user()->hasRole('admin'))
          <a class="header__link" href="{{ route('admin.group-lessons.index') }}" data-modal="0"><span class="header__link-text">Кабинет</span></a>
        @endif
      </div>
      </div>
    </header>
    <div class="cover-sign-up">
      <div class="cover-sign-up__container flex-col lg:flex-row items-center lg:items-start">
        <div class="cover-sign-up__col cover-sign-up__col--left p-0 w-full lg:w-1/2 lg:pr-12">
          <h1 class="cover-sign-up__title my-8 lg:my-16 text-center lg:text-left">Запишитесь и&nbsp;сразу&nbsp;<br>пройдите вводное занятие&nbsp;<br>или получите консультацию</h1>
          <ul class="cover-sign-up__list hidden lg:block">
            <li class="cover-sign-up__list-item">Стоимость вводного занятия&nbsp;&mdash;&nbsp;<b class="cover-sign-up__list-item-text-bold">0&nbsp;руб.</b></li>
            <li class="cover-sign-up__list-item">Подберем план обучения на&nbsp;основе&nbsp;<b class="cover-sign-up__list-item-text-bold">тестирования</b></li>
            <li class="cover-sign-up__list-item">Достигнем цели и&nbsp;поднимем&nbsp;<b class="cover-sign-up__list-item-text-bold">уровень знаний</b></li>
          </ul>
        </div>
        <div class="cover-sign-up__col cover-sign-up__col--right">
          <form wire:submit.prevent="register" class="cover-sign-up__form">
            <div class="cover-sign-up__form-part cover-sign-up__form-part--top">
              <div class="cover-sign-up__form-row">
                <input wire:model.lazy="parent_name" class="cover-sign-up__input-text" type="text" id="parent_name" name="parent_name" value="" placeholder="Ваше имя">
                @error('parent_name')
                <p class="mt-2 text-sm text-red-600 text-left">{{ $message }}</p>
                @enderror
              </div>
              <div class="cover-sign-up__form-row">
                <input wire:model.lazy="phone_number" class="cover-sign-up__input-text cover-sign-up__input-text--phone" type="text" id="phone_number" name="phone_number" value="" placeholder="+7 (___)___-__-__">
                @error('phone_number')
                <p class="mt-2 text-sm text-red-600 text-left">{{ $message }}</p>
                @enderror
              </div>
              <div class="cover-sign-up__form-row">
                <input wire:model.lazy="email" class="cover-sign-up__input-text"  type="email" id="email" name="email" value="" placeholder="Ваш e-mail">
                @error('email')
                <p class="mt-2 text-sm text-red-600 text-left">{{ $message }}</p>
                @enderror
              </div>
              <div class="cover-sign-up__form-row">
                <input wire:model.lazy="password" type="password" id="password" name="password" class="cover-sign-up__input-text" placeholder="Пароль">
                @error('password')
                <p class="mt-2 text-sm text-red-600 text-left">{{ $message }}</p>
                @enderror
              </div>
              <div class="cover-sign-up__form-row cover-sign-up__form-row--2-col">
                <input wire:model.lazy="child_name" class="cover-sign-up__input-text"  type="text" id="child_name" name="child_name" value="" placeholder="Имя ребенка">
                <div class="cover-sign-up__select-wrapper">
                  <select wire:model.lazy="grade" name="grade" id="grade" class="cover-sign-up__select">
                    @foreach($this->grades as $grade_key => $grade)
                        <option class="cover-sign-up__select-option" value="{{ $grade_key }}">{{ $grade }}</option>
                    @endforeach
                    <!-- <option class="cover-sign-up__select-option" disabled selected value>Класс</option> -->
                  </select>
                </div>
              </div>
              <div class="cover-sign-up__form-row">
                <button type="submit" class="cover-sign-up__btn cover-sign-up__btn--blue">Начать занятия</button>
              </div>
              <div class="cover-sign-up__form-row">
                <button x-on:click.prevent="openGetHelpModal" type="button" class="cover-sign-up__btn cover-sign-up__btn--white">Получить консультацию</button>
              </div>
              <div class="cover-sign-up__form-row">
                <div class="cover-sign-up__text">Нажимая на&nbsp;кнопку, я&nbsp;принимаю&nbsp;<a class="cover-sign-up__text-link" href="#">условия соглашения</a></div>
              </div>
            </div>
            @guest
            <div class="cover-sign-up__form-part cover-sign-up__form-part--bottom">
              <div class="cover-sign-up__text cover-sign-up__text--big">У&nbsp;вас уже есть аккаунт?</div>
              <a x-on:click.prevent="openAuthModal" class="cover-sign-up__link" href="#">Войти</a>
            </div>
            @endguest
          </form>
          <div class="mx-auto" style="max-width: 495px;">
            <ul class="cover-sign-up__list mt-8 lg:hidden">
                <li class="cover-sign-up__list-item">Стоимость вводного занятия&nbsp;&mdash;&nbsp;<b
                        class="cover-sign-up__list-item-text-bold">0&nbsp;руб.</b></li>
                <li class="cover-sign-up__list-item">Подберем план обучения на&nbsp;основе&nbsp;<b
                        class="cover-sign-up__list-item-text-bold">тестирования</b></li>
                <li class="cover-sign-up__list-item">Достигнем цели и&nbsp;поднимем&nbsp;<b
                        class="cover-sign-up__list-item-text-bold">уровень знаний</b></li>
            </ul>
          </div>
        </div>
      </div>
    </div>
    <div class="faq">
      <div class="faq__container">
        <div class="faq__title">Вопросы/Ответы</div>
      </div>
      <div class="faq__container">
        <div class="faq__item">
          <div class="faq__item-question">У вас много репетиторов, как вы контролируете качество занятий?</div>
          <div class="faq__item-answer">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi sit pretium bibendum tellus aliquam auctor sem elementum velit. Sit facilisis accumsan, mattis imperdiet lobortis. Curabitur diam sit feugiat mattis feugiat diam nunc posuere. Tellus neque, est, amet adipiscing venenatis, velit semper neque faucibus. Cursus egestas dis massa proin est. Quisque bibendum nullam metus, sed quis. Massa nunc, congue sollicitudin dictumst cras. Convallis morbi nisi, magna egestas. Varius elit augue dictumst at velit tempus, sed et mollis.</div>
        </div>
        <div class="faq__item">
          <div class="faq__item-question">Как, когда и в какое время я буду учиться?</div>
          <div class="faq__item-answer">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi sit pretium bibendum tellus aliquam auctor sem elementum velit. Sit facilisis accumsan, mattis imperdiet lobortis. Curabitur diam sit feugiat mattis feugiat diam nunc posuere. Tellus neque, est, amet adipiscing venenatis, velit semper neque faucibus. Cursus egestas dis massa proin est. Quisque bibendum nullam metus, sed quis. Massa nunc, congue sollicitudin dictumst cras. Convallis morbi nisi, magna egestas. Varius elit augue dictumst at velit tempus, sed et mollis.</div>
        </div>
        <div class="faq__item">
          <div class="faq__item-question">Чем ваши домашние задания отличаются от школьных?</div>
          <div class="faq__item-answer">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi sit pretium bibendum tellus aliquam auctor sem elementum velit. Sit facilisis accumsan, mattis imperdiet lobortis. Curabitur diam sit feugiat mattis feugiat diam nunc posuere. Tellus neque, est, amet adipiscing venenatis, velit semper neque faucibus. Cursus egestas dis massa proin est. Quisque bibendum nullam metus, sed quis. Massa nunc, congue sollicitudin dictumst cras. Convallis morbi nisi, magna egestas. Varius elit augue dictumst at velit tempus, sed et mollis.</div>
        </div>
        <div class="faq__item">
          <div class="faq__item-question">Можно поменять график занятий или сделать паузу?</div>
          <div class="faq__item-answer">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi sit pretium bibendum tellus aliquam auctor sem elementum velit. Sit facilisis accumsan, mattis imperdiet lobortis. Curabitur diam sit feugiat mattis feugiat diam nunc posuere. Tellus neque, est, amet adipiscing venenatis, velit semper neque faucibus. Cursus egestas dis massa proin est. Quisque bibendum nullam metus, sed quis. Massa nunc, congue sollicitudin dictumst cras. Convallis morbi nisi, magna egestas. Varius elit augue dictumst at velit tempus, sed et mollis.</div>
        </div>
        <div class="faq__item">
          <div class="faq__item-question">Какие репетиторы преподают в MathLabs?</div>
          <div class="faq__item-answer">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi sit pretium bibendum tellus aliquam auctor sem elementum velit. Sit facilisis accumsan, mattis imperdiet lobortis. Curabitur diam sit feugiat mattis feugiat diam nunc posuere. Tellus neque, est, amet adipiscing venenatis, velit semper neque faucibus. Cursus egestas dis massa proin est. Quisque bibendum nullam metus, sed quis. Massa nunc, congue sollicitudin dictumst cras. Convallis morbi nisi, magna egestas. Varius elit augue dictumst at velit tempus, sed et mollis.</div>
        </div>
      </div>
    </div>
    @livewire('site.feedback-form')
    <div class="bg-secondary">
        <div class="container width-adaptive mx-auto text-white py-8">
            <div class="flex flex-col sm:flex-row justify-between space-y-8 sm:space-y-0 sm:space-x-8 mb-12 md:mb-32">
                <div class="w-full sm:w-1/2 lg:w-2/5">
                    <h3 class="text-xl sm:text-2xl font-bold mb-6">Команда MathLabs</h3>
                    <div class="text-sm md:text-xl opacity-75">Мы — преподаватели и единомышленники, которые помогают в
                        изучении математики.</div>
                </div>
                <div class="w-full sm:w-1/2 lg:w-2/5">
                    <h3 class="text-xl sm:text-2xl font-bold mb-6">Если у вас есть вопросы – напишите нам!</h3>
                    <div class="footer__list-social"><a
                            class="footer__list-social-item footer__list-social-item--telegram" href="#"></a><a
                            class="footer__list-social-item footer__list-social-item--whatsapp" href="#"></a><a
                            class="footer__list-social-item footer__list-social-item--viber" href="#"></a><a
                            class="footer__list-social-item footer__list-social-item--facebook" href="#"></a></div>
                </div>
            </div>
            <div class="flex flex-col lg:flex-row justify-between space-y-8 lg:space-y-0 lg:space-x-8">
                <div class="text-base">© 2020 MathLabs. Все права защищиены</div>
                <div class="text-base"><a class="underline" href="#">Политика конфиденциальности</a></div>
                <div class="text-base"><a class="underline" href="#">Пользовательское соглашение</a></div>
            </div>
        </div>
    </div>
    <div x-show.transition.opacity="isOpenAuthModal()" class="" style="display: none;">
        <livewire:site.auth-modal/>
    </div>
    <div x-show.transition.opacity="isOpenGetHelpModal()" class="" style="display: none;">
        <livewire:site.receive-consultation-modal/>
    </div>
    <script>
        (function () {
            let faqItem = document.getElementsByClassName('faq__item');
            for (let i = 0; i < faqItem.length; i++) {
                faqItem[i].addEventListener("click", function() {
                    this.classList.toggle('faq__item--active');
                })
            }
            /* faqItem.addEventListener('click', function(event) {
                this.classList.add('faq__item--active');
            }); */
        })();
    </script>
</div>

@push('scripts')
  <script src="https://cdn.jsdelivr.net/gh/alpinejs/alpine@v2.x.x/dist/alpine.min.js" defer></script>
  <script src="{{ asset('js/utils/util.js') }}"></script>
  <script src="{{ asset('js/utils/smooth-scrolling.js') }}"></script>
  <script>
      function siteData() {
          return {
              mobileMenuOpen: false,
              authModalOpened: false,
              getHelpModalOpened: false,
              atTop: window.pageYOffset < 10,

              openAuthModal() { this.authModalOpened = true },
              closeAuthModal() { this.authModalOpened = false },
              isOpenAuthModal() { return this.authModalOpened === true },
              checkAuthModal() { console.log(this.isOpenAuthModal()) },

              openGetHelpModal() { this.getHelpModalOpened = true },
              closeGetHelpModal() { this.getHelpModalOpened = false },
              isOpenGetHelpModal() { return this.getHelpModalOpened === true },
              checkGetHelpModal() { console.log(this.isOpenGetHelpModal()) },
          }
      }

      (function() {
          
      }());
  </script>
@endpush

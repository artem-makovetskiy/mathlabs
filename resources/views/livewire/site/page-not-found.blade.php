<div>
    <section class="page-not-found">
      <div class="page-not-found__container">
        <h1 class="page-not-found__title">Ошибка 404</h1>
        <p class="page-not-found__descr">Кажется вы не туда попали, такой страницы не существует</p><a class="page-404__btn" href="/">Вернуться на главную</a>
      </div>
    </section>
</div>

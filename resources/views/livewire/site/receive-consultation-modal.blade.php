<div>
    <div class="modal-receive-consultation">
      <div @click.away="closeGetHelpModal" class="modal-receive-consultation__container">
        <div x-on:click.prevent="closeGetHelpModal" wire:click="resetForm" class="modal-receive-consultation__close"></div>
        <div class="modal-receive-consultation__inner{{ $receive_consultation_show_success_msg ? ' hidden' : '' }}">
            <div class="modal-receive-consultation__title">Заполните форму&nbsp;<br>и&nbsp;получите бесплатную&nbsp;<br>консультацию</div>
            <div class="modal-receive-consultation__descr">Мы&nbsp;свяжемся с&nbsp;вами, ответим на&nbsp;вопросы,&nbsp;<br>и&nbsp;поможем подобрать расписание&nbsp;<br>и&nbsp;подходящий вариант обучения</div>
            <form wire:submit.prevent="submitForm" class="modal-receive-consultation__form">
            <div class="modal-receive-consultation__form-row">
                <input wire:model.lazy="receive_consultation_name" class="modal-receive-consultation__input-text" type="text" value="" placeholder="Ваше имя">
                @error('receive_consultation_name')
                    <p class="mt-2 text-sm text-red-600 text-left">{{ $message }}</p>
                @enderror  
            </div>
            <div class="modal-receive-consultation__form-row modal-receive-consultation__form-row--2-col">
                <div class="">
                    <input wire:model.lazy="receive_consultation_phone_number" class="modal-receive-consultation__input-text modal-receive-consultation__input-text--phone" type="text" value="" placeholder="+7 (___)___-__-__">
                    @error('receive_consultation_phone_number')
                        <p class="mt-2 text-sm text-red-600 text-left">{{ $message }}</p>
                    @enderror
                </div>
                <div class="modal-receive-consultation__select-wrapper">
                <select wire:model.lazy="receive_consultation_grade" class="modal-receive-consultation__select">
                    @foreach($this->grades as $grade_key => $grade)
                        <option class="modal-receive-consultation__select-option" value="{{ $grade_key }}">{{ $grade }}</option>
                    @endforeach
                </select>
                </div>
            </div>
            <div class="modal-receive-consultation__form-row">
                <button class="modal-receive-consultation__btn inline-flex items-center justify-center" type="submit">
                    <svg wire:loading.class.remove="hidden" wire:target="submitForm" class="animate-spin -ml-1 mr-3 h-5 w-5 text-white hidden" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24"><circle class="opacity-25" cx="12" cy="12" r="10" stroke="currentColor" stroke-width="4"></circle> <path class="opacity-75" fill="currentColor" d="M4 12a8 8 0 018-8V0C5.373 0 0 5.373 0 12h4zm2 5.291A7.962 7.962 0 014 12H0c0 3.042 1.135 5.824 3 7.938l3-2.647z"></path></svg>
                    Оставить заявку
                </button>
            </div>
            <div class="modal-receive-consultation__form-row">
                <div class="modal-receive-consultation__text">Нажимая на&nbsp;кнопку, я&nbsp;принимаю&nbsp;<a class="modal-receive-consultation__text-link" href="#">условия соглашения</a></div>
            </div>
            </form>
        </div>
        <div class="modal-receive-consultation__success-msg stack{{ $receive_consultation_show_success_msg ? '' : ' hidden' }}">
            <h3 class="modal-receive-consultation__success-msg__title text-3xl font-bold">Спасибо за заявку</h3>
            <p class="modal-receive-consultation__success-msg__desc text-xl">В ближайшее время мы свяжемся с Вами.</p>
        </div>
      </div>
      <div class="modal-receive-consultation__overlay"></div>
    </div>
</div>

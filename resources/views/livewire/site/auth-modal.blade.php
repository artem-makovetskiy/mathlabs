<div>
    <div class="modal-auth-reg">
        <div @click.away="closeAuthModal" class="modal-auth-reg__container">
            <div class="modal-auth-reg__nav">
            <div wire:click="setActivePanel('register')" class="modal-auth-reg__nav-item{{ $active_panel === 'register' ? ' modal-auth-reg__nav-item--active' : '' }}" data-content="0">Регистрация</div>
            <div wire:click="setActivePanel('login')" class="modal-auth-reg__nav-item{{ $active_panel === 'login' ? ' modal-auth-reg__nav-item--active' : '' }}" data-content="1">Вход</div>
            </div>
            <div class="modal-auth-reg__content">
            <div class="modal-auth-reg__content-item{{ $active_panel === 'register' ? ' modal-auth-reg__content-item--active' : '' }}" data-content="0">
                <div class="modal-auth-reg__title">Регистрация</div>
                <form wire:submit.prevent="register" class="modal-auth-reg__form">
                    <div class="modal-auth-reg__form-row">
                        <input wire:model.lazy="parent_name" type="text" id="auth_modal_parent_name" name="auth_modal_parent_name" required class="modal-auth-reg__input-text" placeholder="Ваше имя">
                        @error('parent_name')
                        <p class="mt-2 text-sm text-red-600 text-left">{{ $message }}</p>
                        @enderror
                    </div>
                    <div class="modal-auth-reg__form-row">
                        <input wire:model.lazy="phone_number" type="text" id="auth_modal_phone_number" name="auth_modal_phone_number" required class="modal-auth-reg__input-text modal-auth-reg__input-text--phone" placeholder="+7 (___)___-__-__">
                        @error('phone_number')
                        <p class="mt-2 text-sm text-red-600 text-left">{{ $message }}</p>
                        @enderror
                    </div>
                    <div class="modal-auth-reg__form-row">
                        <input wire:model.lazy="email" type="email" id="auth_modal_email" name="auth_modal_email" required class="modal-auth-reg__input-text" placeholder="Ваш e-mail">
                        @error('email')
                        <p class="mt-2 text-sm text-red-600 text-left">{{ $message }}</p>
                        @enderror
                    </div>
                    <div class="modal-auth-reg__form-row">
                        <input wire:model.lazy="password" type="password" id="auth_modal_password" name="auth_modal_password" required class="modal-auth-reg__input-text" placeholder="Пароль">
                        @error('password')
                        <p class="mt-2 text-sm text-red-600 text-left">{{ $message }}</p>
                        @enderror
                    </div>
                    <div class="modal-auth-reg__form-row modal-auth-reg__form-row--2-col">
                        <input wire:model.lazy="child_name" type="text" id="auth_modal_child_name" name="auth_modal_child_name" required class="modal-auth-reg__input-text" placeholder="Имя ребенка">
                        <div class="modal-auth-reg__select-wrapper">
                            <select wire:model.lazy="grade" required name="auth_modal_grade" id="auth_modal_grade" class="modal-auth-reg__select">
                                @foreach($this->grades as $grade_key => $grade)
                                    <option class="modal-auth-reg__select-option" value="{{ $grade_key }}">{{ $grade }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="modal-auth-reg__form-row">
                        <button type="submit" class="modal-auth-reg__btn inline-flex items-center justify-center">
                            <svg wire:loading.class.remove="hidden" wire:target="register" class="animate-spin -ml-1 mr-3 h-5 w-5 text-white hidden" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24"><circle class="opacity-25" cx="12" cy="12" r="10" stroke="currentColor" stroke-width="4"></circle> <path class="opacity-75" fill="currentColor" d="M4 12a8 8 0 018-8V0C5.373 0 0 5.373 0 12h4zm2 5.291A7.962 7.962 0 014 12H0c0 3.042 1.135 5.824 3 7.938l3-2.647z"></path></svg>
                            Зарегистрироваться
                        </button>
                    </div>
                    <div class="modal-auth-reg__form-row">
                        <div class="modal-auth-reg__text">
                            Нажимая на&nbsp;кнопку, я&nbsp;принимаю&nbsp;
                            <a href="#" class="modal-auth-reg__text-link">условия соглашения</a>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-auth-reg__content-item{{ $active_panel === 'login' ? ' modal-auth-reg__content-item--active' : '' }}" data-content="1">
                <div class="modal-auth-reg__title">Вход в&nbsp;кабинет&nbsp;<br>ученика</div>
                <form wire:submit.prevent="authenticate" class="modal-auth-reg__form">
                    <div class="modal-auth-reg__form-row">
                        <input wire:model.lazy="login_email" type="email" id="login_email" name="login_email" class="modal-auth-reg__input-text" placeholder="E-mail или телефон" autocomplete="email">
                        @error('login_email')
                        <p class="mt-2 text-sm text-red-600 text-left">{{ $message }}</p>
                        @enderror
                    </div>
                    <div class="modal-auth-reg__form-row">
                        <input wire:model.lazy="login_password" type="password" id="login_password" name="login_password" class="modal-auth-reg__input-text" placeholder="Пароль">
                        @error('login_password')
                        <p class="mt-2 text-sm text-red-600 text-left">{{ $message }}</p>
                        @enderror
                    </div>
                    <div class="modal-auth-reg__form-row">
                        <button type="submit" class="modal-auth-reg__btn inline-flex items-center justify-center transition ease-in-out duration-150">
                            <svg wire:loading.class.remove="hidden" wire:target="authenticate" class="animate-spin -ml-1 mr-3 h-5 w-5 text-white hidden" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24"><circle class="opacity-25" cx="12" cy="12" r="10" stroke="currentColor" stroke-width="4"></circle> <path class="opacity-75" fill="currentColor" d="M4 12a8 8 0 018-8V0C5.373 0 0 5.373 0 12h4zm2 5.291A7.962 7.962 0 014 12H0c0 3.042 1.135 5.824 3 7.938l3-2.647z"></path></svg>
                            Войти в&nbsp;MathLabs
                        </button>
                    </div>
                    <div class="modal-auth-reg__form-row">
                        <a href="{{ route('password.request') }}" class="modal-auth-reg__link">Забыли пароль?</a>
                    </div>
                </form>
            </div>
            </div>
        </div>
        <div class="modal-auth-reg__overlay"></div>
    </div>
</div>
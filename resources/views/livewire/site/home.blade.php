<div>
    <section class="intro">
        <div class="container width-adaptive max-w-6xl mx-auto py-8 sm:py-20">
            <div class="stack stack-lg flex-col items-center text-center">
                <h1 class="font-bold text-2xl sm:text-3xl md:text-4xl lg:text-6xl leading-tight">Вы здесь, потому что
                    хотите, чтобы Ваш ребенок знал математику. <div class="text-primary">Этого же хотим и мы.</div>
                </h1>
                <div class="text-base sm:text-lg lg:text-2xl">Онлайн-обучение математике от призеров олимпиад МГУ и
                    Физтеха</div>
                <form wire:submit.prevent="submitIntroForm" class="intro__form">
                    <input wire:model.lazy="intro_form_phone_number" type="text" class="intro__form__control form-control-default sm:flex-auto"
                        name="intro_form_phone_number" placeholder="Введите ваш телефон">
                    <input wire:model.lazy="intro_form_grade" type="number" min="1" max="11" class="intro__form__control form-control-default sm:flex-auto w-40 mt-4 sm:ml-4 sm:mt-0"
                        name="intro_form_grade" placeholder="Класс ученика">
                    <button type="submit" class="btn-primary-lg inline-flex items-center justify-center xs:w-auto mt-4 sm:mt-0 sm:ml-6 transition ease-in-out duration-150">
                        <svg wire:loading.class.remove="hidden" wire:target="submitIntroForm" class="animate-spin -ml-1 mr-3 h-5 w-5 text-white hidden" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24"><circle class="opacity-25" cx="12" cy="12" r="10" stroke="currentColor" stroke-width="4"></circle> <path class="opacity-75" fill="currentColor" d="M4 12a8 8 0 018-8V0C5.373 0 0 5.373 0 12h4zm2 5.291A7.962 7.962 0 014 12H0c0 3.042 1.135 5.824 3 7.938l3-2.647z"></path></svg>
                        Записаться
                    </button>
                    <!-- <a class="btn-primary-lg xs:w-auto mt-4 sm:mt-0 sm:ml-6"
                        href="{{ route('site.sign-up-online') }}">Записаться</a> -->
                </form>
                @error('intro_form_phone_number')
                    <p class="mt-2 text-sm text-red-600 text-left">{{ $message }}</p>
                @enderror
                @error('intro_form_grade')
                    <p class="mt-2 text-sm text-red-600 text-left">{{ $message }}</p>
                @enderror
                @if($intro_form_show_success_msg)
                    <p class="mt-4 text-lg text-green-500 text-left">Спасибо за заявку! В ближайшее время мы свяжемся с Вами.</p>
                @endif
            </div>
        </div>
    </section>
    <section class="features">
        <div class="container width-adaptive mx-auto py-16">
            {{-- <div class="grid gap-6 grid-cols-1 md:grid-cols-2 xl:grid-cols-4"> --}}
            <div class="features-grid">
                <div class="border-4 border-gray-100 rounded-lg px-8 py-6">
                    <div class="mb-6"><svg width="30" height="32" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M29.562 24.727L15.016 0 .47 24.727 15.016 32l14.546-7.273zM16.292 7.198l6.786 11.538-6.787 3.397V7.198zm0 21.313v-3.526l8.083-4.044 1.604 2.726-9.688 4.843zM13.74 7.198V28.51l-9.686-4.844L13.74 7.198z" fill="#644BEC"/></svg></div>
                    <div class="text-lg mb-6">Бесплатно проводим первое тестирование, чтобы определить уровень ученика</div>
                </div>
                <div class="border-4 border-gray-100 rounded-lg px-8 py-6">
                    <div class="mb-6"><svg width="21" height="32" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M21 25.333V6.667C21 2.929 16.388 0 10.5 0S0 2.929 0 6.667v18.666C0 29.071 4.612 32 10.5 32S21 29.071 21 25.333zm-2.625-4.443c-1.91-1.369-4.703-2.223-7.875-2.223-3.172 0-5.965.854-7.875 2.223v-9.78c1.91 1.369 4.703 2.223 7.875 2.223 3.172 0 5.965-.854 7.875-2.223v9.78zM10.5 2.667c4.64 0 7.875 2.108 7.875 4s-3.234 4-7.875 4c-4.64 0-7.875-2.108-7.875-4s3.234-4 7.875-4zm0 26.666c-4.64 0-7.875-2.108-7.875-4s3.234-4 7.875-4c4.64 0 7.875 2.108 7.875 4s-3.234 4-7.875 4z" fill="#644BEC"/></svg></div>
                    <div class="text-lg mb-6">Выясняем, на какие темы стоит сделать упор в подготовке. Чаще всего - на все :)</div>
                </div>
                <div class="border-4 border-gray-100 rounded-lg px-8 py-6">
                    <div class="mb-6"><svg width="32" height="32" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M16 0C7.177 0 0 7.178 0 16s7.177 16 16 16 16-7.178 16-16S24.823 0 16 0zm0 2.667c5.485 0 10.204 3.331 12.249 8.075C25.36 9.044 21.022 8 16 8c-5.021 0-9.36 1.044-12.249 2.742C5.796 5.998 10.515 2.667 16 2.667zm0 18.666c-7.631 0-13.333-2.816-13.333-5.333 0-2.517 5.702-5.333 13.333-5.333S29.333 13.483 29.333 16c0 2.517-5.702 5.333-13.333 5.333zm0 8c-5.485 0-10.204-3.331-12.249-8.075C6.64 22.956 10.98 24 16 24c5.021 0 9.36-1.044 12.249-2.742-2.045 4.744-6.764 8.075-12.249 8.075z" fill="#644BEC"/></svg></div>
                    <div class="text-lg mb-6">Проводим занятия так, чтобы учащийся постоянно был мотивирован к росту</div>
                </div>
                <div class="border-4 border-gray-100 rounded-lg px-8 py-6">
                    <div class="mb-6"><svg width="32" height="32" fill="none" xmlns="http://www.w3.org/2000/svg"><g clip-path="url(#clip0)" fill="#644BEC"><path d="M17.333 0h-2.666v14.667H0v2.666h14.667V32h2.666V17.333H32v-2.666H17.333V0z"/><path d="M12 0H9.333c0 5.147-4.186 9.333-9.333 9.333V12c6.617 0 12-5.383 12-12zM20 32h2.667c0-5.147 4.186-9.333 9.333-9.333V20c-6.616 0-12 5.384-12 12z"/></g><defs><clipPath id="clip0"><path fill="#fff" d="M0 0h32v32H0z"/></clipPath></defs></svg></div>
                    <div class="text-lg mb-6">Точно знаем, как сдавать экзамены, ведь мы сами сдали их на 100 баллов</div>
                </div>
            </div>
        </div>
    </section>
    <section class="simple-test relative">
        <div class="container width-adaptive mx-auto py-16">
            <h2 class="section-heading">Давайте сразу с интересной задачи!</h2>
            {{-- <div class="grid grid-cols-12 gap-8">
                <div class="col-start-3 md:col-start-4 col-span-3 md:col-span-2 font-bold text-2xl text-right">Дано:</div>
                <div class="col-span-6 md:col-span-4 text-lg">В стране Кидцуния любые два города соединены дорогой с односторонним движением.</div>
                <div class="col-start-3 md:col-start-4 col-span-3 md:col-span-2 font-bold text-2xl text-right">Доказать:</div>
                <div class="col-span-6 md:col-span-4 text-lg">Докажите, что можно проехать по всем городам, побывав в каждом по одному разу.</div>
            </div> --}}
            {{-- <div class="grid grid-cols-4 gap-8 max-w-2xl mx-auto">
                <div class="col-span-2 font-bold text-2xl text-right">Дано:</div>
                <div class="col-span-2 text-lg">В стране Кидцуния любые два города соединены дорогой с односторонним движением.</div>
                <div class="col-span-2 font-bold text-2xl text-right">Доказать:</div>
                <div class="col-span-2 text-lg">Докажите, что можно проехать по всем городам, побывав в каждом по одному разу.</div>
            </div> --}}
            <div class="max-w-2xl mx-auto">
                <table class="table w-full">
                    <tr>
                        <td class="block sm:table-cell font-bold text-2xl sm:text-right md:pl-24 px-4 sm:py-6 align-top">Дано:</td>
                        <td class="block sm:table-cell text-lg px-4 py-6" style="">В стране Кидцуния любые два города соединены дорогой с односторонним движением.</td>
                    </tr>
                    <tr>
                        <td class="block sm:table-cell font-bold text-2xl sm:text-right md:pl-24 px-4 sm:py-6 align-top">Доказать:</td>
                        <td class="block sm:table-cell text-lg px-4 p-6">Докажите, что можно проехать по всем городам, побывав в каждом по одному разу.</td>
                    </tr>
                </table>
            </div>
        </div>
        <div class="simple-test__cover-left"></div>
        <div class="simple-test__cover-right"></div>
        <div class="simple-test__cover-line">
            <svg width="100%" height="29" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M2 2l34.345 25h826.29L899 2" stroke="#B0C1FF" stroke-width="4" stroke-dasharray="10 10"/></svg>
        </div>
    </section>
    <div class="container width-adaptive mx-auto pt-8 sm:pt-16 lg:pt-28 pb-8 sm:pb-12">
        <div class="flex flex-col lg:flex-row justify-center items-center space-x-0 lg:space-x-12 space-y-8 lg:space-y-0 bg-blue-light rounded-lg p-6 md:p-12 lg:p-16">
            <div class="text-xl md:text-2xl lg:text-3xl font-bold text-center lg:text-left">Попробуйте бесплатное вводное онлайн-занятие</div>
            <button x-on:click.prevent="openGetHelpModal" type="button" class="btn-primary-lg text-base md:text-xl w-auto">Оставить заявку</button>
        </div>
    </div>
    <section class="classes" id="online-classes">
        <div class="container width-adaptive mx-auto py-8 sm:py-16 lg:py-24">
            <h2 class="section-heading">Все, что нужно для успешной подготовки к олимпиадам и сдаче экзаменов</h2>
            <div class="grid grid-cols-2 gap-4" style="grid-auto-rows: 1fr;">
                <div class="col-span-2">
                    <div class="rounded-lg px-8 py-6 bg-secondary h-full relative overflow-hidden">
                        <div class="stack stack-lg w-full md:w-3/5 lg:w-2/5 text-white h-full">
                            <h4 class="text-lg sm:text-2xl md:text-3xl font-bold">Олимпиадная математика</h4>
                            <div class="text-sm sm:text-xl opacity-75 mb-8">Научим решать задачи, которые встречаются на олимпиадах. Разберем основные методы и подходы к решению задач</div>
                            <div class="mt-auto">
                                <a href="{{ route('site.sign-up-online') }}" class="btn-white-lg w-auto relative z-10">Записаться</a>
                            </div>
                        </div>
                        <div class="absolute top-0 right-0 w-1/3 lg:w-1/2"><svg width="710" height="415" fill="none" xmlns="http://www.w3.org/2000/svg"><path opacity=".2" d="M7 53.885l102.824 290.569M7 53.884l78 48.501m-78-48.5L224.529-25M109.824 344.454L186.5 282.6m-76.676 61.854L85 102.385M186.5 282.6l83-62.715m-83 62.715L85 102.385m184.5 117.5l-44.971 171L489.882 282.6M269.5 219.885L85 102.385m184.5 117.5L489.882 282.6M269.5 219.885l220.382-90m-220.382 90L224.529-25m265.353 307.6L611.471 31.84M489.882 282.6L717 371.202 611.471 31.839M489.882 282.6V129.885m121.589-98.046l-121.589 98.046m121.589-98.046L429-25H224.529M85 102.385L224.529-25m265.353 154.885L224.529-25" stroke="#fff" stroke-width="10"/></svg></div>
                    </div>
                </div>
                <div class="col-span-2">
                    <div class="rounded-lg px-8 py-6 bg-primary h-full relative overflow-hidden">
                        <div class="stack stack-lg w-full md:w-3/5 lg:w-2/5 text-white h-full">
                            <h4 class="text-lg sm:text-2xl md:text-3xl font-bold">Школьная программа</h4>
                            <div class="text-sm sm:text-xl opacity-75 mb-8">Поможем разобраться с отдельными темами по математике, решим множество задач.</div>
                            <div class="mt-auto">
                                <a href="{{ route('site.sign-up-online') }}" class="btn-white-lg w-auto relative z-10">Записаться</a>
                            </div>
                        </div>
                        <div class="absolute top-0 right-0 w-1/3 lg:w-1/2">
                            <svg width="100%" height="400" fill="none" xmlns="http://www.w3.org/2000/svg"><path opacity=".2" d="M1078.07 431.559c-272.268 65.519-555.062-186.238-298.026-236.998 74.113-14.636 121.792-13.183 153.043-3.11m0 0c56.821 18.315 59.331 65.13 67.663 95.553m-67.663-95.553c26.444 31.767 49.437 65.149 67.663 95.553m-67.663-95.553C842.132 82.185 710.362-7.964 591.286 107.967 428.08 266.865 465.077 89.751 503.977-18.668L279 334.268m721.75-47.264c2.98 10.876 6.71 19.657 13.92 24.293a687.757 687.757 0 00-13.92-24.293zM6.81 342.5C-23.493 65.067 430.168-231.029 355.489 19.693m0 0a12.71 12.71 0 01-.055.183c-51.95 174.001-180.221 225.987-267.326 249.008m267.381-249.19c-81.749-.495-200.396 80.039-267.381 249.19m267.381-249.19c11.601.07 22.459 1.771 32.321 5.084M88.108 268.884c-38.571 10.193-69.071 14.707-81.298 21.621 22.548-.327 48.075-1.894 75-5.032m6.298-16.589a626.561 626.561 0 00-6.297 16.589m0 0C233.192 267.83 428.796 200.528 387.81 24.778m-306 260.695c-6.681 18.378-12.76 37.738-18.135 58.088M387.81 24.778a251.283 251.283 0 00-3.701-13.996C319.649-207.69 459.076-92.373 536.846-7.405L329.54-371m58.27 395.778c47.12 15.824 71.499 68.406 45.532 155.378-74.955 251.049-181.779 295.36-221.618 317.887C364.348 495.831 560.96 376.971 496.5 158.5c-64.459-218.471 167.49-43.334 245.26 41.634l-48.865-355.696" stroke="#fff" stroke-width="10"/></svg>
                        </div>
                    </div>
                </div>
                <div class="col-span-2 md:col-span-1">
                    <div class="relative border-4 border-gray-100 rounded-lg px-8 py-6 h-full overflow-hidden">
                        <div class="stack stack-lg w-full lg:w-4/5 h-full">
                            <h4 class="text-lg sm:text-2xl md:text-3xl font-bold">ЕГЭ по математике</h4>
                            <div class="text-sm sm:text-xl opacity-75 mb-8">Подготовим к сдаче ЕГЭ по математике. Сделаем упор на решение типовых сдач из профильной математики.</div>
                            <div class="mt-auto">
                                <a href="{{ route('site.sign-up-online') }}" class="btn-primary-lg w-auto relative z-10">Записаться</a>
                            </div>
                        </div>
                        <div class="absolute right-0 bottom-0 z-0"><svg width="165" height="169" fill="none" xmlns="http://www.w3.org/2000/svg"><g opacity=".4" fill="#B0C1FF"><path d="M26.985 127.756l-10.541-5.903 59.03-105.41 10.54 5.904 5.903-10.541L70.835 0 0 126.491l21.082 11.806 5.903-10.541zM170.342 69.57l10.541 5.903-59.03 105.41-10.541-5.903-5.903 10.541 21.082 11.806 70.836-126.492-21.082-11.806-5.903 10.541zM43.429 123.118l63.245 35.418 11.806-21.082-10.541-5.903-5.903 10.541-38.3-21.449 44.467-16.637-9.05-46.608 38.301 21.448-5.903 10.541 10.541 5.903 11.806-21.082-63.246-35.417-5.507 9.834 9.249 47.647-45.458 17.012-5.507 9.834z"/></g></svg></div>
                    </div>
                </div>
                <div class="col-span-2 md:col-span-1">
                    <div class="relative border-4 border-gray-100 rounded-lg px-8 py-6 h-full">
                        <div class="stack stack-lg w-full lg:w-4/5 h-full">
                            <h4 class="text-lg sm:text-2xl md:text-3xl font-bold">ОГЭ по математике</h4>
                            <div class="text-sm sm:text-xl opacity-75 mb-8">Интенсивная подготовка к успешной сдаче финальных экзаменов по математике за 9-й класс.</div>
                            <div class="mt-auto">
                                <a href="{{ route('site.sign-up-online') }}" class="btn-primary-lg w-auto relative z-10">Записаться</a>
                            </div>
                        </div>
                        <div class="absolute right-0 bottom-0 z-0"><svg width="145" height="154" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M27.054 135.4l23.235-53.838a63.152 63.152 0 0022.942 3.077l9.795 68.95 11.443-1.625-6.536-46.007c14.305-3.115 26.292-9.683 33.016-13.98l22.212 26.931 8.919-7.355-37.141-45.032.01-.007-37.394-45.318-9.061-10.988-5.643 13.077-23.293 53.94.012.003-23.13 53.592 10.614 4.58zm51.61-94.694l19.818 24.03c-4.715 3.089-9.97 5.347-15.448 6.738l-4.37-30.768zM54.877 70.93L67.22 42.33l4.372 30.772c-5.65.187-11.325-.522-16.714-2.173zm31.424 23.547l-1.628-11.464a63.232 63.232 0 0021.176-9.344l7.677 9.308c-5.854 3.605-15.747 8.845-27.225 11.5z" fill="#B0C1FF" opacity=".4"/></svg></div>
                    </div>
                </div>
                <div class="col-span-2 md:col-span-1">
                    <div class="relative border-4 border-gray-100 rounded-lg px-8 py-6 h-full">
                        <div class="stack stack-lg w-full lg:w-4/5 h-full">
                            <h4 class="text-lg sm:text-2xl md:text-3xl font-bold">Математические школы</h4>
                            <div class="text-sm sm:text-xl opacity-75 mb-8">Занятия для подготовки в ведущие математические школы с разборами и заданий, предлагавшихся на вступительных экзаменах.</div>
                            <div class="mt-auto">
                                <a href="{{ route('site.sign-up-online') }}" class="btn-primary-lg w-auto relative z-10">Записаться</a>
                            </div>
                        </div>
                        <div class="absolute right-0 bottom-0 z-0"><svg width="187" height="147" fill="none" xmlns="http://www.w3.org/2000/svg"><g opacity=".4" fill="#B0C1FF"><path d="M124.184 43.687l5.365-10.734c-15.037-7.518-33.263-3.21-43.347 10.24L63.358 73.65 49.053 66.5l-5.366 10.733 12.354 6.177-21.536 28.713a22.74 22.74 0 01-28.381 6.703L.758 129.559c15.037 7.518 33.263 3.21 43.347-10.24L66.95 88.862l11.622 5.81 5.366-10.734-9.671-4.835L95.802 50.39a22.742 22.742 0 0128.382-6.703zM118.818 67.837l-2.683-5.368c-.62.311-15.337 7.87-26.831 30.86-11.494 22.991-8.709 39.3-8.585 39.982l11.805-2.149c-.022-.124-2.056-13.324 7.513-32.467 9.568-19.137 21.387-25.45 21.506-25.515l-2.725-5.343zM158.532 91.716c.024.125 2.059 13.325-7.511 32.467-9.567 19.138-21.387 25.451-21.506 25.516l2.725 5.343 2.683 5.367c.626-.311 15.338-7.869 26.831-30.86 11.494-22.99 8.709-39.299 8.585-39.982l-11.807 2.149zM138.867 97.39l-10.789 5.597-1.999-11.99-11.836 1.977 2.637 15.818-14.236 7.384 5.522 10.654 10.79-5.597 1.998 11.99 11.836-1.977-2.637-15.818 14.236-7.383-5.522-10.654z"/></g></svg></div>
                    </div>
                </div>
                <div class="col-span-2 md:col-span-1">
                    <div class="relative border-4 border-gray-100 rounded-lg px-8 py-6 h-full">
                        <div class="stack stack-lg w-full lg:w-4/5 h-full">
                            <h4 class="text-lg sm:text-2xl md:text-3xl font-bold">Международные экзамены</h4>
                            <div class="text-sm sm:text-xl opacity-75 mb-8">Готовитесь к сдаче GRE или математической части GMAT? Сможем улучшить баллы на сдаче этих экзаменов!</div>
                            <div class="mt-auto">
                                <a href="{{ route('site.sign-up-online') }}" class="btn-primary-lg w-auto relative z-10">Записаться</a>
                            </div>
                        </div>
                        <div class="absolute right-0 bottom-0 z-0"><svg width="148" height="149" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M70.563 14.437c-38.91 7.892-64.142 45.97-56.25 84.876 7.892 38.906 45.966 64.142 84.876 56.25 38.909-7.892 64.142-45.969 56.25-84.876-7.892-38.906-45.967-64.142-84.876-56.25zm2.385 11.76c24.192-4.906 47.983 5.564 61.243 24.656-14.258-4.904-34.326-5.627-56.472-1.135-22.145 4.492-40.345 12.977-51.566 23.05 4.774-22.75 22.604-41.664 46.795-46.57zm16.699 82.324c-33.655 6.827-61.322-.492-63.574-11.594C23.822 85.826 46.45 68.305 80.105 61.48c33.655-6.826 61.322.492 63.573 11.594 2.252 11.101-20.376 28.621-54.031 35.448zm7.156 35.282c-24.191 4.906-47.982-5.564-61.242-24.656 14.258 4.904 34.326 5.627 56.471 1.135 22.146-4.492 40.346-12.978 51.567-23.05-4.774 22.75-22.604 41.664-46.796 46.571z" fill="#E0E6FF"/></svg></div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="quote bg-primary relative">
        <div class="flex">
            <div class="w-full sm:w-2/5 lg:w-1/2 relative pl-12 md:pl-20 py-16 pr-12 ml-10% xl:ml-15% 2xl:ml-25% text-white">
                <div class="text-lg md:text-xl lg:text-3xl font-bold">Большинство людей настолько боятся самого названия математики, что они готовы совершенно искренне преувеличивать свою неспособность к математике</div>
                <div class="flex space-x-4 mt-6">
                    <div class="border-t-2 border-white opacity-25 mt-3 w-16"></div>
                    <div class="">
                        <div class="font-bold text-lg mb-2">Годфри Харди</div>
                        <div class="">Английский математик</div>
                    </div>
                </div>
                <div class="hidden md:block absolute top-12 left-0"><svg width="52" height="37" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M12.488 14.8v.87c2.973.29 5.575 1.452 7.804 3.483 2.23 1.886 3.345 4.28 3.345 7.182 0 2.757-1.04 5.224-3.122 7.4C18.435 35.912 15.61 37 12.042 37c-3.42 0-6.319-1.523-8.697-4.57C1.115 29.381 0 25.61 0 21.111c0-4.498 2.007-9.141 6.02-13.93C10.184 2.394 14.347 0 18.51 0c1.337 0 2.006.218 2.006.653 0 .435-.297.653-.892.653-.594 0-1.71 1.088-3.345 3.265-2.527 3.192-3.79 6.602-3.79 10.229zm28.362 0v.87c2.974.29 5.575 1.452 7.805 3.483 2.23 1.886 3.345 4.28 3.345 7.182 0 2.757-1.04 5.224-3.122 7.4C46.946 35.912 44.195 37 40.628 37c-3.42 0-6.319-1.523-8.697-4.57-2.379-3.048-3.568-6.82-3.568-11.318 0-4.498 2.08-9.141 6.243-13.93C38.77 2.394 42.932 0 47.094 0c1.19 0 1.784.218 1.784.653 0 .435-.149.653-.446.653-.892 0-2.156 1.088-3.79 3.265-2.528 3.337-3.792 6.747-3.792 10.229z" fill="#9481FB"/></svg></div>
                <div class="block md:hidden absolute top-12 left-0"><svg width="27" height="19" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M6.484 7.6v.447a6.899 6.899 0 014.053 1.788c1.157.969 1.736 2.198 1.736 3.688 0 1.416-.54 2.683-1.62 3.8C9.571 18.441 8.104 19 6.252 19c-1.776 0-3.281-.782-4.516-2.347C.579 15.088 0 13.15 0 10.84c0-2.31 1.042-4.694 3.126-7.153C5.288 1.23 7.45 0 9.61 0c.695 0 1.042.112 1.042.335 0 .224-.154.336-.463.336-.309 0-.888.558-1.737 1.676C7.14 3.987 6.484 5.737 6.484 7.6zm14.727 0v.447a6.898 6.898 0 014.052 1.788c1.158.969 1.737 2.198 1.737 3.688 0 1.416-.54 2.683-1.621 3.8C24.375 18.441 22.948 19 21.095 19c-1.775 0-3.28-.782-4.516-2.347-1.235-1.565-1.852-3.502-1.852-5.812 0-2.31 1.08-4.694 3.242-7.153C20.13 1.23 22.29 0 24.453 0c.617 0 .926.112.926.335 0 .224-.077.336-.232.336-.463 0-1.119.558-1.968 1.676-1.312 1.714-1.968 3.465-1.968 5.253z" fill="#9481FB"/></svg></div>
            </div>
            <div class="hidden sm:block w-3/5 lg:w-1/2">
                <div class="media-wrapper h-full">
                    <img src="{{ asset('/img/home/quote-img.png') }}" alt="">
                </div>
            </div>
        </div>
    </section>

    {{-- <section class="quote bg-primary relative">
        <div class="flex">
            <div class="w-full sm:w-2/5 lg:w-1/2 relative pl-12 md:pl-20 py-16 ml-5% md:ml-10% lg:ml-15% text-white" style="padding-right: 5%;">
                <div class="text-lg md:text-xl lg:text-3xl font-bold">Большинство людей настолько боятся самого названия математики, что они готовы совершенно искренне преувеличивать свою неспособность к математике</div>
                <div class="flex space-x-4 mt-6">
                    <div class="border-t-2 border-white opacity-25 mt-3 w-16"></div>
                    <div class="">
                        <div class="font-bold text-lg mb-2">Годфри Харди</div>
                        <div class="">Английский математик</div>
                    </div>
                </div>
                <div class="hidden md:block absolute top-12 left-0"><svg width="52" height="37" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M12.488 14.8v.87c2.973.29 5.575 1.452 7.804 3.483 2.23 1.886 3.345 4.28 3.345 7.182 0 2.757-1.04 5.224-3.122 7.4C18.435 35.912 15.61 37 12.042 37c-3.42 0-6.319-1.523-8.697-4.57C1.115 29.381 0 25.61 0 21.111c0-4.498 2.007-9.141 6.02-13.93C10.184 2.394 14.347 0 18.51 0c1.337 0 2.006.218 2.006.653 0 .435-.297.653-.892.653-.594 0-1.71 1.088-3.345 3.265-2.527 3.192-3.79 6.602-3.79 10.229zm28.362 0v.87c2.974.29 5.575 1.452 7.805 3.483 2.23 1.886 3.345 4.28 3.345 7.182 0 2.757-1.04 5.224-3.122 7.4C46.946 35.912 44.195 37 40.628 37c-3.42 0-6.319-1.523-8.697-4.57-2.379-3.048-3.568-6.82-3.568-11.318 0-4.498 2.08-9.141 6.243-13.93C38.77 2.394 42.932 0 47.094 0c1.19 0 1.784.218 1.784.653 0 .435-.149.653-.446.653-.892 0-2.156 1.088-3.79 3.265-2.528 3.337-3.792 6.747-3.792 10.229z" fill="#9481FB"/></svg></div>
                <div class="block md:hidden absolute top-12 left-0"><svg width="27" height="19" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M6.484 7.6v.447a6.899 6.899 0 014.053 1.788c1.157.969 1.736 2.198 1.736 3.688 0 1.416-.54 2.683-1.62 3.8C9.571 18.441 8.104 19 6.252 19c-1.776 0-3.281-.782-4.516-2.347C.579 15.088 0 13.15 0 10.84c0-2.31 1.042-4.694 3.126-7.153C5.288 1.23 7.45 0 9.61 0c.695 0 1.042.112 1.042.335 0 .224-.154.336-.463.336-.309 0-.888.558-1.737 1.676C7.14 3.987 6.484 5.737 6.484 7.6zm14.727 0v.447a6.898 6.898 0 014.052 1.788c1.158.969 1.737 2.198 1.737 3.688 0 1.416-.54 2.683-1.621 3.8C24.375 18.441 22.948 19 21.095 19c-1.775 0-3.28-.782-4.516-2.347-1.235-1.565-1.852-3.502-1.852-5.812 0-2.31 1.08-4.694 3.242-7.153C20.13 1.23 22.29 0 24.453 0c.617 0 .926.112.926.335 0 .224-.077.336-.232.336-.463 0-1.119.558-1.968 1.676-1.312 1.714-1.968 3.465-1.968 5.253z" fill="#9481FB"/></svg></div>
            </div>
            <div class="hidden sm:block w-3/5 lg:w-1/2">
                <div class="media-wrapper h-full">
                    <img src="{{ asset('/img/home/quote-img.png') }}" alt="">
                </div>
            </div>
        </div>
    </section> --}}

    {{-- <section class="quote bg-primary relative">
        <div class="flex">
            <div class="w-1/2"></div>
            <div class="w-1/2">
                <div class="media-wrapper">
                    <img src="{{ asset('/img/home/quote-img.png') }}" alt="">
                </div>
            </div>
        </div>
        <div class="w-full h-full absolute top-0 left-0">
            <div class="container width-adaptive mx-auto relative py-16">
                <div class="flex">
                    <div class="w-1/2 pr-48 pl-20 text-white">
                        <div class="text-xl lg:text-3xl font-bold">Большинство людей настолько боятся самого названия математики, что они готовы совершенно искренне преувеличивать свою неспособность к математике</div>
                        <div class="flex space-x-4 mt-6">
                            <div class="border-t-2 border-white opacity-25 mt-3 w-16"></div>
                            <div class="">
                                <div class="font-bold text-lg mb-2">Годфри Харди</div>
                                <div class="">Английский математик</div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="absolute top-12 left-0"><svg width="52" height="37" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M12.488 14.8v.87c2.973.29 5.575 1.452 7.804 3.483 2.23 1.886 3.345 4.28 3.345 7.182 0 2.757-1.04 5.224-3.122 7.4C18.435 35.912 15.61 37 12.042 37c-3.42 0-6.319-1.523-8.697-4.57C1.115 29.381 0 25.61 0 21.111c0-4.498 2.007-9.141 6.02-13.93C10.184 2.394 14.347 0 18.51 0c1.337 0 2.006.218 2.006.653 0 .435-.297.653-.892.653-.594 0-1.71 1.088-3.345 3.265-2.527 3.192-3.79 6.602-3.79 10.229zm28.362 0v.87c2.974.29 5.575 1.452 7.805 3.483 2.23 1.886 3.345 4.28 3.345 7.182 0 2.757-1.04 5.224-3.122 7.4C46.946 35.912 44.195 37 40.628 37c-3.42 0-6.319-1.523-8.697-4.57-2.379-3.048-3.568-6.82-3.568-11.318 0-4.498 2.08-9.141 6.243-13.93C38.77 2.394 42.932 0 47.094 0c1.19 0 1.784.218 1.784.653 0 .435-.149.653-.446.653-.892 0-2.156 1.088-3.79 3.265-2.528 3.337-3.792 6.747-3.792 10.229z" fill="#9481FB"/></svg></div>
            </div>
        </div>
    </section> --}}
    <section class="mentors" id="mentors">
        <div class="container width-adaptive mx-auto relative pt-8 sm:pt-16 lg:pt-24">
            <h2 class="section-heading mb-20">Знакомтесь с нашими преподавателями</h2>
            <div class="owl-carousel">
                <div class="">
                    <div class="flex flex-col sm:flex-row space-y-8 sm:space-y-0 sm:space-x-8 md:space-x-12">
                        <div class="w-full sm:w-1/2">
                            <div class="media-wrapper rounded-lg overflow-hidden">
                                <img src="{{ asset('/img/home/mentor.png') }}" alt="">
                            </div>
                        </div>
                        <div class="w-full sm:w-1/2 stack">
                            <div class="text-xl md:text-2xl lg:text-3xl font-bold">Альберт Горяев</div>
                            <div class="text-sm md:text-lg lg:text-xl font-bold">Механико-математический факультет МГУ</div>
                            <div class="text-sm md:text-lg lg:text-xl my-6">Сын ученого, который занимается вопросами космического излучения в РАН. Альберт любит математику так сильно, что берет книги по функциональному анализу даже на встречи с друзьями.<br><br>Подготовил ряд школьников, которые поступили в ряд ведущих лицеев и математических школ Москвы.</div>
                            <div class="flex space-x-12">
                            <div class="w-1/2">
                                <div class="">ЕГЭ по математике</div>
                                <div class="font-bold text-sm md:text-lg">100 из 100 баллов</div>
                            </div>
                            <div class="w-1/2">
                                <div class="">Дополнительный экзамен МГУ</div>
                                <div class="font-bold text-sm md:text-lg">100 из 100 баллов</div>
                            </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="">
                    <div class="flex flex-col sm:flex-row space-y-8 sm:space-y-0 sm:space-x-8 md:space-x-12">
                        <div class="w-full sm:w-1/2">
                            <div class="media-wrapper rounded-lg overflow-hidden">
                                <img src="{{ asset('/img/home/mentor-2.jpg') }}" alt="">
                            </div>
                        </div>
                        <div class="w-full sm:w-1/2 stack">
                            <div class="text-xl md:text-2xl lg:text-3xl font-bold">Каплоухая Нина</div>
                            <div class="text-sm md:text-lg lg:text-xl font-bold">МФТИ. Факультет управления и прикладной математики</div>
                            <div class="text-sm md:text-lg lg:text-xl my-6">Cпециалист по разбору задач в нашей команде.
                            Нина подбирает ученикам MathLabs самые интересные задачи и прикладывает все усилия к их развитию. Кроме этого, Нина занимается анализом данных.
                            Нина не только сдала ЕГЭ на 100 баллов, но и стала победителем олимпиад “Ломоносов” и “Межвузовской математической олимпиады”, а также призером олимпиад “Покори Воробьевы горы”, “МФТИ” и «Курчатов»</div>
                            <div class="flex space-x-12">
                            <div class="w-1/2">
                                <div class="">ЕГЭ по математике</div>
                                <div class="font-bold text-sm md:text-lg">100 из 100 баллов</div>
                            </div>
                            <div class="w-1/2">
                                <div class="">Дополнительный экзамен МГУ</div>
                                <div class="font-bold text-sm md:text-lg">100 из 100 баллов</div>
                            </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="pricing" id="pricing">
        <div class="container width-adaptive mx-auto relative py-8 sm:py-16 lg:py-24">
            <div class="text-center">
                <h2 class="section-heading">Стоимость онлайн-занятий</h2>
                <div class="pricing-items inline-flex md:flex flex-col md:flex-row justify-center md:items-stretch space-x-0 space-y-4 md:space-x-4 md:space-y-0">
                    <div class="pricing-item pricing-item--green flex flex-col text-center bg-blue-lighter rounded-md px-6 py-12">
                        <div class="text-sm lg:text-xl text-gray-500 opacity-75">Первое вводное занятие</div>
                        <div class="mb-8 text-2xl lg:text-3xl font-bold">Бесплатно</div>
                        <div class="mt-auto"><a href="{{ route('site.sign-up-online') }}" class="btn-green-lg w-auto">Записаться</a></div>
                    </div>
                    <div class="pricing-item pricing-item--purple flex flex-col text-center bg-blue-lighter rounded-md px-6 py-12">
                        <div class="text-sm lg:text-xl text-gray-500 opacity-75">Занятия в группе</div>
                        <div class="mb-8 text-2xl lg:text-3xl font-bold">от 1 190 руб/занятие</div>
                        <div class="mt-auto"><a href="{{ route('site.sign-up-online') }}" class="btn-primary-lg w-auto">Записаться</a></div>
                    </div>
                    <div class="pricing-item pricing-item--blue flex flex-col text-center bg-blue-lighter rounded-md px-6 py-12">
                        <div class="text-sm lg:text-xl text-gray-500 opacity-75">Курсы по теме</div>
                        <div class="mb-8 text-2xl lg:text-3xl font-bold">от 3 200 руб.</div>
                        <div class="mt-auto"><a href="{{ route('site.sign-up-online') }}" class="btn-primary-lg w-auto">Записаться</a></div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="subscribe bg-primary relative overflow-hidden">
        <div class="container width-adaptive mx-auto relative py-24 z-10">
            <div class="stack text-white text-center">
                <h2 class="font-bold text-2xl sm:text-3xl md:text-4xl text-center">Присоединяйтесь к нам</h2>
                <div class="text-sm sm:text-lg md:text-2xl opacity-75">Начните с бесплатного пробного занятия</div>
                <div class="flex flex-col sm:flex-row items-center justify-center mt-8 mb-4">
                    <input type="text" class="form-control-default bg-white rounded-md w-60 md:w-96" name="phone_number" placeholder="Введите ваш телефон">
                    <button type="button" class="btn-black-sm w-auto mt-4 sm:mt-0 sm:ml-4">Записаться</button>
                </div>
            </div>
        </div>
        <div class="subscribe__cover-left"><svg width="775" height="311" viewBox="0 0 775 311" fill="none" xmlns="http://www.w3.org/2000/svg">
        <path opacity="0.2" d="M203.81 342.5C173.5 65 627.389 -231.173 552.434 19.8765C477.479 270.926 243.649 267.979 203.81 290.505C356.434 288.293 645.568 229.253 581.109 10.7815C516.649 -207.69 656.076 -92.3717 733.846 -7.40376L526.54 -371" stroke="white" stroke-width="5"></path>
        </svg></div>
        <div class="subscribe__cover-right"><svg width="519" height="311" viewBox="0 0 519 311" fill="none" xmlns="http://www.w3.org/2000/svg">
        <path opacity="0.2" d="M4 53.8845L106.824 344.454M4 53.8845L82 102.385M4 53.8845L221.529 -25M106.824 344.454L183.5 282.6M106.824 344.454L82 102.385M183.5 282.6L266.5 219.885M183.5 282.6L82 102.385M266.5 219.885L221.529 390.885L486.882 282.6M266.5 219.885L82 102.385M266.5 219.885L486.882 282.6M266.5 219.885L486.882 129.885M266.5 219.885L221.529 -25M486.882 282.6L608.471 31.8391M486.882 282.6L714 371.202L608.471 31.8391M486.882 282.6L486.882 129.885M608.471 31.8391L486.882 129.885M608.471 31.8391L426 -25L221.529 -25M82 102.385L221.529 -25M486.882 129.885L221.529 -25" stroke="white" stroke-width="5"></path>
        </svg></div>
    </section>
</div>

<div>
    <div class="feedback">
      <div class="feedback__container">
        <div class="feedback__title">Остались вопросы?</div>
        <div class="feedback__descr">Напишите нам! Мы&nbsp;ответим со&nbsp;скоростью света!</div>
        <form wire:submit.prevent="submitForm" class="feedback__form">
          <div class="feedback__form-row">
            <div class="w-full">
                <textarea wire:model.lazy="message" class="feedback__textarea" value="" placeholder="Вопросы сюда"></textarea>
                @error('message')
                    <p class="mt-2 text-sm text-red-600 text-left">{{ $message }}</p>
                @enderror
            </div>
          </div>
          <div class="feedback__form-row feedback__form-row--2-col space-y-4 sm:space-y-0 sm:space-x-4">
            <div class="w-full">
                <input wire:model.lazy="name" class="feedback__input-text" type="text" value="" placeholder="Ваше имя">
                @error('name')
                    <p class="mt-2 text-sm text-red-600 text-left">{{ $message }}</p>
                @enderror
            </div>
            <div class="w-full">
                <input wire:model.lazy="email" class="feedback__input-text" type="text" value="" placeholder="Ваш e-mail">
                @error('email')
                    <p class="mt-2 text-sm text-red-600 text-left">{{ $message }}</p>
                @enderror
            </div>
        </div>
          <div class="feedback__form-row">
            <button class="feedback__btn" type="submit">Отправить</button>
          </div>
          <div class="feedback__form-row">
            <div class="feedback__text">Нажимая на&nbsp;кнопку, я&nbsp;принимаю&nbsp;<a class="feedback__text-link" href="#">условия соглашения</a></div>
          </div>
        </form>
      </div>
    </div>
</div>

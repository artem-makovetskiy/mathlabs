<div>
    <div class="content">
        <div class="content__container">
            <div class="content__col content__col--left">
                @livewire('admin.sidebar-nav', [])
            </div>
            <div class="content__col content__col--right">
                @if (session()->has('message'))
                    <div class="alert alert-success flex items-center bg-green-100 text-green-500 rounded p-4 mb-4">
                        <div class="">
                            <svg fill="none" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" viewBox="0 0 24 24" class="w-8 h-8"><path d="M9 12l2 2 4-4m6 2a9 9 0 11-18 0 9 9 0 0118 0z"></path></svg>
                        </div>
                        <div class="ml-3">{{ session('message') }}</div>
                    </div>
                @endif
                @if (session()->has('warning_message'))
                    <div class="alert alert-warning flex items-center bg-orange-100 text-orange-400 rounded p-4 mb-4">
                        <div class="">
                            <svg fill="none" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" viewBox="0 0 24 24" class="w-8 h-8"><path d="M9 12l2 2 4-4m6 2a9 9 0 11-18 0 9 9 0 0118 0z"></path></svg>
                        </div>
                        <div class="ml-3">{{ session('warning_message') }}</div>
                        <!-- <div class="ml-3">Участники имеют разные занятия.</div> -->
                    </div>
                @endif
                @if ($errors->any())
                    <div class="alert alert-danger flex items-center bg-red-100 text-red-500 rounded p-4 mb-4">
                        <div class="">
                            <svg fill="none" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" viewBox="0 0 24 24" class="w-8 h-8"><path d="M12 9v2m0 4h.01m-6.938 4h13.856c1.54 0 2.502-1.667 1.732-3L13.732 4c-.77-1.333-2.694-1.333-3.464 0L3.34 16c-.77 1.333.192 3 1.732 3z"></path></svg>
                        </div>
                        <ul class="ml-3">
                            @foreach ($errors->all() as $error)
                                <li>* {{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <!-- <h3 class="text-2xl font-bold pt-2 mb-6">Создание группы</h3> -->
                <div class="panel mb-6 overflow-visible">
                    <div x-data="{ tab: 'members', selectMember: false, selectOpen: false, selectLessonOpen: false }" class="">
                        <div class="stack stack-lg">
                            <div class="border-b border-gray-200 px-8 pt-8">
                                <div class="flex items-center space-x-8">
                                    <div class="border-b-2 border-transparent pb-4 relative top-0.5" :class="{ 'border-primary': tab === 'members' }"><a @click="tab = 'members'" href="#" class="font-semibold text-gray-400" :class="{ 'text-gray-700': tab === 'members' }">Участники</a></div>
                                    <div class="border-b-2 border-transparent pb-4 relative top-0.5" :class="{ 'border-primary': tab === 'settings' }"><a @click="tab = 'settings'" href="#" class="font-semibold text-gray-400" :class="{ 'text-gray-700': tab === 'settings' }">Настройки</a></div>
                                </div>
                            </div>
                            <div x-show="tab === 'members'" class="stack stack-lg">
                                <div class="flex justify-between px-8">
                                    <div class="relative">
                                        <button x-show.transition.duration.150ms="!selectMember" x-on:click="selectMember = true" type="button" class="inline-flex items-center px-4 py-2 shadow-sm border border-transparent text-sm leading-4 font-medium rounded-md text-white bg-primary hover:bg-primary-500 focus:outline-none focus:border-primary-700 focus:shadow-outline-indigo active:bg-primary-700 transition ease-in-out duration-150">
                                            <svg class="-ml-1 mr-2 h-5 w-5" fill="none" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" viewBox="0 0 24 24" stroke="currentColor"><path d="M18 9v3m0 0v3m0-3h3m-3 0h-3m-2-5a4 4 0 11-8 0 4 4 0 018 0zM3 20a6 6 0 0112 0v1H3v-1z"></path></svg>
                                            Добавить участника
                                        </button>
                                        <div x-show.transition.duration.150ms="selectMember" class="absolute top-0 left-0 space-y-1" style="display: none;">
                                            <div class="relative">
                                                <span class="inline-block w-96 rounded-md shadow-sm">
                                                    <button @click="selectOpen = true" type="button" aria-haspopup="listbox" aria-expanded="true"
                                                        aria-labelledby="listbox-label"
                                                        class="cursor-default relative w-full rounded-md border border-gray-300 bg-white pl-3 pr-10 py-2 text-left focus:outline-none focus:shadow-outline-blue focus:border-blue-300 transition ease-in-out duration-150 sm:text-sm sm:leading-5">
                                                        <div class="flex items-center space-x-3">
                                                            <img class="flex-shrink-0 h-6 w-6 rounded-full" src="http://mathlabs.test/img/icons/icon-avatar-example.svg" alt="" />
                                                            <span class="block truncate leading-4">
                                                                Новый участник
                                                            </span>
                                                        </div>
                                                        <span
                                                            class="absolute inset-y-0 right-0 flex items-center pr-2 pointer-events-none">
                                                            <svg class="h-5 w-5 text-gray-400" viewBox="0 0 20 20"
                                                                fill="none" stroke="currentColor">
                                                                <path d="M7 7l3-3 3 3m0 6l-3 3-3-3" stroke-width="1.5"
                                                                    stroke-linecap="round" stroke-linejoin="round" />
                                                            </svg>
                                                        </span>
                                                    </button>
                                                </span>

                                                <!-- Select popover, show/hide based on select state. -->
                                                <div x-show="selectOpen" @click.away="selectOpen = false" class="absolute mt-1 w-full rounded-md bg-white shadow-lg z-10" style="display: none;">
                                                    <ul tabindex="-1" role="listbox" aria-labelledby="listbox-label"
                                                        aria-activedescendant="listbox-item-3"
                                                        class="max-h-56 rounded-md py-1 text-base leading-6 shadow-xs overflow-auto focus:outline-none sm:text-sm sm:leading-5">

                                                        <!--
                                                            Select option, manage highlight styles based on mouseenter/mouseleave and keyboard navigation.

                                                            Highlighted: "text-white bg-indigo-600", Not Highlighted: "text-gray-900"
                                                        -->
                                                        @forelse($this->new_members_poll as $member_key => $member)
                                                            <li wire:click="addNewMember({{ $member['id'] }}, {{ $member_key }})" id="listbox-item-0" role="option"
                                                                class="text-gray-900 cursor-default select-none relative py-2 pl-3 pr-9 hover:text-white hover:bg-primary">
                                                                <div class="flex items-center">
                                                                    <img class="flex-shrink-0 h-6 w-6 rounded-full" src="http://mathlabs.test/img/icons/icon-avatar-example.svg" alt="" />
                                                                    <!-- Selected: "font-semibold", Not Selected: "font-normal" -->
                                                                    <!-- <span class="font-normal block truncate ml-2">
                                                                        {{ $member['id'] }}
                                                                    </span> -->
                                                                    <span class="font-normal block truncate ml-2">
                                                                        {{ $member['user']['child_name'] }}
                                                                    </span>
                                                                    <span class="block truncate ml-4">
                                                                        {{ collect($member['scheduled_time']['weekdays'])->join(', ') }}:
                                                                        {{ collect($member['scheduled_time']['time'])->join(', ') }}
                                                                    </span>
                                                                </div>

                                                                <!--
                                                                Checkmark, only display for selected option.

                                                                Highlighted: "text-white", Not Highlighted: "text-indigo-600"
                                                                -->
                                                                <span
                                                                    class="absolute inset-y-0 right-0 flex items-center pr-4{{ collect($group_members)->search(fn($item) => $item['purchase_id'] === $member['id']) !== false ? '' : ' hidden' }}">
                                                                    <svg class="h-5 w-5" viewBox="0 0 20 20"
                                                                        fill="currentColor">
                                                                        <path fill-rule="evenodd"
                                                                            d="M16.707 5.293a1 1 0 010 1.414l-8 8a1 1 0 01-1.414 0l-4-4a1 1 0 011.414-1.414L8 12.586l7.293-7.293a1 1 0 011.414 0z"
                                                                            clip-rule="evenodd" />
                                                                    </svg>
                                                                </span>
                                                            </li>
                                                        @empty
                                                            <li class="text-gray-900 cursor-default select-none relative py-2 pl-3 pr-9 hover:text-white hover:bg-primary"><div class="flex items-center">-</div></li>
                                                        @endforelse

                                                        <!-- More options... -->
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <span class="relative z-0 inline-flex shadow-sm">
                                        <button type="button" class="relative inline-flex items-center px-4 py-1.5 rounded-l-md border border-gray-300 bg-white text-sm leading-5 font-medium text-gray-700hover:text-gray-500 focus:z-10 focus:outline-none focus:border-blue-300 focus:shadow-outline-blue active:bg-gray-100 active:text-gray-700 transition ease-in-out duration-150">
                                            Все
                                        </button>
                                        <button type="button" class="-ml-px relative inline-flex items-center px-4 py-1.5 border border-gray-300 bg-white text-sm leading-5 font-medium text-gray-700 hover:text-gray-500 focus:z-10 focus:outline-none focus:border-blue-300 focus:shadow-outline-blue active:bg-gray-100 active:text-gray-700 transition ease-in-out duration-150">
                                            Какие-то
                                        </button>
                                        <div class="-ml-px rounded-l-none shadow-sm">
                                            <input type="text" class="form-input block w-full rounded-l-none bg-gray-100 sm:text-sm sm:leading-5" placeholder="Filter members...">
                                        </div>
                                    </span>
                                </div>
                                <div class="px-8 pb-8">
                                    <table class="min-w-full relative">
                                        <thead>
                                        <tr class="">
                                            <th class="border-b-2 border-gray-100 p-4 text-left text-xs leading-4 font-medium text-gray-500 uppercase tracking-wider">
                                                Имя ученика
                                            </th>
                                            <th class="border-b-2 border-gray-100 p-4 text-left text-xs leading-4 font-medium text-gray-500 uppercase tracking-wider">
                                                Занятие
                                            </th>
                                            <th class="border-b-2 border-gray-100 p-4 text-left text-xs leading-4 font-medium text-gray-500 uppercase tracking-wider">
                                                Время
                                            </th>
                                            <th class="border-b-2 border-gray-100 p-4"></th>
                                        </tr>
                                        </thead>
                                        <tbody class="bg-white">
                                        @if(count($this->group_members) > 0)
                                            @foreach($this->group_members as $member)
                                                <tr>
                                                    <td class="p-4 border-b border-gray-100 whitespace-no-wrap text-sm leading-5 font-medium text-gray-900">
                                                        <div class="flex items-center space-x-2">
                                                            <div class="flex-shrink-0">
                                                                <img class="h-6 w-6 rounded-full" src="http://mathlabs.test/img/icons/icon-avatar-example.svg" alt="" />
                                                            </div>
                                                            <span class="leading-tight">{{ $member['child_name'] }}</span>
                                                        </div>
                                                    </td>
                                                    <td class="p-4 border-b border-gray-100 whitespace-no-wrap text-sm leading-5 text-gray-500">
                                                        <a class="font-semibold underline" href="{{ route('admin.group-lessons.edit', $member['lesson']['id']) }}">{{ $member['lesson']['title'] }}</a>
                                                    </td>
                                                    <td class="p-4 border-b border-gray-100 whitespace-no-wrap text-sm leading-5 text-gray-500">
                                                        {{ collect($member['scheduled_time']['weekdays'])->join(', ') }}:
                                                        {{ collect($member['scheduled_time']['time'])->join(', ') }}
                                                    </td>
                                                    <td class="p-4 border-b border-gray-100 whitespace-no-wrap text-right text-sm leading-5 font-medium">
                                                        <div wire:click="removeItem({{ $member['id'] }})" class="w-5 h-5 cursor-pointer text-gray-500">
                                                            <svg fill="none" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" viewBox="0 0 24 24" stroke="currentColor"><path d="M19 7l-.867 12.142A2 2 0 0116.138 21H7.862a2 2 0 01-1.995-1.858L5 7m5 4v6m4-6v6m1-10V4a1 1 0 00-1-1h-4a1 1 0 00-1 1v3M4 7h16"></path></svg>
                                                        </div>
                                                    </td>
                                                </tr>
                                            @endforeach
                                        @else
                                            <tr>
                                                <td class="p-4"></td>
                                            </tr>
                                        @endif
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div x-show="tab === 'settings'" class="px-8 pb-8">
                                <form wire:submit.prevent="updateGroup" wire:key="form">
                                    <div>
                                        <div>
                                            <div class="mt-6 grid grid-cols-1 row-gap-6 col-gap-4 sm:grid-cols-6">
                                                <div class="sm:col-start-1 sm:col-end-4">
                                                    <div class="space-y-1">
                                                        <label id="listbox-label"
                                                            class="block text-sm leading-5 font-medium text-gray-700">
                                                            Занятие
                                                        </label>
                                                        <div class="relative">
                                                            <span class="inline-block w-full rounded-md shadow-sm">
                                                                <button @click="selectLessonOpen = true" type="button" aria-haspopup="listbox"
                                                                    aria-expanded="true" aria-labelledby="listbox-label"
                                                                    class="cursor-default relative w-full rounded-md border border-gray-300 bg-white pl-3 pr-10 py-2 text-left focus:outline-none focus:shadow-outline-blue focus:border-blue-300 transition ease-in-out duration-150 sm:text-sm sm:leading-5">
                                                                    <span class="block truncate">
                                                                        {{ $lessonData['title'] }}
                                                                    </span>
                                                                    <span
                                                                        class="absolute inset-y-0 right-0 flex items-center pr-2 pointer-events-none">
                                                                        <svg class="h-5 w-5 text-gray-400"
                                                                            viewBox="0 0 20 20" fill="none"
                                                                            stroke="currentColor">
                                                                            <path d="M7 7l3-3 3 3m0 6l-3 3-3-3"
                                                                                stroke-width="1.5"
                                                                                stroke-linecap="round"
                                                                                stroke-linejoin="round" />
                                                                        </svg>
                                                                    </span>
                                                                </button>
                                                            </span>

                                                            <!-- Select popover, show/hide based on select state. -->
                                                            <div x-show="selectLessonOpen" @click.away="selectLessonOpen = false"
                                                                class="absolute mt-1 w-full rounded-md bg-white shadow-lg z-10" style="display: none;">
                                                                <ul tabindex="-1" role="listbox"
                                                                    aria-labelledby="listbox-label"
                                                                    aria-activedescendant="listbox-item-3"
                                                                    class="max-h-60 rounded-md py-1 text-base leading-6 shadow-xs overflow-auto focus:outline-none sm:text-sm sm:leading-5">

                                                                    <!--
                                                                Select option, manage highlight styles based on mouseenter/mouseleave and keyboard navigation.

                                                                Highlighted: "text-white bg-indigo-600", Not Highlighted: "text-gray-900"
                                                            -->
                                                                    @forelse($this->lessons_poll as $lesson_key => $lesson_opt)
                                                                        <li wire:click="changeLesson({{ $lesson_opt['id'] }})" id="listbox-option-0" role="option"
                                                                            class="cursor-default select-none relative py-2 pl-3 pr-9 hover:text-white hover:bg-primary{{ $lessonData['id'] === $lesson_opt['id'] ? ' text-white bg-primary' : ' text-gray-900' }}">
                                                                            <!-- Selected: "font-semibold", Not Selected: "font-normal" -->
                                                                            <span class="font-normal block truncate">
                                                                                {{ $lesson_opt['title'] }}
                                                                            </span>

                                                                            <!--
                                                                            Checkmark, only display for selected option.

                                                                            Highlighted: "text-white", Not Highlighted: "text-indigo-600"
                                                                            -->
                                                                            <span
                                                                                class="absolute inset-y-0 right-0 flex items-center pr-4{{ $lessonData['id'] === $lesson_opt['id'] ? '' : ' hidden' }}">
                                                                                <svg class="h-5 w-5" viewBox="0 0 20 20"
                                                                                    fill="currentColor">
                                                                                    <path fill-rule="evenodd"
                                                                                        d="M16.707 5.293a1 1 0 010 1.414l-8 8a1 1 0 01-1.414 0l-4-4a1 1 0 011.414-1.414L8 12.586l7.293-7.293a1 1 0 011.414 0z"
                                                                                        clip-rule="evenodd" />
                                                                                </svg>
                                                                            </span>
                                                                        </li>
                                                                    @empty
                                                                        <li class="cursor-default select-none relative py-2 pl-3 pr-9 hover:text-white hover:bg-primary">-</li>
                                                                    @endforelse

                                                                    <!-- More options... -->
                                                                </ul>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="sm:col-start-1 sm:col-span-1">
                                                    <label for="settings.weekday" class="block text-sm font-medium leading-5 text-gray-700">
                                                        День недели
                                                    </label>
                                                    <div class="mt-1 rounded-md shadow-sm">
                                                        <select wire:model="settings.weekday" id="settings.weekday" class="form-select block w-full bg-gray-100 transition duration-150 ease-in-out sm:text-sm sm:leading-5">
                                                            <option value="">-</option>
                                                            @foreach($this->weekdays_options as $option)
                                                                <option value="{{ $option }}">{{ $option }}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="sm:col-span-2">
                                                    <label for="lesson_time" class="block text-sm font-medium leading-5 text-gray-700">Время</label>
                                                    <div class="mt-1 relative rounded-md shadow-sm">
                                                        <div class="absolute inset-y-0 left-0 pl-3 flex items-center pointer-events-none">
                                                            <svg class="h-5 w-5 text-gray-400" fill="none" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" viewBox="0 0 24 24" stroke="currentColor"><path d="M12 8v4l3 3m6-3a9 9 0 11-18 0 9 9 0 0118 0z"></path></svg>
                                                        </div>
                                                        <input wire:model="settings.lesson_time" id="lesson_time" class="form-input block w-full pl-10 bg-gray-100 sm:text-sm sm:leading-5" placeholder="HH:MM" />
                                                    </div>
                                                </div>
                                                @if($settings['schedule_meeting'])
                                                <div class="sm:col-span-1">
                                                    <label for="weekday" class="block text-sm font-medium leading-5 text-gray-700">
                                                        Кол-во занятий
                                                    </label>
                                                    <div class="mt-1 rounded-md shadow-sm">
                                                        <select wire:model="settings.end_times" id="end_times" class="form-select block w-full bg-gray-100 transition duration-150 ease-in-out sm:text-sm sm:leading-5">
                                                            <option value="">-</option>
                                                            @foreach($this->num_of_lessons_opts as $option)
                                                                <option value="{{ $option }}">{{ $option }}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                                @endif
                                                <div class="sm:col-span-6">
                                                    <div class="flex items-center">
                                                        <input  wire:model="settings.schedule_meeting" id="remember_me" type="checkbox" class="form-checkbox h-4 w-4 text-indigo-600 transition duration-150 ease-in-out" />
                                                        <label for="remember_me" class="ml-2 block text-sm leading-tight text-gray-900">
                                                            {{ $groupData['schedule_meeting'] ? 'Обновить трансляцию' : 'Запланировать трансляцию' }}
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    @if($meeting)
                                        <div class="grid grid-cols-1 row-gap-3 sm:grid-cols-6 mt-6">
                                            <div class="sm:col-span-6">
                                                <h3 class="text-lg font-semibold mb-2">Запланированные занятия</h3>
                                            </div>
                                            <div class="flex items-center sm:col-span-2 bg-gray-100 rounded-tl-lg rounded-bl-lg mb-4">
                                                <div class="px-4 py-4">Следующее: {{ $meeting['next_meeting_time'] }}</div>
                                            </div>
                                            <div class="flex items-center sm:col-span-2 bg-gray-100 mb-4">
                                                <div class="px-4 py-4">Осталось занятий: {{ $meeting['end_times_left'] }}</div>
                                            </div>
                                            <div class="flex items-center justify-end sm:col-span-2 sm:col-start-5 text-right bg-gray-100 rounded-tr-lg rounded-br-lg px-4 py-4 mb-4">
                                                <a href="{{ route('account.streamLesson', [$lesson->type, $lesson->uuid, $meeting['meeting_id']]) }}" class="item__btn--blue-empty px-4 py-2 text-sm w-auto">Начать трансляцию</a>
                                            </div>
                                        </div>
                                    @endif
                                    <div class="mt-8 border-t border-gray-200 pt-5">
                                        <div class="flex justify-end">
                                            <a class="item__btn--blue-empty w-auto" href="{{ route('admin.webinars.index') }}">Отмена</a>
                                            <button class="item__btn--blue w-auto ml-3">
                                                <svg wire:loading wire:target="updateGroup" class="hidden text-white w-4 h-4 icon--is-spinning mr-2" aria-hidden="true" viewBox="0 0 16 16"><title>Loading</title><g stroke-width="1" fill="currentColor" stroke="currentColor"><path d="M.5,8a7.5,7.5,0,1,1,1.91,5" fill="none" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round"></path></g></svg>
                                                <span>Сохранить</span>
                                            </button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.26.0/moment.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/flatpickr"></script>
    <script src="https://npmcdn.com/flatpickr/dist/l10n/ru.js"></script>
    <script>
        (function (flatpickr) {
            flatpickr("#lesson_time", {
                enableTime: true,
                noCalendar: true,
                dateFormat: "H:i",
                time_24hr: true,
                "locale": "ru"
            });
        })(flatpickr);
    </script>
</div>
<div>
    <div class="content">
        <div class="content__container">
            <div class="content__col content__col--left">
                @livewire('admin.sidebar-nav', ['active_page' => 'all_group_lessons'])
            </div>
            <div class="content__col content__col--right">
                <div class="content-header content-header--online-classes">
                    <div class="content-header__container px-4">
                        <div class="flex justify-between items-center px-2 py-4">
                            <div class="hidden w-1/2 flex rounded-md shadow-sm">
                                <div class="relative flex-grow focus-within:z-10">
                                    <div class="absolute inset-y-0 left-0 pl-3 flex items-center pointer-events-none">
                                        <svg class="h-5 w-5 text-gray-400" fill="none" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" viewBox="0 0 24 24" class="w-8 h-8"><path d="M21 21l-6-6m2-5a7 7 0 11-14 0 7 7 0 0114 0z"></path></svg>
                                    </div>
                                    <input id="email" class="form-input block w-full rounded-none rounded-l-md pl-10 transition ease-in-out duration-150 sm:text-sm sm:leading-5" placeholder="Search..." />
                                </div>
                                <button class="-ml-px relative inline-flex items-center px-4 py-2 border border-gray-300 text-sm leading-5 font-medium rounded-r-md text-gray-700 bg-gray-50 hover:text-gray-500 hover:bg-white focus:outline-none focus:shadow-outline-blue focus:border-blue-300 active:bg-gray-100 active:text-gray-700 transition ease-in-out duration-150">
                                    <svg class="h-5 w-5 text-gray-400" fill="currentColor" viewBox="0 0 20 20"><path fill-rule="evenodd" d="M3 3a1 1 0 011-1h12a1 1 0 011 1v3a1 1 0 01-.293.707L12 11.414V15a1 1 0 01-.293.707l-2 2A1 1 0 018 17v-5.586L3.293 6.707A1 1 0 013 6V3z" clip-rule="evenodd"></path></svg>
                                    <span class="ml-2">Filter</span>
                                </button>
                            </div>
                            <a href="{{ route('admin.user-groups.create') }}" class="item__btn--blue flex space-x-2 w-auto">
                                <svg class="-ml-1 h-4 w-4" fill="none" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" viewBox="0 0 24 24"><path d="M12 4v16m8-8H4"></path></svg>
                                <span class="leading-none">Создать группу</span>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="panel">
                    <ul>
                        @foreach($this->groups as $group)
                            <li class="border-t border-gray-200">
                                <a href="{{ route('admin.user-groups.edit', $group->id) }}"
                                    class="block hover:bg-gray-50 focus:outline-none focus:bg-gray-50 transition duration-150 ease-in-out">
                                    <div class="px-4 py-4 sm:px-6">
                                        <div class="md:grid md:grid-cols-12 md:gap-4">
                                        <!-- </div> -->
                                        <!-- <div class="flex items-center justify-between"> -->
                                            <div class="col-span-5">
                                                <div class="text-sm leading-5 font-medium text-indigo-600 truncate">
                                                    {{ $group->lesson->title }}
                                                </div>
                                                <div class="mt-2 flex items-center text-sm leading-5 text-gray-500">
                                                    <svg class="flex-shrink-0 mr-1.5 h-5 w-5 text-gray-400"
                                                        fill="currentColor" viewBox="0 0 20 20">
                                                        <path
                                                            d="M9 6a3 3 0 11-6 0 3 3 0 016 0zM17 6a3 3 0 11-6 0 3 3 0 016 0zM12.93 17c.046-.327.07-.66.07-1a6.97 6.97 0 00-1.5-4.33A5 5 0 0119 16v1h-6.07zM6 11a5 5 0 015 5v1H1v-1a5 5 0 015-5z" />
                                                    </svg>
                                                    Групповой/Индивидуальный
                                                </div>
                                            </div>
                                            <div class="col-span-3">
                                                <div class="flex items-center overflow-hidden">
                                                    @foreach($group->users()->take(5)->get() as $user)
                                                    <img class="inline-block h-6 w-6 rounded-full text-white shadow-solid"
                                                        src="http://mathlabs.test/img/icons/icon-avatar-example.svg"
                                                        alt="" />
                                                    @endforeach
                                                    @if($group->users()->count() > 5)
                                                    <div class="ml-2 text-gray-500">+{{ $group->users()->count() - 5 }}</div>
                                                    @endif
                                                </div>
                                                <div class="mt-2 flex items-center text-sm leading-5 text-gray-500">
                                                    <svg class="flex-shrink-0 mr-1.5 h-5 w-5 text-gray-400" fill="none"
                                                        stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                                                        viewBox="0 0 24 24" stroke="currentColor">
                                                        <path d="M12 8v4l3 3m6-3a9 9 0 11-18 0 9 9 0 0118 0z"></path>
                                                    </svg>
                                                    <span>
                                                        Время урока - 
                                                        <time datetime="2020-01-07">{{ $group->scheduled_time }}</time>
                                                    </span>
                                                </div>
                                            </div>
                                            <div class="col-span-4">
                                                <div class="ml-2 flex-shrink-0 flex justify-end">
                                                    @if($group->schedule_meeting)
                                                    <span class="mr-2 inline-flex items-center px-2.5 py-0.5 rounded-full text-xs font-medium leading-4 bg-purple-100 text-purple-800">
                                                        Запланирована
                                                    </span>
                                                    @endif
                                                    <span
                                                        class="px-2 inline-flex text-xs leading-5 font-semibold rounded-full bg-green-100 text-green-800">
                                                        Статус
                                                    </span>
                                                </div>
                                                <div class="mt-2 flex items-center justify-end text-sm leading-5 text-gray-500">
                                                    <svg class="flex-shrink-0 mr-1.5 h-5 w-5 text-gray-400" fill="currentColor"
                                                        viewBox="0 0 20 20">
                                                        <path fill-rule="evenodd"
                                                            d="M6 2a1 1 0 00-1 1v1H4a2 2 0 00-2 2v10a2 2 0 002 2h12a2 2 0 002-2V6a2 2 0 00-2-2h-1V3a1 1 0 10-2 0v1H7V3a1 1 0 00-1-1zm0 5a1 1 0 000 2h8a1 1 0 100-2H6z"
                                                            clip-rule="evenodd" />
                                                    </svg>
                                                    <span>
                                                        Добавлена
                                                        <time datetime="2020-01-07">{{ $group->created_at->diffForHumans() }}</time>
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </a>
                            </li>
                        @endforeach
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
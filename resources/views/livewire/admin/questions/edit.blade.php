<div>
    <div class="content">
        <div class="content__container">
            <div class="content__col content__col--left">
                @livewire('admin.sidebar-nav', [])
            </div>
            <div class="content__col content__col--right">
                <div class="hidden content-header content-header--online-classes">
                    <div class="content-header__container">
                        <div class="px-6 py-5">
                            <h3 class="text-2xl font-bold">Редактировать вопрос</h3>
                        </div>
                    </div>
                </div>
                <div class="panel">
                    <div class="px-6 py-4">
                        <div class="stack stack-lg">
                            <h3 class="text-2xl font-bold">Редактировать вопрос</h3>
                            @if (session()->has('message'))
                                <div class="alert alert-success flex items-center bg-green-100 text-green-500 rounded p-4">
                                    <div class="">
                                        <svg fill="none" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" viewBox="0 0 24 24" class="w-8 h-8"><path d="M9 12l2 2 4-4m6 2a9 9 0 11-18 0 9 9 0 0118 0z"></path></svg>
                                    </div>
                                    <div class="ml-3">{{ session('message') }}</div>
                                </div>
                            @endif
                            @if ($errors->any())
                                <div class="alert alert-danger flex items-center bg-red-100 text-red-500 rounded p-4">
                                    <div class="">
                                        <svg fill="none" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" viewBox="0 0 24 24" class="w-8 h-8"><path d="M12 9v2m0 4h.01m-6.938 4h13.856c1.54 0 2.502-1.667 1.732-3L13.732 4c-.77-1.333-2.694-1.333-3.464 0L3.34 16c-.77 1.333.192 3 1.732 3z"></path></svg>
                                    </div>
                                    <ul class="ml-3">
                                        @foreach ($errors->all() as $error)
                                            <li>* {{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif
                            <form wire:submit.prevent="updateQuestion" wire:key="form">
                                <div>
                                    <div>
                                        <div class="mt-6 grid grid-cols-1 row-gap-6 col-gap-4 sm:grid-cols-6">
                                            <div class="sm:col-span-2">
                                                <div class="flex justify-between">
                                                    <label for="title" class="block text-sm font-medium leading-5 text-gray-700">Название</label>
                                                </div>
                                                <div class="mt-1 relative rounded-md shadow-sm">
                                                    <input wire:model.lazy="questionData.title" id="title" class="form-input block w-full bg-gray-100 sm:text-sm sm:leading-5" placeholder="" />
                                                </div>
                                            </div>
                                            <div class="sm:col-span-1">
                                                <label for="type" class="block text-sm font-medium leading-5 text-gray-700">
                                                    Тип
                                                </label>
                                                <div class="mt-1 rounded-md shadow-sm">
                                                    <select wire:model="questionData.type_selected" id="type" class="form-select block w-full bg-gray-100 transition duration-150 ease-in-out sm:text-sm sm:leading-5">
                                                        <option value="">-</option>
                                                        @foreach($this->types as $question_type)
                                                            <option value="{{ $question_type['value'] }}">{{ $question_type['name'] }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="sm:col-span-1">
                                                <label for="type" class="block text-sm font-medium leading-5 text-gray-700">
                                                    Класс
                                                </label>
                                                <div class="mt-1 rounded-md shadow-sm">
                                                    <select wire:model="questionData.grade" id="grade" class="form-select block w-full bg-gray-100 transition duration-150 ease-in-out sm:text-sm sm:leading-5">
                                                        <option value="">-</option>
                                                        @foreach($this->grades as $grade_key => $grade)
                                                            <option value="{{ $grade_key }}">{{ $grade }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="sm:col-span-4">
                                                <div class="flex justify-between">
                                                    <label for="body" class="block text-sm font-medium leading-5 text-gray-700">Описание</label>
                                                    <span class="text-sm leading-5 text-gray-500"></span>
                                                </div>
                                                <div class="mt-1 rounded-md shadow-sm">
                                                    <textarea wire:model.lazy="questionData.body" id="body" rows="3" class="form-textarea block w-full bg-gray-100 transition duration-150 ease-in-out sm:text-sm sm:leading-5"></textarea>
                                                </div>
                                                <p class="hidden mt-2 text-sm text-gray-500"></p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                @if($questionData['type_selected'] == 1)
                                    <h4 class="text-xl font-bold mt-6">Ответы</h4>
                                    <div class="mt-6">
                                        <div class="relative w-64 rounded-md shadow-sm">
                                            <input wire:model.lazy="option_single.name" id="" class="form-input block w-full bg-gray-100 sm:text-sm sm:leading-5" placeholder="Текст ответа" />
                                        </div>
                                    </div>
                                @elseif(in_array($questionData['type_selected'], [2,3]))
                                    <h4 class="text-xl font-bold mt-6">Ответы</h4>
                                    <div class="mt-6">
                                        <div class="grid grid-cols-1 row-gap-6 col-gap-4 sm:grid-cols-6">
                                            @foreach($question_options as $option_index => $option)
                                                <div class="sm:col-span-2 flex items-center">
                                                    <div>#{{ $option_index + 1 }}</div>
                                                    <div class="ml-2 relative w-full rounded-md shadow-sm">
                                                        <input wire:model.lazy="question_options.{{$option_index}}.name" id="" class="form-input block w-full bg-gray-100 sm:text-sm sm:leading-5" placeholder="Текст ответа" />
                                                    </div>
                                                </div>
                                                <div class="sm:col-span-4">
                                                    <div class="flex items-center h-full">
                                                        <div class="flex items-center h-5">
                                                            <input wire:model="question_options.{{$option_index}}.is_solution" id="quest_opt_{{ $option_index }}" type="checkbox" class="form-checkbox h-4 w-4 text-indigo-600 transition duration-150 ease-in-out" />
                                                        </div>
                                                        <label for="quest_opt_{{ $option_index }}" class="ml-2">Верный ответ</label>
                                                    </div>
                                                </div>
                                            @endforeach
                                        </div>
                                        <a wire:click.prevent="addOption" class="item__btn--blue-empty w-auto mt-4" href="#">Добавить ответ</a>
                                    </div>
                                @endif
                                <div class="mt-8 border-t border-gray-200 pt-5">
                                    <div class="flex justify-end">
                                        <a class="item__btn--blue-empty w-auto" href="{{ route('admin.webinars.index') }}">Отмена</a>
                                        <button class="item__btn--blue w-auto ml-3">
                                            <svg wire:loading wire:target="updateQuestion" class="hidden text-white w-4 h-4 icon--is-spinning mr-2" aria-hidden="true" viewBox="0 0 16 16"><title>Loading</title><g stroke-width="1" fill="currentColor" stroke="currentColor"><path d="M.5,8a7.5,7.5,0,1,1,1.91,5" fill="none" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round"></path></g></svg>
                                            <span>Сохранить</span>
                                        </button>

                                        <span class="hidden inline-flex rounded-md shadow-sm">
                                          <button type="button" class="py-2 px-4 border border-gray-300 rounded-md text-sm leading-5 font-medium text-gray-700 hover:text-gray-500 focus:outline-none focus:border-blue-300 focus:shadow-outline-blue active:bg-gray-50 active:text-gray-800 transition duration-150 ease-in-out">
                                            Отмена
                                          </button>
                                        </span>
                                        <span class="hidden ml-3 inline-flex rounded-md shadow-sm">
                                          <button type="submit" class="inline-flex justify-center items-center py-2 px-4 border border-transparent text-sm leading-5 font-medium rounded-md text-white bg-indigo-600 hover:bg-indigo-500 focus:outline-none focus:border-indigo-700 focus:shadow-outline-indigo active:bg-indigo-700 transition duration-150 ease-in-out">
                                              <svg wire:loading wire:target="updateQuestion" class="hidden text-white w-4 h-4 icon--is-spinning mr-2" aria-hidden="true" viewBox="0 0 16 16"><title>Loading</title><g stroke-width="1" fill="currentColor" stroke="currentColor"><path d="M.5,8a7.5,7.5,0,1,1,1.91,5" fill="none" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round"></path></g></svg>
                                              <span>Сохранить</span>
                                          </button>
                                        </span>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        (function () {

        })();
    </script>
</div>
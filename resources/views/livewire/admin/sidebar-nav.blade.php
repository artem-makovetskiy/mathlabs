<div>
    <div class="sidebar">
        <div class="sidebar__nav">
            <a class="sidebar__item sidebar__item--group-lessons{{ $active_page === 'all_group_lessons' ? ' sidebar__item--active' : '' }}" href="{{ route('admin.group-lessons.index') }}">Групповые занятия<span class="sidebar__item-border"></span></a>
            <!-- <a class="sidebar__item{{ $active_page === 'group_lessons_purchases' ? ' sidebar__item--active' : '' }}" href="{{ route('admin.group-lessons.purchases') }}">Записи на групповые занятия<span class="sidebar__item-border"></span></a> -->
            <a class="sidebar__item sidebar__item--available-classes{{ $active_page === 'all_webinars' ? ' sidebar__item--active' : '' }}" href="{{ route('admin.webinars.index') }}">Интенсивы<span class="sidebar__item-border"></span></a>
            <a class="sidebar__item sidebar__item--questions{{ $active_page === 'questions' ? ' sidebar__item--active' : '' }}" href="{{ route('admin.questions.index') }}"><div>Вопросы для теста</div><span class="sidebar__item-border"></span></a>
            <a class="sidebar__item sidebar__item--settings{{ $active_page === 'settings' ? ' sidebar__item--active' : '' }}" href="#">Настройки<span class="sidebar__item-border"></span></a>
            <a class="sidebar__item sidebar__item--notifications{{ $active_page === 'notifications' ? ' sidebar__item--active' : '' }}" href="#">Уведомления<div class="sidebar__item-numbers hidden">5</div><span class="sidebar__item-border"></span></a>
            <a class="sidebar__item sidebar__item--exit" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">Выход<span class="sidebar__item-border"></span></a>
        </div>
    </div>
</div>

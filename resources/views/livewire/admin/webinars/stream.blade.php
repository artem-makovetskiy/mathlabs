<div>
{{--    <div class="content">--}}
{{--        <div class="content__container">--}}
{{--            <div class="content__col content__col--left">--}}
{{--                <div class="sidebar">--}}
{{--                    <div class="sidebar__nav">--}}
{{--                        <a class="sidebar__item sidebar__item--available-classes" href="{{ route('admin.webinars.index') }}">{{ $this->label }}<div class="sidebar__item-numbers">5</div><span class="sidebar__item-border"></span></a>--}}
{{--                        <a class="sidebar__item sidebar__item--settings" href="#">Настройки<span class="sidebar__item-border"></span></a>--}}
{{--                        <a class="sidebar__item sidebar__item--notifications" href="#">Уведомления<div class="sidebar__item-numbers">5</div><span class="sidebar__item-border"></span></a>--}}
{{--                        <a class="sidebar__item sidebar__item--exit" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">Выход<span class="sidebar__item-border"></span></a>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--            <div class="content__col content__col--right">--}}
{{--                <div class="hidden content-header content-header--online-classes">--}}
{{--                    <div class="content-header__container">--}}
{{--                        <div class="px-6 py-5">--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--                <div class="panel">--}}
{{--                    <div class="px-6 py-4">--}}
{{--                        <iframe src="https://zoom.us/wc/71250575822/join?prefer=1&un=dG9tbXk=" width="1000" height="500" sandbox="allow-same-origin allow-forms allow-scripts" allow="microphone; camera"></iframe>--}}
{{--                        <iframe src="https://us04web.zoom.us/s/71170827269?pwd=bGdDcyswQlRsM1hlZC9BQmNDTVd0dz09" width="1000" height="500" sandbox="allow-same-origin allow-forms allow-scripts" allow="microphone; camera"></iframe>--}}
{{--                        <iframe src="https://zoom.us/wc/71170827269/join?pwd=bGdDcyswQlRsM1hlZC9BQmNDTVd0dz09" width="1000" height="500" sandbox="allow-same-origin allow-forms allow-scripts" allow="microphone; camera"></iframe>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--    </div>--}}
    <script>
        {{--var zoomConfig = JSON.parse('{{ collect(config('zoom-api'))->toJson() }}');--}}
        var zoomConfig = <?php echo json_encode(config('zoom-api')); ?>;
        var signature = '{{ $this->signature }}';
        var meetingNumber = {{ $this->meeting_number }};
        var meetingPassword = '{{ $this->password }}';
        var meetingRole = {{ $this->role }};
    </script>
</div>

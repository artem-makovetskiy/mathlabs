<div>
    <div class="content">
        <div class="content__container">
            <div class="content__col content__col--left">
                @livewire('admin.sidebar-nav', ['active_page' => 'all_webinars'])
            </div>
            <div class="content__col content__col--right">
                <div class="content-header content-header--online-classes">
                    <div class="content-header__container px-4">
                        <div class="flex justify-between items-center px-2 py-4">
                            <div class="hidden w-1/2 flex rounded-md shadow-sm">
                                <div class="relative flex-grow focus-within:z-10">
                                    <div class="absolute inset-y-0 left-0 pl-3 flex items-center pointer-events-none">
                                        <svg class="h-5 w-5 text-gray-400" fill="none" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" viewBox="0 0 24 24" class="w-8 h-8"><path d="M21 21l-6-6m2-5a7 7 0 11-14 0 7 7 0 0114 0z"></path></svg>
                                    </div>
                                    <input id="email" class="form-input block w-full rounded-none rounded-l-md pl-10 transition ease-in-out duration-150 sm:text-sm sm:leading-5" placeholder="Search..." />
                                </div>
                                <button class="-ml-px relative inline-flex items-center px-4 py-2 border border-gray-300 text-sm leading-5 font-medium rounded-r-md text-gray-700 bg-gray-50 hover:text-gray-500 hover:bg-white focus:outline-none focus:shadow-outline-blue focus:border-blue-300 active:bg-gray-100 active:text-gray-700 transition ease-in-out duration-150">
                                    <svg class="h-5 w-5 text-gray-400" fill="currentColor" viewBox="0 0 20 20"><path fill-rule="evenodd" d="M3 3a1 1 0 011-1h12a1 1 0 011 1v3a1 1 0 01-.293.707L12 11.414V15a1 1 0 01-.293.707l-2 2A1 1 0 018 17v-5.586L3.293 6.707A1 1 0 013 6V3z" clip-rule="evenodd"></path></svg>
                                    <span class="ml-2">Filter</span>
                                </button>
                            </div>
                            <a href="{{ route('admin.webinars.create') }}" class="item__btn--blue flex space-x-2 w-auto">
                                <svg class="-ml-1 h-4 w-4" fill="none" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" viewBox="0 0 24 24"><path d="M12 4v16m8-8H4"></path></svg>
                                <span class="leading-none">Создать занятие</span>
                            </a>
                            <a href="{{ route('admin.webinars.create') }}" class="hidden inline-flex items-center px-4 py-2 border border-transparent text-sm font-medium rounded-md text-white bg-indigo-600 hover:bg-indigo-500 focus:outline-none focus:shadow-outline-indigo focus:border-indigo-700 active:bg-indigo-700 transition duration-150 ease-in-out">
                                <svg class="-ml-1 mr-2 h-4 w-4" fill="none" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" viewBox="0 0 24 24"><path d="M12 4v16m8-8H4"></path></svg>
                                Создать занятие
                            </a>
                        </div>
                    </div>
                </div>
                <div class="panel">
                    <table class="min-w-full">
                        <thead>
                        <tr>
                            <th class="px-6 py-3 border-b border-gray-200 bg-gray-50 text-left text-xs leading-4 font-medium text-gray-500 uppercase tracking-wider">
                                Название
                            </th>
                            <th class="px-6 py-3 border-b border-gray-200 bg-gray-50 text-left text-xs leading-4 font-medium text-gray-500 uppercase tracking-wider">
                                Цена
                            </th>
                            <th class="px-6 py-3 border-b border-gray-200 bg-gray-50 text-left text-xs leading-4 font-medium text-gray-500 uppercase tracking-wider">
                                Класс
                            </th>
                            <th class="px-6 py-3 border-b border-gray-200 bg-gray-50"></th>
                        </tr>
                        </thead>
                        <tbody class="bg-white">
                        @forelse($this->webinars as $webinar)
                            <tr>
                                <td class="px-8 py-6 whitespace-no-wrap border-b border-gray-200 text-sm leading-5 font-medium text-gray-900">
                                    {{ $webinar->title }}
                                </td>
                                <td class="px-8 py-6 whitespace-no-wrap border-b border-gray-200 text-sm leading-5 text-gray-500">
                                    {{ $webinar->price }}
                                </td>
                                <td class="px-8 py-6 whitespace-no-wrap border-b border-gray-200 text-sm leading-5 text-gray-500">
                                    {{ $webinar->grade }}
                                </td>
                                <td class="px-8 py-6 whitespace-no-wrap text-right border-b border-gray-200 text-sm leading-5 font-medium">
                                    <a href="{{ route('admin.webinars.schedule', $webinar->id) }}" class="mr-4 text-indigo-600 hover:text-indigo-900 focus:outline-none focus:underline">Запланировать</a>
                                    <a href="{{ route('admin.webinars.edit', $webinar->id) }}" class="text-indigo-600 hover:text-indigo-900 focus:outline-none focus:underline">Изменить</a>
                                </td>
                            </tr>
                        @empty
                            <tr><td class="px-8 py-6 whitespace-no-wrap border-b border-gray-200 text-sm leading-5 font-medium text-gray-900"></td><td class="px-8 py-6 whitespace-no-wrap border-b border-gray-200 text-sm leading-5 font-medium text-gray-900"></td><td class="px-8 py-6 whitespace-no-wrap border-b border-gray-200 text-sm leading-5 font-medium text-gray-900"></td></tr>
                        @endforelse
                        </tbody>
                    </table>
                    {{ $this->webinars->links() }}
                </div>
            </div>
        </div>
    </div>
</div>

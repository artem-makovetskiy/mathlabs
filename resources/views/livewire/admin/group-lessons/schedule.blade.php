<div>
    <div class="content">
        <div class="content__container">
            <div class="content__col content__col--left">
                @livewire('admin.sidebar-nav', [])
            </div>
            <div class="content__col content__col--right">
                <div class="hidden content-header content-header--online-classes">
                    <div class="content-header__container">
                        <div class="px-6 py-5">
                            <h3 class="text-2xl font-bold">Запланировать интенсив</h3>
                        </div>
                    </div>
                </div>
                <div class="panel">
                    <div class="px-6 py-4">
                        <div class="stack stack-lg">
                            <h3 class="text-2xl font-bold">Запланировать занятие</h3>
                            @if (session()->has('message'))
                                <div class="alert alert-success flex items-center bg-green-100 text-green-500 rounded p-4">
                                    <div class="">
                                        <svg fill="none" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" viewBox="0 0 24 24" class="w-8 h-8"><path d="M9 12l2 2 4-4m6 2a9 9 0 11-18 0 9 9 0 0118 0z"></path></svg>
                                    </div>
                                    <div class="ml-3">{{ session('message') }}</div>
                                </div>
                            @endif
                            @if ($errors->any())
                                <div class="alert alert-danger flex items-center bg-red-100 text-red-500 rounded p-4">
                                    <div class="">
                                        <svg fill="none" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" viewBox="0 0 24 24" class="w-8 h-8"><path d="M12 9v2m0 4h.01m-6.938 4h13.856c1.54 0 2.502-1.667 1.732-3L13.732 4c-.77-1.333-2.694-1.333-3.464 0L3.34 16c-.77 1.333.192 3 1.732 3z"></path></svg>
                                    </div>
                                    <ul class="ml-3">
                                        @foreach ($errors->all() as $error)
                                            <li>* {{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif
                            <form wire:submit.prevent="submitForm" wire:key="form">
                                <div>
                                    <div>
                                        <div class="mt-6 grid grid-cols-1 row-gap-6 col-gap-4 sm:grid-cols-6">
                                            <div class="sm:col-span-2">
                                                <div class="flex justify-between">
                                                    <label for="topic" class="block text-sm font-medium leading-5 text-gray-700">Название</label>
                                                    <span class="text-sm leading-5 text-gray-500"></span>
                                                </div>
                                                <div class="mt-1 relative rounded-md shadow-sm">
                                                    <input type="text" wire:model.lazy="topic" id="topic" class="form-input block w-full bg-gray-100 sm:text-sm sm:leading-5" placeholder="" />
                                                </div>
                                            </div>
                                            <div class="sm:col-span-2">
                                                <label for="meeting_date" class="block text-sm font-medium leading-5 text-gray-700">Дата начала</label>
                                                <div class="mt-1 relative rounded-md shadow-sm">
                                                    <div class="absolute inset-y-0 left-0 pl-3 flex items-center pointer-events-none">
                                                        <svg class="h-5 w-5 text-gray-400" fill="none" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" viewBox="0 0 24 24" stroke="currentColor"><path d="M8 7V3m8 4V3m-9 8h10M5 21h14a2 2 0 002-2V7a2 2 0 00-2-2H5a2 2 0 00-2 2v12a2 2 0 002 2z"></path></svg>
                                                    </div>
                                                    <input wire:model.lazy="meeting_date_start" id="meeting_date" class="form-input block w-full pl-10 bg-gray-100 sm:text-sm sm:leading-5" placeholder="DD/MM/YYYY" />
                                                </div>
                                            </div>
                                            <div class="sm:col-span-1">
                                                <label for="weekday" class="block text-sm font-medium leading-5 text-gray-700">
                                                    День недели
                                                </label>
                                                <div class="mt-1 rounded-md shadow-sm">
                                                    <select wire:model="weekday" id="weekday" class="form-select block w-full bg-gray-100 transition duration-150 ease-in-out sm:text-sm sm:leading-5">
                                                        <option value="">-</option>
                                                        @foreach($this->weekdays_options as $option)
                                                            <option value="{{ $option }}">{{ $option }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="sm:col-span-1">
                                                <label for="weekday" class="block text-sm font-medium leading-5 text-gray-700">
                                                    Кол-во занятий
                                                </label>
                                                <div class="mt-1 rounded-md shadow-sm">
                                                    <select wire:model="recurrence_options.end_times" id="end_times" class="form-select block w-full bg-gray-100 transition duration-150 ease-in-out sm:text-sm sm:leading-5">
                                                        <option value="">-</option>
                                                        @foreach($this->num_of_lessons_opts as $option)
                                                            <option value="{{ $option }}">{{ $option }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                            <!-- <div class="sm:col-span-2">
                                                <label for="meeting_date" class="block text-sm font-medium leading-5 text-gray-700">Дата конца</label>
                                                <div class="mt-1 relative rounded-md shadow-sm">
                                                    <div class="absolute inset-y-0 left-0 pl-3 flex items-center pointer-events-none">
                                                        <svg class="h-5 w-5 text-gray-400" fill="none" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" viewBox="0 0 24 24" stroke="currentColor"><path d="M8 7V3m8 4V3m-9 8h10M5 21h14a2 2 0 002-2V7a2 2 0 00-2-2H5a2 2 0 00-2 2v12a2 2 0 002 2z"></path></svg>
                                                    </div>
                                                    <input wire:model.lazy="meeting_date_end" id="meeting_date" class="form-input block w-full pl-10 bg-gray-100 sm:text-sm sm:leading-5" placeholder="DD/MM/YYYY" />
                                                </div>
                                            </div> -->
                                        </div>
                                        @if(count($meetings) > 0)
                                            <div class="grid grid-cols-1 row-gap-3 sm:grid-cols-6 mt-6">
                                                <div class="sm:col-span-6">
                                                    <h3 class="text-xl font-semibold mb-4">Запланированные занятия</h3>
                                                </div>
                                                @foreach ($meetings as $meeting)
                                                <div class="flex items-center sm:col-span-2 mb-4{{ !$loop->last ? ' border-b border-gray-200' : '' }}">
                                                    <div class="text-lg px-4 py-4">{{ $meeting['topic'] }}</div>
                                                </div>
                                                <div class="flex items-center sm:col-span-2 mb-4{{ !$loop->last ? ' border-b border-gray-200' : '' }}">
                                                    <div class="px-4 py-4">{{ $meeting['meeting_time'] }}</div>
                                                </div>
                                                <div class="flex items-center justify-end sm:col-span-2 sm:col-start-5 text-right px-4 py-4 mb-4{{ !$loop->last ? ' border-b border-gray-200' : '' }}">
                                                    <a href="{{ route('account.streamLesson', [$lesson->type, $lesson->uuid, $meeting['meeting_id']]) }}" class="item__btn--blue-empty px-4 py-2 text-sm w-auto">Join Link</a>
                                                </div>
                                                @endforeach
                                            </div>
                                        @endif
                                    </div>
                                </div>
                                <div class="mt-8 border-t border-gray-200 pt-5">
                                    <div class="flex justify-end">
                                        <a class="item__btn--blue-empty w-auto" href="{{ route('admin.group-lessons.index') }}">Отмена</a>
                                        <button class="item__btn--blue w-auto ml-3">
                                            <svg wire:loading wire:target="submitForm" class="hidden text-white w-4 h-4 icon--is-spinning mr-2" aria-hidden="true" viewBox="0 0 16 16"><title>Loading</title><g stroke-width="1" fill="currentColor" stroke="currentColor"><path d="M.5,8a7.5,7.5,0,1,1,1.91,5" fill="none" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round"></path></g></svg>
                                            <span>Сохранить</span>
                                        </button>

                                        <span class="hidden inline-flex rounded-md shadow-sm">
                                          <button type="button" class="py-2 px-4 border border-gray-300 rounded-md text-sm leading-5 font-medium text-gray-700 hover:text-gray-500 focus:outline-none focus:border-blue-300 focus:shadow-outline-blue active:bg-gray-50 active:text-gray-800 transition duration-150 ease-in-out">
                                            Отмена
                                          </button>
                                        </span>
                                        <span class="hidden ml-3 inline-flex rounded-md shadow-sm">
                                          <button type="submit" class="inline-flex justify-center items-center py-2 px-4 border border-transparent text-sm leading-5 font-medium rounded-md text-white bg-indigo-600 hover:bg-indigo-500 focus:outline-none focus:border-indigo-700 focus:shadow-outline-indigo active:bg-indigo-700 transition duration-150 ease-in-out">
                                              <svg wire:loading wire:target="submitForm" class="hidden text-white w-4 h-4 icon--is-spinning mr-2" aria-hidden="true" viewBox="0 0 16 16"><title>Loading</title><g stroke-width="1" fill="currentColor" stroke="currentColor"><path d="M.5,8a7.5,7.5,0,1,1,1.91,5" fill="none" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round"></path></g></svg>
                                              <span>Сохранить</span>
                                          </button>
                                        </span>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.26.0/moment.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/flatpickr"></script>
    <script src="https://npmcdn.com/flatpickr/dist/l10n/ru.js"></script>
    <script>
        (function (flatpickr) {
            flatpickr("#meeting_date", {
                enableTime: true,
                dateFormat: '{{ $this->meetingdateformat }}',
                "locale": "ru"
            });
        })(flatpickr);
    </script>
</div>

<div>
    <div class="content">
        <div class="content__container">
            <div class="content__col content__col--left">
                @livewire('admin.sidebar-nav', [])
            </div>
            <div class="content__col content__col--right">
                <h3 class="text-2xl font-bold pt-2">Записи на занятия</h3>
                <div class="flex items-center justify-end mb-2">
                    <div x-data="{ open: false }" @keydown.window.escape="open = false" @click.away="open = false" class="relative inline-block text-left">
                        <div>
                            <span class="rounded-md shadow-sm">
                            <button @click="open = !open" type="button" class="inline-flex justify-center w-full rounded-md border border-gray-300 px-4 pt-2 pb-1 bg-white text-sm leading-5 font-medium text-gray-700 hover:text-gray-500 focus:outline-none focus:border-blue-300 focus:shadow-outline-blue active:bg-gray-50 active:text-gray-800 transition ease-in-out duration-150">
                                <div class="w-5 h-5 mr-2">
                                    <svg fill="none" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" viewBox="0 0 24 24" stroke="currentColor"><path d="M3 4h13M3 8h9m-9 4h6m4 0l4-4m0 0l4 4m-4-4v12"></path></svg>
                                </div>
                                Сортировать по
                                <svg class="-mr-1 ml-2 h-5 w-5" fill="currentColor" viewBox="0 0 20 20">
                                <path fill-rule="evenodd" d="M5.293 7.293a1 1 0 011.414 0L10 10.586l3.293-3.293a1 1 0 111.414 1.414l-4 4a1 1 0 01-1.414 0l-4-4a1 1 0 010-1.414z" clip-rule="evenodd"/>
                                </svg>
                            </button>
                            </span>
                        </div>
                        <div x-show="open" style="display: none;" x-transition:enter="transition ease-out duration-100" x-transition:enter-start="transform opacity-0 scale-95" x-transition:enter-end="transform opacity-100 scale-100" x-transition:leave="transition ease-in duration-75" x-transition:leave-start="transform opacity-100 scale-100" x-transition:leave-end="transform opacity-0 scale-95" class="origin-top-right absolute right-0 mt-2 w-56 rounded-md shadow-lg z-10">
                            <div class="rounded-md bg-white shadow-xs">
                            <div class="py-1">
                                <a wire:click="changeSortOrder('lesson')" href="#" class="block relative px-4 py-2 text-sm leading-5 hover:bg-gray-100 hover:text-gray-900 focus:outline-none{{ $sort_by === 'lesson' ? ' text-white bg-primary' : ' text-gray-700' }}">
                                    Занятие
                                    <span
                                        class="absolute inset-y-0 right-0 flex items-center pr-4{{ $sort_by === 'lesson' ? '' : ' hidden' }}">
                                        <svg class="h-5 w-5" viewBox="0 0 20 20" fill="currentColor">
                                            <path fill-rule="evenodd"
                                                d="M16.707 5.293a1 1 0 010 1.414l-8 8a1 1 0 01-1.414 0l-4-4a1 1 0 011.414-1.414L8 12.586l7.293-7.293a1 1 0 011.414 0z"
                                                clip-rule="evenodd" />
                                        </svg>
                                    </span>
                                </a>
                                <a wire:click="changeSortOrder('created_at')" href="#" class="block relative px-4 py-2 text-sm leading-5 hover:bg-gray-100 hover:text-gray-900 focus:outline-none{{ $sort_by === 'created_at' ? ' text-white bg-primary' : ' text-gray-700' }}">
                                    Время записи
                                    <span
                                        class="absolute inset-y-0 right-0 flex items-center pr-4{{ $sort_by === 'created_at' ? '' : ' hidden' }}">
                                        <svg class="h-5 w-5" viewBox="0 0 20 20" fill="currentColor">
                                            <path fill-rule="evenodd"
                                                d="M16.707 5.293a1 1 0 010 1.414l-8 8a1 1 0 01-1.414 0l-4-4a1 1 0 011.414-1.414L8 12.586l7.293-7.293a1 1 0 011.414 0z"
                                                clip-rule="evenodd" />
                                        </svg>
                                    </span>
                                </a>
                            </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="panel mb-6">
                    <div class="">
                        <div class="stack stack-lg">
                            @if (session()->has('message'))
                                <div class="alert alert-success flex items-center bg-green-100 text-green-500 rounded p-4">
                                    <div class="">
                                        <svg fill="none" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" viewBox="0 0 24 24" class="w-8 h-8"><path d="M9 12l2 2 4-4m6 2a9 9 0 11-18 0 9 9 0 0118 0z"></path></svg>
                                    </div>
                                    <div class="ml-3">{{ session('message') }}</div>
                                </div>
                            @endif
                            @if ($errors->any())
                                <div class="alert alert-danger flex items-center bg-red-100 text-red-500 rounded p-4">
                                    <div class="">
                                        <svg fill="none" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" viewBox="0 0 24 24" class="w-8 h-8"><path d="M12 9v2m0 4h.01m-6.938 4h13.856c1.54 0 2.502-1.667 1.732-3L13.732 4c-.77-1.333-2.694-1.333-3.464 0L3.34 16c-.77 1.333.192 3 1.732 3z"></path></svg>
                                    </div>
                                    <ul class="ml-3">
                                        @foreach ($errors->all() as $error)
                                            <li>* {{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif
                            <div class="">

                                <table class="min-w-full relative">
                                    <thead>
                                    <tr class="">
                                        <th class="border-b-2 border-gray-100 p-4">
                                            <div class="flex items-center">
                                                <input wire:change="uncheckAll()" id="" type="checkbox" @if(count($selected_items) > 0) checked="true" @endif class="form-checkbox h-5 w-5 text-indigo-600 transition duration-150 ease-in-out" />
                                            </div>
                                        </th>
                                        <th class="border-b-2 border-gray-100 p-4 text-left text-xs leading-4 font-medium text-gray-500 uppercase tracking-wider">
                                            Имя ученика
                                        </th>
                                        <th class="border-b-2 border-gray-100 p-4 text-left text-xs leading-4 font-medium text-gray-500 uppercase tracking-wider">
                                            Занятие
                                        </th>
                                        <th class="border-b-2 border-gray-100 p-4 text-left text-xs leading-4 font-medium text-gray-500 uppercase tracking-wider">
                                            Время занятий
                                        </th>
                                        <th class="border-b-2 border-gray-100 p-4 text-right text-xs leading-4 font-medium text-gray-500 uppercase tracking-wider">Время записи</th>
                                    </tr>
                                    @if(count($selected_items) > 0)
                                    <tr class="absolute top-0 left-16 w-full bg-white py-2">
                                        <th>
                                            <span class="relative z-0 inline-flex shadow-sm">
                                            <button type="button" class="relative inline-flex items-center px-4 py-1.5 cursor-default select-none rounded-l-md border border-gray-300 bg-white text-sm leading-5 font-medium text-gray-700 focus:outline-none">
                                                {{ count($selected_items) }} выбрано
                                            </button>
                                            <button wire:click="createGroup()" type="button" class="-ml-px relative inline-flex items-center px-4 py-1.5 rounded-r-md border border-transparent bg-primary text-sm leading-5 font-medium text-white hover:text-primary-200 focus:z-10 focus:outline-none focus:border-blue-300 focus:shadow-outline-blue active:bg-primary-100 active:text-primary-700 transition ease-in-out duration-150">
                                                Добавить в группу
                                            </button>
                                            </span>
                                        </th>
                                    </tr>
                                    @endif
                                    </thead>
                                    <tbody class="bg-white">
                                    @foreach($this->grouplessonpurchases as $purchase)
                                        <tr>
                                            <td class="p-4 border-b border-gray-100">
                                                <div class="flex items-center">
                                                    <input wire:change="addItem({{ $purchase->id }})" id="" type="checkbox" class="form-checkbox h-5 w-5 text-indigo-600 transition duration-150 ease-in-out" {{ collect($selected_items)->search($purchase->id) !== false ? 'checked' : '' }}/>
                                                </div>
                                            </td>
                                            <td class="p-4 border-b border-gray-100 whitespace-no-wrap text-sm leading-5 font-medium text-gray-900">
                                                <div class="flex items-center space-x-2">
                                                    <div class="flex-shrink-0">
                                                        <img class="h-6 w-6 rounded-full" src="http://mathlabs.test/img/icons/icon-avatar-example.svg" alt="" />
                                                    </div>
                                                    <span class="leading-tight">{{ $purchase->user->child_name }}, {{ $purchase->user->grade }} класс</span>
                                                </div>
                                            </td>
                                            <td class="p-4 border-b border-gray-100 whitespace-no-wrap text-sm leading-5 text-gray-500">
                                                <a class="font-semibold underline" href="{{ route('admin.group-lessons.edit', $purchase->lesson->id) }}">{{ $purchase->lesson->title }}</a>
                                            </td>
                                            <td class="p-4 border-b border-gray-100 whitespace-no-wrap text-sm leading-5 text-gray-500">
                                                {{ collect($purchase->scheduled_time['weekdays'])->join(', ') }}:
                                                {{ collect($purchase->scheduled_time['time'])->join(', ') }}
                                            </td>
                                            <td class="p-4 border-b border-gray-100 whitespace-no-wrap text-right text-sm leading-5 text-gray-500">
                                                {{ $purchase->created_at->format('d.m.Y H:s') }}
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>

                                <!-- <div class="flex items-center border-b-2 border-gray-100 p-4">
                                    <div class="flex items-center">
                                        <input id="" type="checkbox" class="form-checkbox form-checkbox--unselect h-5 w-5 text-indigo-600 transition duration-150 ease-in-out" />
                                    </div>
                                    <span class="ml-4">2 selected</span>
                                </div>
                                @foreach($this->grouplessonpurchases as $purchase)
                                    <div class="flex items-center space-x-4 p-4 border-b border-gray-100">
                                        <div class="flex items-center">
                                            <input id="" type="checkbox" class="form-checkbox h-5 w-5 text-indigo-600 transition duration-150 ease-in-out" />
                                        </div>
                                        <div class="flex items-center space-x-2">
                                            <div class="flex-shrink-0">
                                                <img class="h-6 w-6 rounded-full" src="http://mathlabs.test/img/icons/icon-avatar-example.svg" alt="" />
                                            </div>
                                            <span class="">{{ $purchase->user->child_name }}</span>
                                        </div>
                                        <div class="px-8"><a class="font-semibold underline" href="{{ route('admin.group-lessons.edit', $purchase->lesson->id) }}">{{ $purchase->lesson->title }}</a></div>
                                        <div class="px-6">
                                            {{ collect($purchase->scheduled_time['weekdays'])->join(', ') }}:
                                            {{ collect($purchase->scheduled_time['time'])->join(', ') }}
                                        </div>
                                    </div>
                                @endforeach
                                <div class="flex items-center space-x-4 p-4 border-b border-gray-100">
                                    <div class="flex items-center">
                                        <input id="" type="checkbox" class="form-checkbox h-5 w-5 text-indigo-600 transition duration-150 ease-in-out" />
                                    </div>
                                    <div class="flex-shrink-0">
                                        <img class="h-6 w-6 rounded-full" src="http://mathlabs.test/img/icons/icon-avatar-example.svg" alt="" />
                                    </div>
                                    <span class=""></span>
                                </div>
                                <div class="flex items-center p-4 border-b border-gray-100">
                                    <div class="flex items-center">
                                        <input id="" type="checkbox" class="form-checkbox h-5 w-5 text-indigo-600 transition duration-150 ease-in-out" />
                                    </div>
                                    <span class="ml-4">Line</span>
                                </div>
                                <div class="flex items-center p-4 border-b border-gray-100">
                                    <div class="flex items-center">
                                        <input id="" type="checkbox" class="form-checkbox h-5 w-5 text-indigo-600 transition duration-150 ease-in-out" />
                                    </div>
                                    <span class="ml-4">Line</span>
                                </div>
                                <div class="flex items-center p-4 border-b border-gray-100">
                                    <div class="flex items-center">
                                        <input id="" type="checkbox" class="form-checkbox h-5 w-5 text-indigo-600 transition duration-150 ease-in-out" />
                                    </div>
                                    <span class="ml-4">Line</span>
                                </div> -->
                            </div>
                        </div>
                    </div>
                </div>
                <div class="hidden bg-white shadow overflow-hidden sm:rounded-md">
                    <ul>
                        <li>
                        <a href="#" class="block hover:bg-gray-50 focus:outline-none focus:bg-gray-50 transition duration-150 ease-in-out">
                            <div class="px-4 py-4 flex items-center sm:px-6">
                            <div class="min-w-0 flex-1 sm:flex sm:items-center sm:justify-between">
                                <div>
                                <div class="text-sm leading-5 font-medium text-indigo-600 truncate">
                                    Back End Developer
                                    <span class="ml-1 font-normal text-gray-500">
                                    in Engineering
                                    </span>
                                </div>
                                <div class="mt-2 flex">
                                    <div class="flex items-center text-sm leading-5 text-gray-500">
                                    <svg class="flex-shrink-0 mr-1.5 h-5 w-5 text-gray-400" fill="currentColor" viewBox="0 0 20 20">
                                        <path fill-rule="evenodd" d="M6 2a1 1 0 00-1 1v1H4a2 2 0 00-2 2v10a2 2 0 002 2h12a2 2 0 002-2V6a2 2 0 00-2-2h-1V3a1 1 0 10-2 0v1H7V3a1 1 0 00-1-1zm0 5a1 1 0 000 2h8a1 1 0 100-2H6z" clip-rule="evenodd"/>
                                    </svg>
                                    <span>
                                        Closing on
                                        <time datetime="2020-01-07">January 7, 2020</time>
                                    </span>
                                    </div>
                                </div>
                                </div>
                                <div class="mt-4 flex-shrink-0 sm:mt-0">
                                <div class="flex overflow-hidden">
                                    <img class="inline-block h-6 w-6 rounded-full text-white shadow-solid" src="https://images.unsplash.com/photo-1491528323818-fdd1faba62cc?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=facearea&facepad=2&w=256&h=256&q=80" alt="" />
                                    <img class="-ml-1 inline-block h-6 w-6 rounded-full text-white shadow-solid" src="https://images.unsplash.com/photo-1550525811-e5869dd03032?ixlib=rb-1.2.1&auto=format&fit=facearea&facepad=2&w=256&h=256&q=80" alt="" />
                                    <img class="-ml-1 inline-block h-6 w-6 rounded-full text-white shadow-solid" src="https://images.unsplash.com/photo-1500648767791-00dcc994a43e?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=facearea&facepad=2.25&w=256&h=256&q=80" alt="" />
                                    <img class="-ml-1 inline-block h-6 w-6 rounded-full text-white shadow-solid" src="https://images.unsplash.com/photo-1472099645785-5658abf4ff4e?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=facearea&facepad=2&w=256&h=256&q=80" alt="" />
                                </div>
                                </div>
                            </div>
                            <div class="ml-5 flex-shrink-0">
                                <svg class="h-5 w-5 text-gray-400" fill="currentColor" viewBox="0 0 20 20">
                                <path fill-rule="evenodd" d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z" clip-rule="evenodd"/>
                                </svg>
                            </div>
                            </div>
                        </a>
                        </li>
                        <li class="border-t border-gray-200">
                        <a href="#" class="block hover:bg-gray-50 focus:outline-none focus:bg-gray-50 transition duration-150 ease-in-out">
                            <div class="px-4 py-4 flex items-center sm:px-6">
                            <div class="min-w-0 flex-1 sm:flex sm:items-center sm:justify-between">
                                <div>
                                <div class="text-sm leading-5 font-medium text-indigo-600 truncate">
                                    Back End Developer
                                    <span class="ml-1 font-normal text-gray-500">
                                    in Engineering
                                    </span>
                                </div>
                                <div class="mt-2 flex">
                                    <div class="flex items-center text-sm leading-5 text-gray-500">
                                    <svg class="flex-shrink-0 mr-1.5 h-5 w-5 text-gray-400" fill="currentColor" viewBox="0 0 20 20">
                                        <path fill-rule="evenodd" d="M6 2a1 1 0 00-1 1v1H4a2 2 0 00-2 2v10a2 2 0 002 2h12a2 2 0 002-2V6a2 2 0 00-2-2h-1V3a1 1 0 10-2 0v1H7V3a1 1 0 00-1-1zm0 5a1 1 0 000 2h8a1 1 0 100-2H6z" clip-rule="evenodd"/>
                                    </svg>
                                    <span>
                                        Closing on
                                        <time datetime="2020-01-07">January 7, 2020</time>
                                    </span>
                                    </div>
                                </div>
                                </div>
                                <div class="mt-4 flex-shrink-0 sm:mt-0">
                                <div class="flex overflow-hidden">
                                    <img class="inline-block h-6 w-6 rounded-full text-white shadow-solid" src="https://images.unsplash.com/photo-1491528323818-fdd1faba62cc?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=facearea&facepad=2&w=256&h=256&q=80" alt="" />
                                    <img class="-ml-1 inline-block h-6 w-6 rounded-full text-white shadow-solid" src="https://images.unsplash.com/photo-1550525811-e5869dd03032?ixlib=rb-1.2.1&auto=format&fit=facearea&facepad=2&w=256&h=256&q=80" alt="" />
                                    <img class="-ml-1 inline-block h-6 w-6 rounded-full text-white shadow-solid" src="https://images.unsplash.com/photo-1500648767791-00dcc994a43e?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=facearea&facepad=2.25&w=256&h=256&q=80" alt="" />
                                    <img class="-ml-1 inline-block h-6 w-6 rounded-full text-white shadow-solid" src="https://images.unsplash.com/photo-1472099645785-5658abf4ff4e?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=facearea&facepad=2&w=256&h=256&q=80" alt="" />
                                </div>
                                </div>
                            </div>
                            <div class="ml-5 flex-shrink-0">
                                <svg class="h-5 w-5 text-gray-400" fill="currentColor" viewBox="0 0 20 20">
                                <path fill-rule="evenodd" d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z" clip-rule="evenodd"/>
                                </svg>
                            </div>
                            </div>
                        </a>
                        </li>
                        <li class="border-t border-gray-200">
                        <a href="#" class="block hover:bg-gray-50 focus:outline-none focus:bg-gray-50 transition duration-150 ease-in-out">
                            <div class="px-4 py-4 flex items-center sm:px-6">
                            <div class="min-w-0 flex-1 sm:flex sm:items-center sm:justify-between">
                                <div>
                                <div class="text-sm leading-5 font-medium text-indigo-600 truncate">
                                    Back End Developer
                                    <span class="ml-1 font-normal text-gray-500">
                                    in Engineering
                                    </span>
                                </div>
                                <div class="mt-2 flex">
                                    <div class="flex items-center text-sm leading-5 text-gray-500">
                                    <svg class="flex-shrink-0 mr-1.5 h-5 w-5 text-gray-400" fill="currentColor" viewBox="0 0 20 20">
                                        <path fill-rule="evenodd" d="M6 2a1 1 0 00-1 1v1H4a2 2 0 00-2 2v10a2 2 0 002 2h12a2 2 0 002-2V6a2 2 0 00-2-2h-1V3a1 1 0 10-2 0v1H7V3a1 1 0 00-1-1zm0 5a1 1 0 000 2h8a1 1 0 100-2H6z" clip-rule="evenodd"/>
                                    </svg>
                                    <span>
                                        Closing on
                                        <time datetime="2020-01-07">January 7, 2020</time>
                                    </span>
                                    </div>
                                </div>
                                </div>
                                <div class="mt-4 flex-shrink-0 sm:mt-0">
                                <div class="flex overflow-hidden">
                                    <img class="inline-block h-6 w-6 rounded-full text-white shadow-solid" src="https://images.unsplash.com/photo-1491528323818-fdd1faba62cc?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=facearea&facepad=2&w=256&h=256&q=80" alt="" />
                                    <img class="-ml-1 inline-block h-6 w-6 rounded-full text-white shadow-solid" src="https://images.unsplash.com/photo-1550525811-e5869dd03032?ixlib=rb-1.2.1&auto=format&fit=facearea&facepad=2&w=256&h=256&q=80" alt="" />
                                    <img class="-ml-1 inline-block h-6 w-6 rounded-full text-white shadow-solid" src="https://images.unsplash.com/photo-1500648767791-00dcc994a43e?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=facearea&facepad=2.25&w=256&h=256&q=80" alt="" />
                                    <img class="-ml-1 inline-block h-6 w-6 rounded-full text-white shadow-solid" src="https://images.unsplash.com/photo-1472099645785-5658abf4ff4e?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=facearea&facepad=2&w=256&h=256&q=80" alt="" />
                                </div>
                                </div>
                            </div>
                            <div class="ml-5 flex-shrink-0">
                                <svg class="h-5 w-5 text-gray-400" fill="currentColor" viewBox="0 0 20 20">
                                <path fill-rule="evenodd" d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z" clip-rule="evenodd"/>
                                </svg>
                            </div>
                            </div>
                        </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
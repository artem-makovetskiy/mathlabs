<div>
    <div class="content">
        <div class="content__container">
            <div class="content__col content__col--left">
                @livewire('admin.sidebar-nav', [])
            </div>
            <div class="content__col content__col--right">
                <div class="hidden content-header content-header--online-classes">
                    <div class="content-header__container">
                        <div class="px-6 py-5">
                            <h3 class="text-2xl font-bold">Редактировать групповое занятие</h3>
                        </div>
                    </div>
                </div>
                <div class="panel">
                    <div class="px-6 py-4">
                        <div class="stack stack-lg">
                            <h3 class="text-2xl font-bold">Редактировать групповое занятие</h3>
                            @if (session()->has('message'))
                                <div class="alert alert-success flex items-center bg-green-100 text-green-500 rounded p-4">
                                    <div class="">
                                        <svg fill="none" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" viewBox="0 0 24 24" class="w-8 h-8"><path d="M9 12l2 2 4-4m6 2a9 9 0 11-18 0 9 9 0 0118 0z"></path></svg>
                                    </div>
                                    <div class="ml-3">{{ session('message') }}</div>
                                </div>
                            @endif
                            @if ($errors->any())
                                <div class="alert alert-danger flex items-center bg-red-100 text-red-500 rounded p-4">
                                    <div class="">
                                        <svg fill="none" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" viewBox="0 0 24 24" class="w-8 h-8"><path d="M12 9v2m0 4h.01m-6.938 4h13.856c1.54 0 2.502-1.667 1.732-3L13.732 4c-.77-1.333-2.694-1.333-3.464 0L3.34 16c-.77 1.333.192 3 1.732 3z"></path></svg>
                                    </div>
                                    <ul class="ml-3">
                                        @foreach ($errors->all() as $error)
                                            <li>* {{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif
                            <form wire:submit.prevent="updateLesson" wire:key="form">
                                <div>
                                    <div>
                                        <div class="mt-6 grid grid-cols-1 row-gap-6 col-gap-4 sm:grid-cols-6">
                                            <div class="sm:col-span-2">
                                                <div class="flex justify-between">
                                                    <label for="title" class="block text-sm font-medium leading-5 text-gray-700">Название</label>
                                                    <span class="text-sm leading-5 text-gray-500">*обязательно</span>
                                                </div>
                                                <div class="mt-1 relative rounded-md shadow-sm">
                                                    <input wire:model.lazy="groupLessonData.title" id="title" class="form-input block w-full bg-gray-100 sm:text-sm sm:leading-5" placeholder="" />
                                                </div>
                                            </div>
                                            <div class="sm:col-span-1">
                                                <label for="price" class="block text-sm font-medium leading-5 text-gray-700">Цена</label>
                                                <div class="mt-1 relative rounded-md shadow-sm">
                                                    <div class="absolute inset-y-0 left-0 pl-3 flex items-center pointer-events-none">
                                                  <span class="text-gray-500 sm:text-sm sm:leading-5">
                                                    $
                                                  </span>
                                                    </div>
                                                    <input wire:model.lazy="groupLessonData.price" id="price" class="form-input block w-full pl-7 pr-12 bg-gray-100 sm:text-sm sm:leading-5" placeholder="0.00" />
                                                    <div class="absolute inset-y-0 right-0 pr-3 flex items-center pointer-events-none">
                                                      <span class="text-gray-500 sm:text-sm sm:leading-5">
                                                        RU
                                                      </span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="sm:col-span-1">
                                                <label for="grade" class="block text-sm font-medium leading-5 text-gray-700">
                                                    Класс
                                                </label>
                                                <div class="mt-1 rounded-md shadow-sm">
                                                    <select wire:model.lazy="groupLessonData.grade" id="grade" class="form-select block w-full bg-gray-100 transition duration-150 ease-in-out sm:text-sm sm:leading-5">
                                                        <option value="">-</option>
                                                        @foreach($this->gradeOptions as $option)
                                                            <option value="{{ $option }}">{{ $option }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="sm:col-span-4">
                                                <div class="flex justify-between">
                                                    <label for="brief_desc" class="block text-sm font-medium leading-5 text-gray-700">Описание</label>
                                                    <span class="text-sm leading-5 text-gray-500"></span>
                                                </div>
                                                <div class="mt-1 rounded-md shadow-sm">
                                                    <textarea wire:model.lazy="groupLessonData.brief_desc" id="brief_desc" rows="3" class="form-textarea block w-full bg-gray-100 transition duration-150 ease-in-out sm:text-sm sm:leading-5"></textarea>
                                                </div>
                                                <p class="hidden mt-2 text-sm text-gray-500"></p>
                                            </div>
                                            <div class="sm:col-span-6">
                                                <div class="flex items-center">
                                                    <input wire:model="groupLessonData.individual" id="individual" type="checkbox" class="form-checkbox h-4 w-4 text-indigo-600 transition duration-150 ease-in-out" />
                                                    <label for="individual" class="ml-2 block text-sm leading-tight text-gray-900">
                                                        Индивидуальное занятие
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="mt-8 border-t border-gray-200 pt-5">
                                    <div class="flex justify-end">
                                        <button wire:click="destroyLesson" type="button" class="mr-auto inline-flex items-center justify-center px-4 py-2 border border-transparent font-medium rounded-full text-red-700 bg-red-100 hover:bg-red-50 focus:outline-none focus:border-red-300 focus:shadow-outline-red active:bg-red-200 transition ease-in-out duration-150 sm:text-sm sm:leading-5">
                                            Удалить занятие
                                        </button>
                                        <a class="item__btn--blue-empty w-auto" href="{{ route('admin.group-lessons.index') }}">Отмена</a>
                                        <button class="item__btn--blue w-auto ml-3">
                                            <svg wire:loading wire:target="submitForm" class="hidden text-white w-4 h-4 icon--is-spinning mr-2" aria-hidden="true" viewBox="0 0 16 16"><title>Loading</title><g stroke-width="1" fill="currentColor" stroke="currentColor"><path d="M.5,8a7.5,7.5,0,1,1,1.91,5" fill="none" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round"></path></g></svg>
                                            <span>Обновить</span>
                                        </button>
                                        <span class="hidden inline-flex rounded-md shadow-sm">
                                          <button type="button" class="py-2 px-4 border border-gray-300 rounded-md text-sm leading-5 font-medium text-gray-700 hover:text-gray-500 focus:outline-none focus:border-blue-300 focus:shadow-outline-blue active:bg-gray-50 active:text-gray-800 transition duration-150 ease-in-out">
                                            Отмена
                                          </button>
                                        </span>
                                        <span class="hidden ml-3 inline-flex rounded-md shadow-sm">
                                          <button type="submit" class="inline-flex justify-center items-center py-2 px-4 border border-transparent text-sm leading-5 font-medium rounded-md text-white bg-indigo-600 hover:bg-indigo-500 focus:outline-none focus:border-indigo-700 focus:shadow-outline-indigo active:bg-indigo-700 transition duration-150 ease-in-out">
                                              <svg wire:loading wire:target="updateLesson" class="hidden text-white w-4 h-4 icon--is-spinning mr-2" aria-hidden="true" viewBox="0 0 16 16"><title>Loading</title><g stroke-width="1" fill="currentColor" stroke="currentColor"><path d="M.5,8a7.5,7.5,0,1,1,1.91,5" fill="none" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round"></path></g></svg>
                                              <span>Обновить</span>
                                          </button>
                                        </span>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script src="https://cdn.jsdelivr.net/npm/flatpickr"></script>
    <script src="https://npmcdn.com/flatpickr/dist/l10n/ru.js"></script>
    <script>
        (function (flatpickr) {
            flatpickr("#event_date", {
                enableTime: true,
                dateFormat: "d-m-Y H:i",
                "locale": "ru"
            });
        })(flatpickr);
    </script>
</div>
<div>
    <div class="hidden sm:mx-auto sm:w-full sm:max-w-md">
        <a href="{{ url('/') }}">
            <x-logo class="w-auto h-16 mx-auto text-indigo-600" />
        </a>

        <h2 class="mt-6 text-3xl font-extrabold text-center text-gray-900 leading-9">
            Регистрация
        </h2>

        <p class="mt-2 text-sm text-center text-gray-600 leading-5 max-w">
            Или
            <a href="{{ route('login') }}" class="font-medium text-indigo-600 hover:text-indigo-500 focus:outline-none focus:underline transition ease-in-out duration-150">
                войдите в аккаунт
            </a>
        </p>
    </div>

    <div class="mt-8 sm:mx-auto sm:w-full sm:max-w-md">
        <div class="modal-auth-reg shadow">
            <div class="modal-auth-reg__container">
                <div class="modal-auth-reg__content">
                    <div class="modal-auth-reg__title">Регистрация</div>
                    <p class="mt-2 mb-6 text-sm text-center text-gray-600 leading-5 max-w">
                        Или
                        <a href="{{ route('login') }}" class="font-medium text-indigo-600 hover:text-indigo-500 focus:outline-none focus:underline transition ease-in-out duration-150">
                            войдите в аккаунт
                        </a>
                    </p>
                    <form wire:submit.prevent="register" class="modal-auth-reg__form">
                        <div class="modal-auth-reg__form-row">
                            <input wire:model.lazy="parent_name" type="text" id="parent_name" name="parent_name" required class="modal-auth-reg__input-text" placeholder="Ваше имя">
                            @error('parent_name')
                            <p class="mt-2 text-sm text-red-600">{{ $message }}</p>
                            @enderror
                        </div>
                        <div class="modal-auth-reg__form-row">
                            <input wire:model.lazy="phone_number" type="text" id="phone_number" name="phone_number" required class="modal-auth-reg__input-text modal-auth-reg__input-text--phone" placeholder="+7 (___)___-__-__">
                            @error('phone_number')
                            <p class="mt-2 text-sm text-red-600">{{ $message }}</p>
                            @enderror
                        </div>
                        <div class="modal-auth-reg__form-row">
                            <input wire:model.lazy="email" type="email" id="email" name="email" required class="modal-auth-reg__input-text" placeholder="Ваш e-mail">
                            @error('email')
                            <p class="mt-2 text-sm text-red-600">{{ $message }}</p>
                            @enderror
                        </div>
                        <div class="modal-auth-reg__form-row">
                            <input wire:model.lazy="password" type="password" id="password" name="password" required class="modal-auth-reg__input-text" placeholder="Пароль">
                            @error('password')
                            <p class="mt-2 text-sm text-red-600">{{ $message }}</p>
                            @enderror
                        </div>
                        <div class="modal-auth-reg__form-row modal-auth-reg__form-row--2-col">
                            <input wire:model.lazy="child_name" type="text" id="child_name" name="child_name" required class="modal-auth-reg__input-text" placeholder="Имя ребенка">
                            <div class="modal-auth-reg__select-wrapper">
                                <select wire:model.lazy="grade" required name="grade" id="grade" class="modal-auth-reg__select">
                                    <option value="-" class="modal-auth-reg__select-option" disabled selected>Класс</option>
                                    <option value="1" class="modal-auth-reg__select-option">1</option>
                                    <option value="2" class="modal-auth-reg__select-option">2</option>
                                    <option value="3" class="modal-auth-reg__select-option">3</option>
                                    <option value="4" class="modal-auth-reg__select-option">4</option>
                                    <option value="5" class="modal-auth-reg__select-option">5</option>
                                    <option value="6" class="modal-auth-reg__select-option">6</option>
                                </select>
                            </div>
                        </div>
                        <div class="modal-auth-reg__form-row">
                            <button type="submit" class="modal-auth-reg__btn">Зарегистрироваться</button>
                        </div>
                        <div class="modal-auth-reg__form-row">
                            <div class="modal-auth-reg__text">
                                Нажимая на&nbsp;кнопку, я&nbsp;принимаю&nbsp;
                                <a href="#" class="modal-auth-reg__text-link">условия соглашения</a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>

{{--        <div class="px-4 py-8 bg-white shadow sm:rounded-lg sm:px-10">--}}
{{--            <form wire:submit.prevent="register">--}}
{{--                <div>--}}
{{--                    <label for="name" class="block text-sm font-medium text-gray-700 leading-5">--}}
{{--                        Name--}}
{{--                    </label>--}}

{{--                    <div class="mt-1 rounded-md shadow-sm">--}}
{{--                        <input wire:model.lazy="name" id="name" type="text" required autofocus class="appearance-none block w-full px-3 py-2 border border-gray-300 rounded-md placeholder-gray-400 focus:outline-none focus:shadow-outline-blue focus:border-blue-300 transition duration-150 ease-in-out sm:text-sm sm:leading-5 @error('name') border-red-300 text-red-900 placeholder-red-300 focus:border-red-300 focus:shadow-outline-red @enderror" />--}}
{{--                    </div>--}}

{{--                    @error('name')--}}
{{--                        <p class="mt-2 text-sm text-red-600">{{ $message }}</p>--}}
{{--                    @enderror--}}
{{--                </div>--}}

{{--                <div class="mt-6">--}}
{{--                    <label for="email" class="block text-sm font-medium text-gray-700 leading-5">--}}
{{--                        Email address--}}
{{--                    </label>--}}

{{--                    <div class="mt-1 rounded-md shadow-sm">--}}
{{--                        <input wire:model.lazy="email" id="email" type="email" required class="appearance-none block w-full px-3 py-2 border border-gray-300 rounded-md placeholder-gray-400 focus:outline-none focus:shadow-outline-blue focus:border-blue-300 transition duration-150 ease-in-out sm:text-sm sm:leading-5 @error('email') border-red-300 text-red-900 placeholder-red-300 focus:border-red-300 focus:shadow-outline-red @enderror" />--}}
{{--                    </div>--}}

{{--                    @error('email')--}}
{{--                        <p class="mt-2 text-sm text-red-600">{{ $message }}</p>--}}
{{--                    @enderror--}}
{{--                </div>--}}

{{--                <div class="mt-6">--}}
{{--                    <label for="password" class="block text-sm font-medium text-gray-700 leading-5">--}}
{{--                        Password--}}
{{--                    </label>--}}

{{--                    <div class="mt-1 rounded-md shadow-sm">--}}
{{--                        <input wire:model.lazy="password" id="password" type="password" required class="appearance-none block w-full px-3 py-2 border border-gray-300 rounded-md placeholder-gray-400 focus:outline-none focus:shadow-outline-blue focus:border-blue-300 transition duration-150 ease-in-out sm:text-sm sm:leading-5 @error('password') border-red-300 text-red-900 placeholder-red-300 focus:border-red-300 focus:shadow-outline-red @enderror" />--}}
{{--                    </div>--}}

{{--                    @error('password')--}}
{{--                        <p class="mt-2 text-sm text-red-600">{{ $message }}</p>--}}
{{--                    @enderror--}}
{{--                </div>--}}

{{--                <div class="mt-6">--}}
{{--                    <label for="password_confirmation" class="block text-sm font-medium text-gray-700 leading-5">--}}
{{--                        Confirm Password--}}
{{--                    </label>--}}

{{--                    <div class="mt-1 rounded-md shadow-sm">--}}
{{--                        <input wire:model.lazy="passwordConfirmation" id="password_confirmation" type="password" required class="block w-full px-3 py-2 placeholder-gray-400 border border-gray-300 appearance-none rounded-md focus:outline-none focus:shadow-outline-blue focus:border-blue-300 transition duration-150 ease-in-out sm:text-sm sm:leading-5" />--}}
{{--                    </div>--}}
{{--                </div>--}}

{{--                <div class="mt-6">--}}
{{--                    <span class="block w-full rounded-md shadow-sm">--}}
{{--                        <button type="submit" class="flex justify-center w-full px-4 py-2 text-sm font-medium text-white bg-indigo-600 border border-transparent rounded-md hover:bg-indigo-500 focus:outline-none focus:border-indigo-700 focus:shadow-outline-indigo active:bg-indigo-700 transition duration-150 ease-in-out">--}}
{{--                            Register--}}
{{--                        </button>--}}
{{--                    </span>--}}
{{--                </div>--}}
{{--            </form>--}}
{{--        </div>--}}
    </div>
</div>

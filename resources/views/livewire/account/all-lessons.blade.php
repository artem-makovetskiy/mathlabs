<div>
    <div class="content">
        <div class="content__container">
            <div class="content__col content__col--left">
                @livewire('account.sidebar-nav', ['banner_type' => 'summer_school', 'active_page' => 'all_lessons'])
            </div>
            <div class="content__col content__col--right">
                <div class="content-header content-header--online-classes">
                    <div class="content-header__container">
                        <div class="content-header__row content-header__row--diff-directions">
                            <div class="content-header__col">
                                <div class="content-header__list hidden lg:flex">
                                    <a wire:click="setLessonsFilter('all')" class="content-header__item{{ $lessons_filter_type === 'all' ? ' content-header__item--active' : '' }}" href="#">Все занятия</a>
                                    <a wire:click="setLessonsFilter('webinar')" class="content-header__item{{ $lessons_filter_type === 'webinar' ? ' content-header__item--active' : '' }}" href="#">Интенсивы</a>
                                    <a wire:click="setLessonsFilter('in_group')" class="content-header__item{{ $lessons_filter_type === 'in_group' ? ' content-header__item--active' : '' }}" href="#">В группе</a>
                                    <a wire:click="setLessonsFilter('individual')" class="content-header__item{{ $lessons_filter_type === 'individual' ? ' content-header__item--active' : '' }}" href="#">Индивидуальные</a>
                                </div>
                                <div class="content-header__tooltips-wrapper lg:hidden">
                                    <div x-data="{ open: false }" @click.away="open = false" class="content-header__tooltip-item content-header__tooltip-item--tall" :class="{ 'content-header__tooltip-item--active': open }">
                                        <div @click="open = !open" class="content-header__tooltip-title">{{ $this->lessontypes[$lessons_filter_type] }}</div>
                                        <div x-show="open" class="content-header__tooltip-content" x-transition:enter="transition ease-out duration-100" x-transition:enter-start="transform opacity-0 scale-95" x-transition:enter-end="transform opacity-100 scale-100" x-transition:leave="transition ease-in duration-75" x-transition:leave-start="transform opacity-100 scale-100" x-transition:leave-end="transform opacity-0 scale-95">
                                            @foreach($this->lessontypes as $type_key => $type_label)
                                                <a wire:click="setLessonsFilter('{{ $type_key }}')" @click="open = false" class="content-header__tooltip-content-item{{ $type_key == $lessons_filter_type ? ' content-header__tooltip-content-item--active' : '' }}" href="#">{{ $type_label }}</a>
                                            @endforeach
                                            <!-- <a class="content-header__tooltip-content-item content-header__tooltip-content-item--active" href="#">11 класс</a><a class="content-header__tooltip-content-item" href="#">10 класс</a><a class="content-header__tooltip-content-item" href="#">9 класс</a><a class="content-header__tooltip-content-item" href="#">8 класс</a><a class="content-header__tooltip-content-item" href="#">7 класс</a><a class="content-header__tooltip-content-item" href="#">6 класс</a><a class="content-header__tooltip-content-item" href="#">5 класс</a><a class="content-header__tooltip-content-item" href="#">4 класс</a><a class="content-header__tooltip-content-item" href="#">3 класс</a><a class="content-header__tooltip-content-item" href="#">2 класс</a><a class="content-header__tooltip-content-item" href="#">1 класс</a> -->
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="content-header__col">
                                <div class="content-header__tooltips-wrapper">
                                    <div class="hidden content-header__tooltip-item">
                                        <div class="content-header__tooltip-title">Все цели</div>
                                        <div class="content-header__tooltip-content"><a class="content-header__tooltip-content-item content-header__tooltip-content-item--active" href="#">Олимпиадная математика</a><a class="content-header__tooltip-content-item" href="#">Школьная программа</a><a class="content-header__tooltip-content-item" href="#">ЕГЭ по математике</a><a class="content-header__tooltip-content-item" href="#">ОГЭ по математике</a><a class="content-header__tooltip-content-item" href="#">Математические школы</a><a class="content-header__tooltip-content-item" href="#">Международные экзамены</a><a class="content-header__tooltip-content-item" href="#">Все цели</a></div>
                                    </div>
                                    <div x-data="{ open: false }" @click.away="open = false" class="content-header__tooltip-item" :class="{ 'content-header__tooltip-item--active': open }">
                                        <div @click="open = !open" class="content-header__tooltip-title">{{ $filter_grade }} класс</div>
                                        <div x-show="open" class="content-header__tooltip-content" x-transition:enter="transition ease-out duration-100" x-transition:enter-start="transform opacity-0 scale-95" x-transition:enter-end="transform opacity-100 scale-100" x-transition:leave="transition ease-in duration-75" x-transition:leave-start="transform opacity-100 scale-100" x-transition:leave-end="transform opacity-0 scale-95">
                                            @foreach($this->grades as $grade_key => $grade)
                                                <a wire:click="setGradeFilter('{{ $grade_key }}')" @click="open = false" class="content-header__tooltip-content-item{{ $grade_key == $filter_grade ? ' content-header__tooltip-content-item--active' : '' }}" href="#">{{ $grade }}</a>
                                            @endforeach
                                            <!-- <a class="content-header__tooltip-content-item content-header__tooltip-content-item--active" href="#">11 класс</a><a class="content-header__tooltip-content-item" href="#">10 класс</a><a class="content-header__tooltip-content-item" href="#">9 класс</a><a class="content-header__tooltip-content-item" href="#">8 класс</a><a class="content-header__tooltip-content-item" href="#">7 класс</a><a class="content-header__tooltip-content-item" href="#">6 класс</a><a class="content-header__tooltip-content-item" href="#">5 класс</a><a class="content-header__tooltip-content-item" href="#">4 класс</a><a class="content-header__tooltip-content-item" href="#">3 класс</a><a class="content-header__tooltip-content-item" href="#">2 класс</a><a class="content-header__tooltip-content-item" href="#">1 класс</a> -->
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div><a class="hidden md:block banner-content" href="#">
                    <div class="banner-content__container">
                        <div class="banner-content__col banner-content__col--left">
                            <div class="banner-content__title">Летняя заочная математическая школа</div>
                            <div class="banner-content__text-wrapper"><span class="banner-content__text">Ранняя регистрация:</span><span class="banner-content__price-new">500 руб.</span><span class="banner-content__price-old">1 500 руб.</span></div>
                            <div class="banner-content__date-and-location">23-28 июля&nbsp;| Москва</div>
                        </div>
                        <div class="banner-content__col banner-content__col--right">
                            <div class="banner-content__btn">Узнать подробнее</div>
                        </div>
                    </div><img class="banner-content__img" src="{{ asset('img/banners/2.png') }}"></a>
                <div class="">
                    <div class="online-classes">
                        <div class="online-classes__container">
                            @forelse($this->lessons as $lesson)
                                <div class="lesson-item lesson-item--{{ $lesson->color }}-line">
                                    <div class="lesson-item__inner">
                                        <div class="lesson-item__col lesson-item__col--left stack">
                                            <div class="flex flex-wrap">
                                                @foreach($lesson->tags as $tag)
                                                <a class="item__tags-item" href="#">{{ $tag }}</a>
                                                @endforeach
                                            </div>
                                            <div class="item__title">{{ $lesson->title }}</div>
                                            <div class="item__descr mb-4">{{ $lesson->brief_desc }}</div>
                                            @if($lesson->type === 'webinar')
                                                <div class="flex-col xs:flex-row flex xs:items-center space-y-2 xs:space-y-0 xs:space-x-2 mt-auto">
                                                    <div class="item__author"><img class="item__author-img" src="../img/examples/1.png">
                                                        <div class="item__author-title">Альберт Горяев</div>
                                                    </div>
                                                    <div class="item__history">{{ $lesson->event_date }}</div>
                                                </div>
                                            @endif
                                        </div>
                                        <div class="lesson-item__col lesson-item__col--right stack">
                                            <div class="item__price-wrapper mb-0">
                                                <span class="item__price item__price--{{ $lesson->color }}">{{ $lesson->price }} руб.</span>
                                            </div>
                                            <a wire:click.prevent="buyLesson('{{ $lesson->uuid }}', '{{ $lesson->type }}')" class="item__btn item__btn--{{ $lesson->color }}" href="#">Записаться</a>
                                            <a class="item__btn item__btn--{{ $lesson->color }}-empty" href="{{ route('account.lessonDetails', [$lesson->type, $lesson->uuid]) }}">Подробнее</a>
                                        </div>
                                    </div>
                                </div>
                            
                                <div class="hidden focus-within:item item--online-classes item--{{ $lesson->color }}-line">
                                    <div class="item__container">
                                        <div class="item__row item__row--top">
                                            <div class="item__tags">
                                                @foreach($lesson->tags as $tag)
                                                    <a class="item__tags-item" href="#">{{ $tag }}</a>
                                                @endforeach
                                            </div>
                                        </div>
                                        <div class="item__row item__row--middle">
                                            <div class="item__col">
                                                <div class="item__title">{{ $lesson->title }}</div>
                                                <div class="item__descr">{{ $lesson->brief_desc }}</div>
                                                <div class="hidden item__alert">Количество мест ограничено. Записывайтесь заранее!</div>
                                            </div>
                                            <div class="item__col">
                                                <div class="item__price-wrapper">
                                                    <span class="item__price item__price--{{ $lesson->color }}">{{ $lesson->price }} руб.</span>
                                                </div>
                                                <a wire:click.prevent="buyLesson('{{ $lesson->uuid }}', '{{ $lesson->type }}')" class="item__btn item__btn--{{ $lesson->color }}" href="#">Записаться</a>
                                                <a class="item__btn item__btn--{{ $lesson->color }}-empty" href="{{ route('account.lessonDetails', [$lesson->type, $lesson->uuid]) }}">Подробнее</a>
                                            </div>
                                        </div>
                                        @if($lesson->type === 'webinar')
                                            <div class="item__row item__row--bottom">
                                                <div class="item__author"><img class="item__author-img" src="../img/examples/1.png">
                                                    <div class="item__author-title">Альберт Горяев</div>
                                                </div>
                                                <div class="item__history">{{ $lesson->event_date }}</div>
                                            </div>
                                        @endif
                                    </div>
                                </div>
                            @empty
                                <div class="online-classes__items-empty">Занятий пока не добавлено.</div>
                            @endforelse
                        </div>
                        @if($this->webinars->hasMorePages())
                            <div class="online-classes__container"><a class="online-classes__btn" href="#">Показать ещё</a></div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
    @if($payment_success)
        <div class="modal-classes-successfully-paid modal-classes-successfully-paid--active">
            <div class="modal-classes-successfully-paid__container">
                <div wire:click="closeModal" class="modal-classes-successfully-paid__close"></div>
                <div class="modal-classes-successfully-paid__title">Занятия успешно оплачены!</div>
                <div class="modal-classes-successfully-paid__descr">В&nbsp;разделе &laquo;Мои уроки&raquo; выбери нужное занятие&nbsp;<br>и&nbsp;начинай обучение. Успехов тебе!</div><a wire:click.prevent="closeModal" class="modal-classes-successfully-paid__btn" href="#">Продолжить</a>
            </div>
            <div wire:click="closeModal" class="modal-classes-successfully-paid__overlay"></div>
        </div>
    @elseif($payment_failure)
        <div class="modal-all-questions modal-all-questions--active">
            <div class="modal-all-questions__container">
                <div wire:click="closeModal" class="modal-all-questions__close"></div>
                <div class="modal-all-questions__title">Недостаточно средств!</div>
                <div class="modal-all-questions__descr">Перейдите в раздел "Оплатить занятия"<br> и приобретите желаемый вариант.</div><a wire:click.prevent="closeModal" class="modal-all-questions__btn" href="#">Вернуться</a>
            </div>
            <div wire:click="closeModal" class="modal-all-questions__overlay"></div>
        </div>
    @endif
    @if($show_scheduled_time)
        <div class="modal-timing modal-timing--active">
            <div class="modal-timing__container">
                <div wire:click="closeModal" class="modal-timing__close"></div>
                <div class="modal-timing__title">Поздравляем! <br>Ты&nbsp;записан на&nbsp;занятия!</div>
                <div class="modal-timing__descr">Выбери день и&nbsp;время удобное для обучения:</div>
                <div class="modal-timing__calendar">
                    <div class="modal-timing__calendar-header">
                        <div class="modal-timing__calendar-row">
                            @foreach($scheduled_time_defaults['weekdays'] as $weekday)
                                <div wire:click="addWeekday('{{ $weekday }}')" class="modal-timing__item{{ in_array($weekday, $scheduled_time['weekdays']) ? ' modal-timing__item--active' : '' }}">{{ $weekday }}</div>
                            @endforeach
                        </div>
                    </div>
                    <div class="modal-timing__calendar-content">
                        @foreach(collect($scheduled_time_defaults['time']) as $time)
                            <div wire:click="addTime('{{ $time }}')" class="modal-timing__item{{ in_array($time, $scheduled_time['time']) ? ' modal-timing__item--active' : '' }}">{{ $time }}</div>
                        @endforeach
                    </div>
                </div><a wire:click.prevent="saveScheduledTime('{{ $lesson->uuid }}')" class="modal-timing__btn" href="#">Сохранить</a>
            </div>
            <div wire:click="closeModal" class="modal-timing__overlay"></div>
        </div>
    @endif
</div>

<div>
    <div class="content">
        <div class="content__container">
            <div class="content__col content__col--left">
                @livewire('account.sidebar-nav', ['banner_type' => 'summer_school'])
            </div>
            <div class="content__col content__col--right">
                <div class="content-header content-header--lesson">
                    <div class="content-header__container">
                        <div class="content-header__row content-header__row--diff-directions">
                            <div class="content-header__col"><a class="content-header__btn content-header__btn--back" href="{{ url()->previous() }}">Вернуться назад</a></div>
                        </div>
                    </div>
                </div>
                <div class="more-about {{ $this->containerclass }}">
                    <div class="more-about__container">
                        <div class="more-about__cover">
                            <div class="more-about__cover-tag">{{ $this->label }}</div>
                            <div class="more-about__date hidden">30&nbsp;марта&nbsp;| с&nbsp;16:00 до&nbsp;20:00</div>
                            <div class="more-about__cover-title">{{ $this->lesson->title }}</div>
                            <div class="more-about__cover-price"><span class="hidden more-about__cover-price-old">1 500 руб.</span>{{ $this->lesson->price }} руб.</div><a class="more-about__cover-btn" href="#">Записаться</a>
                        </div>
                    </div>
                    <div class="more-about__container">
                        <div class="more-about__descr">{{ $this->lesson->brief_desc }}</div>
                        <!-- <div class="more-about__descr">Курс прорабатывает необходимую теорию, учит решать сложные задания из&nbsp;олимпиады Физтех по&nbsp;математике, правильно выстраивая логику рассуждений. На&nbsp;занятиях обсуждаются олимпиадные задачи прошлых лет по&nbsp;математике и&nbsp;разбираются методы их&nbsp;решения.</div> -->
                    </div>
                    <div class="more-about__container">
                        <div class="more-about__bullitts">
                            <div class="more-about__bullitts-item more-about__bullitts-item--graduate">
                                <div class="more-about__bullitts-item-text">Курс длится&nbsp;<span class="more-about__bullitts-item-text-bold">3&nbsp;недели&nbsp;</span>&mdash;&nbsp;<br>с&nbsp;25&nbsp;марта по&nbsp;1&nbsp;мая 2020</div>
                            </div>
                            <div class="more-about__bullitts-item more-about__bullitts-item--book">
                                <div class="more-about__bullitts-item-text">Включает <span class="more-about__bullitts-item-text-bold">5&nbsp;занятий,&nbsp;</span><br>каждое из&nbsp;которых длится <br>2&nbsp;академических часа</div>
                            </div>
                            <div class="more-about__bullitts-item more-about__bullitts-item--paper">
                                <div class="more-about__bullitts-item-text">Предлагается&nbsp;<span class="more-about__bullitts-item-text-bold">куратор</span>, занятия дополняются материалами для<br>распечатки</div>
                            </div>
                        </div>
                    </div>
                    <div class="more-about__container">
                        <div class="more-about__program">
                            <div class="more-about__program-title">Программа интенсива</div>
                            <div class="more-about__program-descr">Онлайн-занятия проходят по&nbsp;средам и&nbsp;пятницам в&nbsp;18:00 и&nbsp;всегда доступны в&nbsp;записи</div>
                            <div class="more-about__program-wrapper">
                                <div class="more-about__program-item">
                                    <div class="more-about__program-item-question">Геометрическая оптика</div>
                                    <div class="more-about__program-item-answer">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi sit pretium bibendum tellus aliquam auctor sem elementum velit. Sit facilisis accumsan, mattis imperdiet lobortis. Curabitur diam sit feugiat mattis feugiat diam nunc posuere. Tellus neque, est, amet adipiscing venenatis, velit semper neque faucibus. Cursus egestas dis massa proin est. Quisque bibendum nullam metus, sed quis. Massa nunc, congue sollicitudin dictumst cras. Convallis morbi nisi, magna egestas. Varius elit augue dictumst at velit tempus, sed et mollis.</div>
                                </div>
                                <div class="more-about__program-item">
                                    <div class="more-about__program-item-question">Молекулярная физика и термодинамика</div>
                                    <div class="more-about__program-item-answer">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi sit pretium bibendum tellus aliquam auctor sem elementum velit. Sit facilisis accumsan, mattis imperdiet lobortis. Curabitur diam sit feugiat mattis feugiat diam nunc posuere. Tellus neque, est, amet adipiscing venenatis, velit semper neque faucibus. Cursus egestas dis massa proin est. Quisque bibendum nullam metus, sed quis. Massa nunc, congue sollicitudin dictumst cras. Convallis morbi nisi, magna egestas. Varius elit augue dictumst at velit tempus, sed et mollis.</div>
                                </div>
                                <div class="more-about__program-item">
                                    <div class="more-about__program-item-question">Геометрическая оптика</div>
                                    <div class="more-about__program-item-answer">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi sit pretium bibendum tellus aliquam auctor sem elementum velit. Sit facilisis accumsan, mattis imperdiet lobortis. Curabitur diam sit feugiat mattis feugiat diam nunc posuere. Tellus neque, est, amet adipiscing venenatis, velit semper neque faucibus. Cursus egestas dis massa proin est. Quisque bibendum nullam metus, sed quis. Massa nunc, congue sollicitudin dictumst cras. Convallis morbi nisi, magna egestas. Varius elit augue dictumst at velit tempus, sed et mollis.</div>
                                </div>
                                <div class="more-about__program-item">
                                    <div class="more-about__program-item-question">Молекулярная физика и термодинамика</div>
                                    <div class="more-about__program-item-answer">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi sit pretium bibendum tellus aliquam auctor sem elementum velit. Sit facilisis accumsan, mattis imperdiet lobortis. Curabitur diam sit feugiat mattis feugiat diam nunc posuere. Tellus neque, est, amet adipiscing venenatis, velit semper neque faucibus. Cursus egestas dis massa proin est. Quisque bibendum nullam metus, sed quis. Massa nunc, congue sollicitudin dictumst cras. Convallis morbi nisi, magna egestas. Varius elit augue dictumst at velit tempus, sed et mollis.</div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="more-about__container">
                        <div class="more-about__cost-of-training">
                            <div class="more-about__cost-of-training-title">Стоимость интенсива</div>
                            <div class="more-about__cost-of-training-wrapper more-about__cost-of-training-wrapper--center">
                                <div class="more-about__cost-of-training-item more-about__cost-of-training-item--orange">
                                    <div class="more-about__cost-of-training-price">{{ $this->lesson->price }} руб.</div>
                                    <div class="hidden more-about__cost-of-training-price-old">1 500 руб.</div><a class="more-about__cost-of-training-btn" href="#">Оплатить интенсив</a>
                                    <div class="more-about__cost-of-training-warning">Количество мест ограничено.<br>Записывайтесь заранее!</div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="more-about__container">
                        <div class="more-about__course-teacher">
                            <div class="more-about__course-teacher-title">Преподаватель курса</div>
                            <div class="more-about__course-teacher-row">
                                <div class="more-about__course-teacher-col more-about__course-teacher-col--left"><img class="more-about__course-teacher-img" src="{{ asset('img/examples/3.png') }}"></div>
                                <div class="more-about__course-teacher-col more-about__course-teacher-col--right">
                                    <div class="more-about__course-teacher-subtitle">Альберт Горяев</div>
                                    <div class="more-about__course-teacher-descr">Механико-математический факультет МГУ</div>
                                    <div class="more-about__course-teacher-text">Сын ученого, который занимается вопросами космического излучения в РАН. Альберт любит математику так сильно, что берет книги по функциональному анализу даже на встречи с друзьями.<br><br>Подготовил ряд школьников, которые поступили в ряд ведущих лицеев и математических школ Москвы.</div>
                                    <div class="more-about__course-teacher-wrapper">
                                        <div class="more-about__course-teacher-assessment">
                                            <div class="more-about__course-teacher-assessment-title">ЕГЭ по математике</div>
                                            <div class="more-about__course-teacher-assessment-text">100 из 100 баллов</div>
                                        </div>
                                        <div class="more-about__course-teacher-assessment">
                                            <div class="more-about__course-teacher-assessment-title">Дополнительный экзамен МГУ</div>
                                            <div class="more-about__course-teacher-assessment-text">100 из 100 баллов</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="more-about__container">
                        <div class="more-about__faq">
                            <div class="more-about__faq-title">Вопросы/Ответы</div>
                            <div class="more-about__faq-wrapper">
                                <div class="more-about__faq-item">
                                    <div class="more-about__faq-item-question">У вас много репетиторов, как вы контролируете качество занятий?</div>
                                    <div class="more-about__faq-item-answer">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi sit pretium bibendum tellus aliquam auctor sem elementum velit. Sit facilisis accumsan, mattis imperdiet lobortis. Curabitur diam sit feugiat mattis feugiat diam nunc posuere. Tellus neque, est, amet adipiscing venenatis, velit semper neque faucibus. Cursus egestas dis massa proin est. Quisque bibendum nullam metus, sed quis. Massa nunc, congue sollicitudin dictumst cras. Convallis morbi nisi, magna egestas. Varius elit augue dictumst at velit tempus, sed et mollis.</div>
                                </div>
                                <div class="more-about__faq-item">
                                    <div class="more-about__faq-item-question">Как, когда и в какое время я буду учиться?</div>
                                    <div class="more-about__faq-item-answer">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi sit pretium bibendum tellus aliquam auctor sem elementum velit. Sit facilisis accumsan, mattis imperdiet lobortis. Curabitur diam sit feugiat mattis feugiat diam nunc posuere. Tellus neque, est, amet adipiscing venenatis, velit semper neque faucibus. Cursus egestas dis massa proin est. Quisque bibendum nullam metus, sed quis. Massa nunc, congue sollicitudin dictumst cras. Convallis morbi nisi, magna egestas. Varius elit augue dictumst at velit tempus, sed et mollis.</div>
                                </div>
                                <div class="more-about__faq-item">
                                    <div class="more-about__faq-item-question">У вас много репетиторов, как вы контролируете качество занятий?</div>
                                    <div class="more-about__faq-item-answer">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi sit pretium bibendum tellus aliquam auctor sem elementum velit. Sit facilisis accumsan, mattis imperdiet lobortis. Curabitur diam sit feugiat mattis feugiat diam nunc posuere. Tellus neque, est, amet adipiscing venenatis, velit semper neque faucibus. Cursus egestas dis massa proin est. Quisque bibendum nullam metus, sed quis. Massa nunc, congue sollicitudin dictumst cras. Convallis morbi nisi, magna egestas. Varius elit augue dictumst at velit tempus, sed et mollis.</div>
                                </div>
                                <div class="more-about__faq-item">
                                    <div class="more-about__faq-item-question">У вас много репетиторов, как вы контролируете качество занятий?</div>
                                    <div class="more-about__faq-item-answer">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi sit pretium bibendum tellus aliquam auctor sem elementum velit. Sit facilisis accumsan, mattis imperdiet lobortis. Curabitur diam sit feugiat mattis feugiat diam nunc posuere. Tellus neque, est, amet adipiscing venenatis, velit semper neque faucibus. Cursus egestas dis massa proin est. Quisque bibendum nullam metus, sed quis. Massa nunc, congue sollicitudin dictumst cras. Convallis morbi nisi, magna egestas. Varius elit augue dictumst at velit tempus, sed et mollis.</div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="more-about__container">
                        <div class="hidden more-about__try-it-for-free">
                            <div class="more-about__try-it-for-free-title">Попробуй бесплатное вводное онлайн-занятие</div><a class="more-about__try-it-for-free-btn" href="#">Оплатить интенсив</a>
                        </div>
                        <div class="flex flex-col lg:flex-row justify-center items-center space-x-0 lg:space-x-8 space-y-8 lg:space-y-0 bg-blue-light rounded-lg p-4 md:p-6 lg:p-8">
                            <div class="text-base md:text-lg lg:text-xl font-bold text-center lg:text-left">Попробуйте бесплатное вводное онлайн-занятие</div>
                            <button type="button" class="more-about__try-it-for-free-btn">
                                @if($this->lesson->type === 'group')
                                Записаться в группу
                                @elseif($this->lesson->type === 'individual')
                                Записаться на занятие
                                @elseif($this->lesson->type === 'webinar')
                                Оплатить интенсив
                                @endif
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

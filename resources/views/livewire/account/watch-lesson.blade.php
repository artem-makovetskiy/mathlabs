<div>
    <div class="content">
        <div class="content__container">
            <div class="content__col content__col--left">
                @livewire('account.sidebar-nav', [])
            </div>
            <div class="content__col content__col--right">
                <div class="content-header content-header--lesson">
                    <div class="content-header__container">
                        <div class="content-header__row content-header__row--diff-directions">
                            <div class="content-header__col"><a class="content-header__btn content-header__btn--back" href="{{ url()->previous() }}">Вернуться назад</a></div>
                        </div>
                    </div>
                </div>
                <div class="lesson">
                    <div class="lesson__container">
                        <div class="lesson__header">
                            <div class="lesson__title">{{ $this->lesson->title }}</div>
                            <div class="hidden lesson__author"><img class="lesson__author-img" src="{{ asset('/img/examples/1.png') }}">
                                <div class="lesson__author-title">Альберт Горяев</div>
                            </div>
                            <div class="lesson__history">10 занятий</div><a class="lesson__link" href="{{ route('account.lessonDetails', [$this->lesson->type, $uuid]) }}">О курсе</a>
                        </div>
                        <div class="lesson__content lesson__content--text">
                            <div class="lesson__content-col">
                                <div class="lesson__content-title">Вводное занятие</div>
                                <ul class="lesson__content-list">
                                    <li class="lesson__content-list-item">Знакомство с курсом и платформой</li>
                                    <li class="lesson__content-list-item">Секреты успешной сдачи ЕГЭ по математике на 90+ баллов</li>
                                    <li class="lesson__content-list-item">Решение сложной, но интересной задачи из ЕГЭ</li>
                                    <li class="lesson__content-list-item">Интерактив: выбор задачи на разбор</li>
                                </ul>
                            </div>
                            <div class="lesson__content-col">
                                <div class="lesson__date"><span class="lesson__date-text">{{ $this->lesson->event_date }}</span><a class="hidden lesson__date-link" href="#">18:30</a><a class="hidden lesson__date-link" href="#">Изменить</a></div>
                                <a class="lesson__btn-download lesson__btn-download--without-icon{{ is_null($this->joinlink) || $this->meetingupcomingsoon === false ? ' lesson__btn--disable' : '' }}" href="{{ !is_null($this->joinlink) && $this->meetingupcomingsoon === true ? $this->joinlink : 'javascript:void(0);'}}" {{ is_null($this->joinlink) || $this->meetingupcomingsoon === false ? 'disabled' : '' }}>Перейти к трансляции</a>
                            </div>
                        </div>
                        <div class="hidden lesson__content lesson__content--video">
                            <div class="lesson__video-wrapper">
                                <div class="lesson__video-poster"><img class="lesson__video-poster-img" src="{{ asset('img/examples/2.png') }}"></div>
                                <video class="lesson__video" id="lesson-video" controls="true">
                                    <!-- <source src="{{ asset('/video/1.mp4') }}" type="video/mp4"> -->
                                </video>
                            </div>
                        </div>
                        <div class="hidden lesson__footer">
                            <div class="lesson__btn lesson__btn--prev lesson__btn--disable">Предыдущее занятие</div><a class="lesson__btn lesson__btn--next" href="#">Следующее занятие</a>
                        </div>
                    </div>
                </div>

{{--                <div class="content-header content-header--my-lessons">--}}
{{--                    <div class="content-header__container">--}}
{{--                        <div class="content-header__row content-header__row--diff-directions">--}}
{{--                            <div class="content-header__col"><a class="content-header__btn content-header__btn--back" href="{{ url()->previous() }}">Вернуться назад</a></div>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--                <div class="content__inner-wrapper">--}}
{{--                    <div class="content__inner-wrapper-col content__inner-wrapper-col--left">--}}

{{--                    </div>--}}
{{--                </div>--}}
            </div>
        </div>
    </div>
</div>

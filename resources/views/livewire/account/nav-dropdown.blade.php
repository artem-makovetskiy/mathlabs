<div>
    <div x-data="{ open: false }" 
        @keydown.window.escape="open = false" 
        @click.away="open = false" 
        class="nav-dropdown relative inline-block text-left w-full z-10 md:hidden">
        <div>
            <span class="rounded-md shadow-sm">
            <button @click="open = !open" type="button" class="nav-dropdown__btn focus:border-none focus:shadow-none active:bg-gray-50 focus:outline-none transition ease-in-out duration-150">
                {{ $this->activepagename }}
                <svg class="-mr-1 ml-2 h-5 w-5 text-gray-700" fill="currentColor" viewBox="0 0 20 20">
                <path fill-rule="evenodd" d="M5.293 7.293a1 1 0 011.414 0L10 10.586l3.293-3.293a1 1 0 111.414 1.414l-4 4a1 1 0 01-1.414 0l-4-4a1 1 0 010-1.414z" clip-rule="evenodd"/>
                </svg>
            </button>
            </span>
        </div>
        <div x-show="open" x-transition:enter="transition ease-out duration-100" x-transition:enter-start="transform opacity-0 scale-95" x-transition:enter-end="transform opacity-100 scale-100" x-transition:leave="transition ease-in duration-75" x-transition:leave-start="transform opacity-100 scale-100" x-transition:leave-end="transform opacity-0 scale-95" 
             class="nav-dropdown__list" style="display: none;">
            <div class="rounded-md bg-white shadow-xs">
            <div class="py-1">
                <a href="{{ route('account.buyLessons') }}" class="nav-dropdown__list__item nav-dropdown__list__item--pay-for-classes">Оплатить занятия</a>
                <a href="{{ route('account.allLessons') }}" class="nav-dropdown__list__item nav-dropdown__list__item--available-classes">Доступные занятия</a>
                <a href="{{ route('account.myLessons') }}" class="nav-dropdown__list__item nav-dropdown__list__item--my-lessons">Мои уроки</a>
                <a href="{{ route('account.quiz') }}" class="nav-dropdown__list__item nav-dropdown__list__item--quiz">Тестирование</a>
                <a href="{{ route('account.settings') }}" class="nav-dropdown__list__item nav-dropdown__list__item--settings">Настройки</a>
                <a href="{{ route('account.notifications') }}" class="nav-dropdown__list__item nav-dropdown__list__item--notifications">Уведомления</a>
                <a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();" class="nav-dropdown__list__item nav-dropdown__list__item--exit">Выход</a>
            </div>
            </div>
        </div>
    </div>
</div>

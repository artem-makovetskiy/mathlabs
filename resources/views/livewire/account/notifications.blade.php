<div>
    <div class="content">
        <div class="content__container">
            <div class="content__col content__col--left">
                @livewire('account.sidebar-nav', ['banner_type' => 'testing', 'active_page' => 'notifications'])
            </div>
            <div class="content__col content__col--right">
                <div class="notifications">
                    <div class="notifications__container">
                        @foreach(auth()->user()->notifications as $notification)
                            <div class="notifications__item">
                                <div class="notifications__col notifications__col--left">
                                    <div class="notifications__icon notifications__icon--bell"></div>
                                </div>
                                <div class="notifications__col notifications__col--middle">
                                    <div class="notifications__title">{{ $notification->data['message'] }}</div>
                                    <div class="notifications__date">{{ $notification->created_at->format('d.m.Y') }}</div>
                                </div>
                                <div class="notifications__col notifications__col--right"><a class="notifications__icon notifications__icon--close" href="#"></a></div>
                            </div>
                        @endforeach

{{--                        <div class="notifications__item">--}}
{{--                            <div class="notifications__col notifications__col--left">--}}
{{--                                <div class="notifications__icon notifications__icon--bell"></div>--}}
{{--                            </div>--}}
{{--                            <div class="notifications__col notifications__col--middle">--}}
{{--                                <div class="notifications__title">Твоё следующее занятие завтра в 18:00! Не пропусти! :)</div>--}}
{{--                                <div class="notifications__date">25.03.2020</div>--}}
{{--                            </div>--}}
{{--                            <div class="notifications__col notifications__col--right"><a class="notifications__icon notifications__icon--close" href="#"></a></div>--}}
{{--                        </div>--}}
{{--                        <div class="notifications__item">--}}
{{--                            <div class="notifications__col notifications__col--left">--}}
{{--                                <div class="notifications__icon notifications__icon--bell"></div>--}}
{{--                            </div>--}}
{{--                            <div class="notifications__col notifications__col--middle">--}}
{{--                                <div class="notifications__title">У тебя есть неоплаченные занятия. Перейди на страницу курса, чтобы оплатить!</div>--}}
{{--                                <div class="notifications__date">25.03.2020</div>--}}
{{--                            </div>--}}
{{--                            <div class="notifications__col notifications__col--right"><a class="notifications__icon notifications__icon--close" href="#"></a></div>--}}
{{--                        </div>--}}
{{--                        <div class="notifications__item">--}}
{{--                            <div class="notifications__col notifications__col--left">--}}
{{--                                <div class="notifications__icon notifications__icon--bell"></div>--}}
{{--                            </div>--}}
{{--                            <div class="notifications__col notifications__col--middle">--}}
{{--                                <div class="notifications__title">В корзине есть неоплаченные занятия. Оплати, чтобы продолжить обучение!</div>--}}
{{--                                <div class="notifications__date">25.03.2020</div>--}}
{{--                            </div>--}}
{{--                            <div class="notifications__col notifications__col--right"><a class="notifications__icon notifications__icon--close" href="#"></a></div>--}}
{{--                        </div>--}}
{{--                        <div class="notifications__item">--}}
{{--                            <div class="notifications__col notifications__col--left">--}}
{{--                                <div class="notifications__icon notifications__icon--bell"></div>--}}
{{--                            </div>--}}
{{--                            <div class="notifications__col notifications__col--middle">--}}
{{--                                <div class="notifications__title">Твоё следующее занятие завтра в 18:00! Не пропусти! :)</div>--}}
{{--                                <div class="notifications__date">25.03.2020</div>--}}
{{--                            </div>--}}
{{--                            <div class="notifications__col notifications__col--right"><a class="notifications__icon notifications__icon--close" href="#"></a></div>--}}
{{--                        </div>--}}
{{--                        <div class="notifications__item">--}}
{{--                            <div class="notifications__col notifications__col--left">--}}
{{--                                <div class="notifications__icon notifications__icon--bell"></div>--}}
{{--                            </div>--}}
{{--                            <div class="notifications__col notifications__col--middle">--}}
{{--                                <div class="notifications__title">У тебя есть неоплаченные занятия. Перейди на страницу курса, чтобы оплатить!</div>--}}
{{--                                <div class="notifications__date">25.03.2020</div>--}}
{{--                            </div>--}}
{{--                            <div class="notifications__col notifications__col--right"><a class="notifications__icon notifications__icon--close" href="#"></a></div>--}}
{{--                        </div>--}}
{{--                        <div class="notifications__item">--}}
{{--                            <div class="notifications__col notifications__col--left">--}}
{{--                                <div class="notifications__icon notifications__icon--bell"></div>--}}
{{--                            </div>--}}
{{--                            <div class="notifications__col notifications__col--middle">--}}
{{--                                <div class="notifications__title">В корзине есть неоплаченные занятия. Оплати, чтобы продолжить обучение!</div>--}}
{{--                                <div class="notifications__date">25.03.2020</div>--}}
{{--                            </div>--}}
{{--                            <div class="notifications__col notifications__col--right"><a class="notifications__icon notifications__icon--close" href="#"></a></div>--}}
{{--                        </div>--}}
{{--                        <div class="notifications__item">--}}
{{--                            <div class="notifications__col notifications__col--left">--}}
{{--                                <div class="notifications__icon notifications__icon--bell"></div>--}}
{{--                            </div>--}}
{{--                            <div class="notifications__col notifications__col--middle">--}}
{{--                                <div class="notifications__title">Твоё следующее занятие завтра в 18:00! Не пропусти! :)</div>--}}
{{--                                <div class="notifications__date">25.03.2020</div>--}}
{{--                            </div>--}}
{{--                            <div class="notifications__col notifications__col--right"><a class="notifications__icon notifications__icon--close" href="#"></a></div>--}}
{{--                        </div>--}}
{{--                        <div class="notifications__item">--}}
{{--                            <div class="notifications__col notifications__col--left">--}}
{{--                                <div class="notifications__icon notifications__icon--bell"></div>--}}
{{--                            </div>--}}
{{--                            <div class="notifications__col notifications__col--middle">--}}
{{--                                <div class="notifications__title">У тебя есть неоплаченные занятия. Перейди на страницу курса, чтобы оплатить!</div>--}}
{{--                                <div class="notifications__date">25.03.2020</div>--}}
{{--                            </div>--}}
{{--                            <div class="notifications__col notifications__col--right"><a class="notifications__icon notifications__icon--close" href="#"></a></div>--}}
{{--                        </div>--}}
                    </div>
                    <div class="notifications__container notifications__container--without-box-shadow"><a class="notifications__btn" href="#">Показать ещё</a></div>
                </div>
            </div>
        </div>
    </div>
</div>

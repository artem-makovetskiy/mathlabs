<div>
    <div class="sidebar">
        <div class="sidebar__nav">
            <a class="sidebar__item sidebar__item--pay-for-classes{{ $active_page === 'pay_for_classes' ? ' sidebar__item--active' : '' }}" href="{{ route('account.buyLessons') }}">Оплатить занятия<span class="sidebar__item-border"></span></a>
            <a class="sidebar__item sidebar__item--available-classes{{ $active_page === 'all_lessons' ? ' sidebar__item--active' : '' }}" href="{{ route('account.allLessons') }}">Доп. занятия<div class="hidden sidebar__item-numbers">5</div><span class="sidebar__item-border"></span></a>
            <a class="sidebar__item sidebar__item--my-lessons{{ $active_page === 'my_lessons' ? ' sidebar__item--active' : '' }}" href="{{ route('account.myLessons') }}">Мои уроки<span class="hidden sidebar__circle"></span><span class="sidebar__item-border"></span></a>
            <a class="sidebar__item sidebar__item--quiz{{ $active_page === 'quiz' ? ' sidebar__item--active' : '' }}" href="{{ route('account.quiz') }}">Тестирование<span class="hidden sidebar__circle"></span><span class="sidebar__item-border"></span>@if(!Auth::user()->quiz_status->isPassed() && !Auth::user()->quiz_status->isSkipped())<span class="sidebar__item-notice"><svg fill="currentColor" viewBox="0 0 20 20"><path fill-rule="evenodd" d="M8.257 3.099c.765-1.36 2.722-1.36 3.486 0l5.58 9.92c.75 1.334-.213 2.98-1.742 2.98H4.42c-1.53 0-2.493-1.646-1.743-2.98l5.58-9.92zM11 13a1 1 0 11-2 0 1 1 0 012 0zm-1-8a1 1 0 00-1 1v3a1 1 0 002 0V6a1 1 0 00-1-1z" clip-rule="evenodd"></path></svg></span>@endif</a>
            <a class="sidebar__item sidebar__item--settings{{ $active_page === 'settings' ? ' sidebar__item--active' : '' }}" href="{{ route('account.settings') }}">Настройки<span class="sidebar__item-border"></span></a>
            <a class="sidebar__item sidebar__item--notifications{{ $active_page === 'notifications' ? ' sidebar__item--active' : '' }}" href="{{ route('account.notifications') }}">Уведомления@if(auth()->user()->notificationCount() > 0)<div class="sidebar__item-numbers">{{ auth()->user()->notificationCount() }}</div>@endif<span class="sidebar__item-border"></span></a>
            <a class="sidebar__item sidebar__item--exit" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">Выход<span class="sidebar__item-border"></span></a>
        </div>
        @if($banner_type === 'testing')
            <div class="hidden sm:block sidebar__banner mb-6"><a class="sidebar__banner-link" href="#">
                    <div class="sidebar__banner-title">Пройди&nbsp;<span class="sidebar__banner-title-selected">тестирование&nbsp;</span><br>и&nbsp;мы&nbsp;подберем план занятий специально для тебя!</div><img class="sidebar__banner-img" src="{{ asset('img/banners/1.png') }}">
                    <div class="sidebar__banner-btn">Начать тестирование</div></a>
            </div>
        @elseif($banner_type === 'summer_school')
            <div class="hidden sm:block sidebar__banner"><a class="sidebar__banner-link" href="#">
                    <div class="sidebar__banner-title sidebar__banner-title--small-indent">Летняя заочная&nbsp;<br>математическая школа</div>
                    <div class="sidebar__banner-descr">23-28 июля&nbsp;| Москва</div><img class="sidebar__banner-img" src="{{ asset('img/banners/2.png') }}">
                    <div class="sidebar__banner-btn">Узнать подробнее</div></a>
            </div>
        @endif
    </div>
</div>

<div>
    <script>
        var zoomConfig = <?php echo json_encode(config('zoom-api')); ?>;
        var appUrl = '{{ config('app.url') }}';
        var signature = '{{ $this->signature }}';
        var meetingNumber = {{ $this->meeting_number }};
        var meetingPassword = '{{ $this->password }}';
        var meetingRole = {{ $this->role }};
        var userName = '{{ $this->user_name }}';
        var leaveUrl = '{{ $this->leave_url }}';
    </script>
</div>

<div>
    <div class="content">
        <div class="content__container">
            <div class="content__col content__col--left">
                @livewire('account.sidebar-nav', ['banner_type' => 'summer_school', 'active_page' => 'pay_for_classes'])
            </div>
            <div class="content__col content__col--right">
                <div class="pay-for-classes">
                    <div class="pay-for-classes__container">
                        <div class="pay-for-classes__title">Оплатить занятия</div>
                        <div class="pay-for-classes__descr">Тебе доступно:<span class="pay-for-classes__descr-bold">{{ $this->authuser->available_lessons }} занятия</span></div>
                        <div class="pay-for-classes__descr">Когда занятий будет &laquo;<span class="pay-for-classes__descr-red">0</span>&raquo;&nbsp;&mdash; оплати подходящий пакет занятий ниже, чтобы продолжить обучение.&nbsp;<br>После &nbsp;<a class="pay-for-classes__descr-link" href="#">привязки карты&nbsp;</a>ты&nbsp;сможешь удобно оплачивать занятия.</div>
                        <div class="pay-for-classes__offers-wrapper">
                            @foreach($this->paymentplans as $payment_plan)
                                <div class="pay-for-classes__offers-item">
                                    <div class="pay-for-classes__offers-title">{{ $payment_plan->lessons_amount }} {{ trans_choice('main.lessons_count', $payment_plan->lessons_amount) }}</div>
                                    <div class="pay-for-classes__offers-price">{{ $payment_plan->price }} руб.</div>
                                    @if($payment_plan->savings_amount > 0)
                                        <div class="pay-for-classes__offers-saving">Экономия {{ $payment_plan->savings_amount }} руб.</div>
                                    @endif
                                    <div class="pay-for-classes__offers-descr">{{ $payment_plan->desc }}</div>
                                    <a wire:click="buyLessons('{{ $payment_plan->id }}')" class="pay-for-classes__offers-btn" href="#">Купить за {{ $payment_plan->lessons_amount * $payment_plan->price }} ₽</a>
                                </div>
                            @endforeach
                        </div>
                        <div class="pay-for-classes__how-it-works">
                            <div class="pay-for-classes__how-it-works-title">Как это работает?</div>
                            <div class="pay-for-classes__how-it-works-row">
                                <div class="pay-for-classes__how-it-works-item pay-for-classes__how-it-works-item--payment">
                                    <div class="pay-for-classes__how-it-works-text">Оплачивай занятия, чтобы обучаться &nbsp;<br>с&nbsp;репетитором&nbsp;<span class="pay-for-classes__how-it-works-item-text-bold">в&nbsp;группе</span></div>
                                </div>
                                <div class="pay-for-classes__how-it-works-item pay-for-classes__how-it-works-item--group">
                                    <div class="pay-for-classes__how-it-works-text">После покупки занятий ты&nbsp;можешь&nbsp;<br>использовать их&nbsp;кол-во для участия&nbsp;<br>в&nbsp;любых доступных групповых занятиях.<br><span class="pay-for-classes__how-it-works-text-bold">Просто запишись&nbsp;</span>и&nbsp;учись!</div>
                                </div>
                                <div class="pay-for-classes__how-it-works-item pay-for-classes__how-it-works-item--add-money">
                                    <div class="pay-for-classes__how-it-works-text">Кол-во доступных занятий видно&nbsp;<br>в&nbsp;панели навигации. Если они&nbsp;<br>закончились&nbsp;&mdash;&nbsp;<span class="pay-for-classes__how-it-works-text-bold">пополни,&nbsp;</span>чтобы&nbsp;<br>продолжить обучение.</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @if($payment_success)
        <div class="modal-classes-successfully-paid modal-classes-successfully-paid--active">
            <div class="modal-classes-successfully-paid__container">
                <div wire:click="closeModal" class="modal-classes-successfully-paid__close"></div>
                <div class="modal-classes-successfully-paid__title">Занятия успешно оплачены!</div>
                <div class="modal-classes-successfully-paid__descr">В&nbsp;разделе &laquo;Мои уроки&raquo; выбери нужное занятие&nbsp;и&nbsp;начинай обучение. Успехов тебе!</div><a wire:click="closeModal" class="modal-classes-successfully-paid__btn" href="#">Продолжить</a>
            </div>
            <div class="modal-classes-successfully-paid__overlay"></div>
        </div>
    @elseif($payment_rejected)
        <div class="modal-classes-successfully-paid modal-classes-successfully-paid--active">
            <div class="modal-classes-successfully-paid__container">
                <div wire:click="closeModal" class="modal-classes-successfully-paid__close"></div>
                <div class="text-2xl text-center mb-6">Произошла ошибка!</div>
                <!-- <div class="modal-classes-successfully-paid__title">Произошла ошибка!</div> -->
                <div class="modal-classes-successfully-paid__descr">Что-то пошло не так, попробуйте снова.</div>
            </div>
            <div class="modal-classes-successfully-paid__overlay"></div>
        </div>
    @endif
</div>
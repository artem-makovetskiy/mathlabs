<div>
    <div class="content">
        <div class="content__container">
            <div class="content__col content__col--left">
                @livewire('account.sidebar-nav', ['banner_type' => 'testing', 'active_page' => 'my_lessons'])
            </div>
            <div class="content__col content__col--right">
                <div class="content-header content-header--my-lessons">
                    <div class="content-header__container">
                        <div class="content-header__row content-header__row--diff-directions">
                            <div class="content-header__col"><a class="content-header__btn content-header__btn--add" href="{{ route('account.allLessons') }}">Добавить занятия</a></div>
                            <div class="content-header__col"><a class="content-header__link content-header__link--folder" href="#"><span class="hidden sm:inline-block">Архив</span></a></div>
                        </div>
                    </div>
                </div>
                <div class="content__inner-wrapper">
                    <div class="content__inner-wrapper-col content__inner-wrapper-col--left">
                        @if(!$quiz_status->isPassed() && !$quiz_status->isSkipped())
                            <div class="banner-start-stop banner-start-stop--with-transition">
                                <div class="banner-start-stop__container">
                                    <div class="banner-start-stop__title">Пройди тестирование <br>и&nbsp;мы&nbsp;подберем план занятий<br>специально для тебя!</div>
                                    <a class="banner-start-stop__btn" href="{{ route('account.quiz') }}">Начать тестирование</a>
                                </div>
                            </div>
                        @else
                            <div class="my-lessons{{ $this->mylessons->count() === 0 ? ' my-lessons--no-classes' : '' }}">
                                <div class="my-lessons__container stack stack-lg">
                                    @if($this->mylessons->count() > 0)
                                        @foreach($this->mylessons as $lesson)
                                            <div class="lesson-item lesson-item--{{ $lesson['color'] }}-line">
                                                <div class="lesson-item__inner">
                                                    <div class="lesson-item__col lesson-item__col--left stack">
                                                        <div class="flex flex-wrap">
                                                            @foreach($lesson['tags'] as $tag)
                                                            <a class="item__tags-item" href="#">{{ $tag }}</a>
                                                            @endforeach
                                                        </div>
                                                        <div class="item__title mb-4">{{ $lesson['title'] }}</div>
                                                        <div class="flex items-center space-x-2 mt-auto">
                                                            @if($lesson['type'] === 'group' || $lesson['type'] === 'individual')
                                                                <a class="item__history item__history--link" href="#">{{ !empty($lesson['groups']) && count($lesson['groups']) > 0 ? $lesson['groups'][0]['scheduled_time'] : 'Ожидает подтверждения' }}</a>
                                                            @elseif($lesson['type'] === 'webinar')
                                                                <div class="item__author"><img class="item__author-img" src="../img/examples/1.png">
                                                                    <div class="item__author-title">Альберт Горяев</div>
                                                                </div>
                                                                <div class="item__history">10 занятий</div>
                                                            @endif
                                                        </div>
                                                    </div>
                                                    <div class="lesson-item__col lesson-item__col--right stack">
                                                        <div class="hidden lg:flex">
                                                            <svg width="20" height="16" viewBox="0 0 20 16" fill="none" xmlns="http://www.w3.org/2000/svg"><path opacity="0.2" d="M0 3.2C0 2.0799 0 1.51984 0.217987 1.09202C0.409734 0.715695 0.715695 0.409734 1.09202 0.217987C1.51984 0 2.0799 0 3.2 0H6.28741C6.91355 0 7.22662 0 7.5108 0.0863016C7.76238 0.162704 7.99643 0.287962 8.19955 0.454909C8.42899 0.643487 8.60265 0.903978 8.94997 1.42496L9.05003 1.57504C9.39735 2.09602 9.57101 2.35651 9.80045 2.54509C10.0036 2.71204 10.2376 2.8373 10.4892 2.9137C10.7734 3 11.0864 3 11.7126 3H16.8C17.9201 3 18.4802 3 18.908 3.21799C19.2843 3.40973 19.5903 3.71569 19.782 4.09202C20 4.51984 20 5.0799 20 6.2V12.8C20 13.9201 20 14.4802 19.782 14.908C19.5903 15.2843 19.2843 15.5903 18.908 15.782C18.4802 16 17.9201 16 16.8 16H3.2C2.0799 16 1.51984 16 1.09202 15.782C0.715695 15.5903 0.409734 15.2843 0.217987 14.908C0 14.4802 0 13.9201 0 12.8V3.2Z" fill="#644BEC"/></svg>
                                                        </div>
                                                        <div class="mb-4"><a class="item__btn item__btn--{{ $lesson['color'] }}" href="{{ route('account.watchLesson', [$lesson['type'], $lesson['uuid']]) }}">К занятиям</a></div>
                                                        @if($lesson['type'] === 'webinar')
                                                        <div class="mt-auto" style="height: 32px;">Следующее: сегодня 18:30</div>
                                                        @elseif(($lesson['type'] === 'group' || $lesson['type'] === 'individual') && !empty($lesson['groups']) && count($lesson['groups']) > 0 && count($lesson['groups'][0]['meetings']) > 0)
                                                        <div class="mt-auto" style="height: 32px;">Следующее: {{ $lesson['groups'][0]['meetings'][0]['next_meeting_time'] }}</div>
                                                        @endif
                                                    </div>
                                                </div>
                                            </div>
                                        @endforeach
                                    @else
                                        <div class="my-lessons__title">Здесь ещё нет занятий</div>
                                    @endif
                                </div>
                            </div>
                        @endif
                        <div class="hidden item item--disable item--folder item--green-line">
                            <div class="item__container">
                                <div class="item__row item__row--top">
                                    <div class="item__tags"><a class="item__tags-item" href="#">Курс</a><a class="item__tags-item" href="#">11 класс</a></div>
                                </div>
                                <div class="item__row item__row--middle">
                                    <div class="item__col">
                                        <div class="item__title">Курс подготовки к олимпиаде «Физтех» по математике</div>
                                    </div>
                                    <div class="item__col"><a class="item__btn item__btn--green" href="#">Вводное занятие</a></div>
                                </div>
                                <div class="item__row item__row--bottom">
                                    <div class="item__author"><img class="item__author-img" src="../img/examples/1.png">
                                        <div class="item__author-title">Альберт Горяев</div>
                                    </div>
                                    <div class="item__history">1 занятие</div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="content__inner-wrapper-col content__inner-wrapper-col--right">
                        <div class="events-calendar">
                            <input id="events_calendar" class="hidden form-input block w-full pl-10 bg-gray-100 sm:text-sm sm:leading-5" />
                        </div>
                        <div class="hidden calendar">
                            <div class="calendar__header">
                                <div class="calendar__monthname"></div>
                                <div class="calendar__nav">
                                    <div class="calendar__nav-item calendar__nav-item--prev"></div>
                                    <div class="calendar__nav-item calendar__nav-item--next"></div>
                                </div>
                            </div>
                            <div class="calendar__content calendar__content--days">
                                <div class="calendar__day">пн</div>
                                <div class="calendar__day">вт</div>
                                <div class="calendar__day">ср</div>
                                <div class="calendar__day">чт</div>
                                <div class="calendar__day">пт</div>
                                <div class="calendar__day">сб</div>
                                <div class="calendar__day">вс</div>
                            </div>
                            <ul class="calendar__content calendar__content--numbers"></ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal-welcome-msg{{ $show_welcome_msg ? '' : ' hidden' }}">
        <div class="modal-welcome-msg__container">
            <div wire:click.prevent="closeWelcomeModal" class="modal-welcome-msg__close"></div>
            <div class="modal-welcome-msg__title">Добро пожаловать в&nbsp;MathLabs!</div>
            <div class="modal-welcome-msg__descr">Чтобы определить твой уровень знаний,&nbsp;<br>пожалуйста, пройди тестирование,&nbsp;<br>перед началом занятий!</div>
            <a class="modal-welcome-msg__btn" href="{{ route('account.quiz') }}">Перейти к тестированию</a>
        </div>
        <div class="modal-welcome-msg__overlay"></div>
    </div>
</div>

@push('after-scripts')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.26.0/moment.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/flatpickr"></script>
    <script src="https://npmcdn.com/flatpickr/dist/l10n/ru.js"></script>
    <script>
        initCalendar();

        window.livewire.on('closeWelcomeModal', () => {
            initCalendar();
        });

        function initCalendar() {
            const calendar = flatpickr("#events_calendar", {
                inline: true,
                mode: "multiple",
                enableTime: false,
                // noCalendar: true,
                dateFormat: "d-m-Y",
                // defaultDate: ["20-08-2020", "04-08-2020"],
                defaultDate: <?php echo json_encode($upcoming_event_dates); ?>,
                "locale": "ru",
                // onChange: function(selectedDates, dateStr, instance) {
                //     return false;
                // },
                onReady: function(selectedDates, dateStr, instance) {
                    const calendarDays = document.querySelectorAll('.flatpickr-day');
                    // console.log(calendarDays);

                    for (var i = 0; i < calendarDays.length; i++) {
                        calendarDays[i].addEventListener("click", function (e) {
                            console.log('click');
                            e.preventDefault();
                        });
                    }

                    // calendarDays.forEach(item => {
                    //     item.addEventListener('click', event => {
                    //         console.log('click');
                    //         // e.preventDefault;
                    //     });
                    // });
                },
            });
        }

        // calendarDays.forEach(item => {
        //     item.addEventListener('click', event => {
        //         console.log('click');
        //         // e.preventDefault;
        //     });
        // });

        // document.addEventListener('click', calendarDays, event => {
        //     console.log('click');
        //     // e.preventDefault;
        // });

        // calendarDays.addEventListener('click', (e) => {
        //     console.log('click');
        //     e.preventDefault;
        // });
    </script>
@endpush

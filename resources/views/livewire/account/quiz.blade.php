<div>
    <div class="content">
        <div class="content__container">
            <div class="content__col content__col--left">
                @livewire('account.sidebar-nav', ['active_page' => 'quiz', 'banner_type' => 'summer_school'])
            </div>
            <div class="content__col content__col--right">
                <div class="content-header content-header--lesson">
                    <div class="content-header__container">
                        <div class="content-header__row content-header__row--diff-directions">
                            <div class="content-header__col">
                                <a class="content-header__btn content-header__btn--back" href="{{ route('account.myLessons') }}">Вернуться в Мои уроки</a>
                            </div>
                            @if(!$quiz_status->isSkipped())
                            <div class="content-header__col">
                                <a wire:click.prevent="skipQuiz()" class="btn-orange-empty-sm" href="#">Пропустить тест</a>
                            </div>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="content__inner-wrapper">
                    @if($this->hasquestions)
                    <div class="test-content">
                        <div class="test-content__header">
                            <div class="test-content__header-list">
                                @foreach($questionsData as $question_index => $question)
                                    <div wire:click.prevent="changeStep({{ $question_index + 1 }})" class="test-content__header-list-item{{ $this->stepPillsClasses($question_index) }}">{{ $question_index + 1 }}</div>
                                @endforeach
                            </div>
                        </div>
                        <div class="test-content__body">
                            @if(!$quiz_finished)
                                <div class="test-content__body-text-wrapper">
                                    <div class="test-content__title">Вопрос {{ $current_step }}&nbsp;из&nbsp;{{ count($questionsData) }}</div>
                                    <div class="test-content__text">{{ $this->currentquestion['body'] }}</div>
                                </div>
                                <form class="test-content__answer">
                                    @if(!$this->currentquestion['type']['has_options'])
                                        <input wire:change="compareOption($event.target.value)" class="test-content__input-text" type="text" value="{{ $this->questionsData[$this->current_step - 1]['given_answer'] }}" placeholder="Ответ">
                                    @elseif($this->currentquestion['type']['has_options'])
                                        <div class="test-content__checkbox-wrapper">
                                            @foreach($this->currentquestion['options'] as $option)
                                                <div class="test-content__checkbox-item">
                                                    <label class="test-content__checkbox-label" for="{{ $option['alias'] }}-option">
                                                        <input wire:change="selectOption('{{ $option['id'] }}', $event.target.checked)" class="test-content__checkbox-default" type="checkbox" name="{{ $option['alias'] }}-option" value="{{ $option['name'] }}" {{ collect($this->currentquestion['selected_options'])->search(function ($item) use($option) { return $option['id'] === $item['id']; }) !== false ? 'checked' : '' }}>
                                                        <div class="test-content__checkbox-text">{{ $option['name'] }}</div>
                                                        <div class="test-content__checkbox-mark"></div>
                                                    </label>
                                                </div>
                                            @endforeach
                                        </div>
                                    @endif
                                    <div class="test-content__btns-wrapper">
                                        <button wire:click.prevent="submitAnswer()" class="test-content__btn test-content__btn--submit" type="submit">Дать ответ</button>
                                        <a wire:click.prevent="nextStep()" class="test-content__btn test-content__btn--link" href="#">Пропустить</a>
                                    </div>
                                </form>
                            @else
                                <div class="test-content__body-text-wrapper">
                                    <div class="test-content__title">Результат</div>
                                    <div class="test-content__stat-results">
                                        <div class="test-content__stat-results-text">Правильные ответы:&nbsp;<span class="test-content__stat-results-text-number test-content__stat-results-text-number--green">{{ $right_answers }}</span></div>
                                        <div class="test-content__stat-results-text">Неправильные ответы:&nbsp;<span class="test-content__stat-results-text-number test-content__stat-results-text-number--orange">{{ $wrong_answers }}</span></div>
                                    </div>
                                    <div class="test-content__text">Есть несколько ошибок, но&nbsp;это не&nbsp;проблема!&nbsp;<br>Теперь ты&nbsp;можешь начать обучение и&nbsp;пройти первый вводный урок. Мы&nbsp;подобрали для тебя занятия таким образом, чтобы ты&nbsp;смог улучшить результаты по&nbsp;всем направлениям.</div>
                                </div>
                                <div class="test-content__btns-wrapper"><a class="test-content__btn test-content__btn--submit" href="{{ route('account.myLessons') }}">Начать обучение</a><a class="test-content__btn test-content__btn--white" href="#">Посмотреть ответы</a></div>
                            @endif
                        </div>
                    </div>
                    @else
                        <div class="test-content">
                            <div class="text-lg">Вопросы пока не добавлены.</div>
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>

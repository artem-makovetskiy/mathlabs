<div>
    <div class="content">
        <div class="content__container">
            <div class="content__col content__col--left">
                @livewire('account.sidebar-nav', ['banner_type' => 'summer_school', 'active_page' => 'settings'])
            </div>
            <div class="content__col content__col--right">
                <div class="settings">
                    <div class="settings__container">
                        @if (session()->has('message'))
                            <div class="alert alert-success flex items-center bg-green-100 text-green-500 rounded p-4 mb-6">
                                <div class="">
                                    <svg fill="none" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" viewBox="0 0 24 24" class="w-8 h-8"><path d="M9 12l2 2 4-4m6 2a9 9 0 11-18 0 9 9 0 0118 0z"></path></svg>
                                </div>
                                <div class="ml-3">{{ session('message') }}</div>
                            </div>
                        @endif
                        @if ($errors->any())
                            <div class="alert alert-danger flex items-center bg-red-100 text-red-500 rounded p-4 mb-6">
                                <div class="">
                                    <svg fill="none" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" viewBox="0 0 24 24" class="w-8 h-8"><path d="M12 9v2m0 4h.01m-6.938 4h13.856c1.54 0 2.502-1.667 1.732-3L13.732 4c-.77-1.333-2.694-1.333-3.464 0L3.34 16c-.77 1.333.192 3 1.732 3z"></path></svg>
                                </div>
                                <ul class="ml-3">
                                    @foreach ($errors->all() as $error)
                                        <li>* {{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        <form wire:submit.prevent="updateSettings" wire:key="settingsForm" class="settings__form">
                            <div class="settings__inner-wrapper">
                                <div class="settings__row">
                                    <div class="settings__title">Личная информация</div>
                                </div>
                                <div class="grid grid-cols-1 md:grid-cols-2 lg:grid-cols-3 gap-4 mb-4">
                                    <div class="col-auto order-1 lg:order-3">
                                        <div class="settings__item settings__item-grade">
                                            <div class="settings__label">Класс</div>
                                            <div class="settings__select-wrapper">
                                                <select wire:model.lazy="userData.grade" class="settings__select">
                                                    @foreach($this->grades as $grade_key => $grade)
                                                        <option class="settings__select-option" value="{{ $grade_key }}">{{ $grade }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-start-1 lg:col-auto order-2">
                                        <div class="settings__item">
                                            <div class="settings__label">Имя ученика</div>
                                            <input wire:model.lazy="userData.child_name" class="settings__text-input" type="text" value="" placeholder="">
                                        </div>
                                    </div>
                                    <div class="col-auto order-3 lg:order-1">
                                        <div class="settings__item">
                                            <div class="settings__label">Имя родителя</div>
                                            <input wire:model.lazy="userData.parent_name" class="settings__text-input" type="text" value="" placeholder="">
                                        </div>
                                    </div>
                                    <div class="col-auto order-4">
                                        <div class="settings__item">
                                            <div class="settings__label">E-mail</div>
                                            <input wire:model.lazy="userData.email" class="settings__text-input" type="text" value="" placeholder="@">
                                        </div>
                                    </div>
                                    <div class="col-auto order-5">
                                        <div class="settings__item">
                                            <div class="settings__label">Номер телефона</div>
                                            <input wire:model.lazy="userData.phone_number" class="settings__text-input settings__text-input--phone-number" type="text" value="" placeholder="+7 (___)___-__-__">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="settings__inner-wrapper">
                                <div class="settings__row">
                                    <div class="settings__title">Сменить пароль</div>
                                </div>
                                <div class="grid grid-cols-1 md:grid-cols-2 lg:grid-cols-3 gap-4 mb-4">
                                    <div class="col-auto">
                                        <div class="settings__item">
                                            <input wire:model.lazy="oldPassword" class="settings__text-input" type="text" value="" placeholder="Старый пароль">
                                        </div>
                                    </div>
                                    <div class="col-auto">
                                        <div class="settings__item">
                                            <input wire:model.lazy="password" class="settings__text-input" type="text" value="" placeholder="Новый пароль">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="settings__inner-wrapper">
                                <div class="hidden settings__row settings__row--big-indent">
                                    <div class="settings__item">
                                        <div class="settings__title">Платежная информация</div>
                                        <div class="settings__descr">Подключи карту и&nbsp;ты&nbsp;сможешь быстро, и&nbsp;удобно оплачивать занятия</div>
                                    </div>
                                </div>
                                <div class="hidden settings__row settings__row--big-indent"><a class="settings__bank-card settings__bank-card--add settings__bank-card--active" href="#">
                                        <div class="settings__bank-card-btn">Привязать карту</div></a>
                                    <div class="settings__bank-card settings__bank-card--mastercard">
                                        <div class="settings__bank-card-text">5160   0000   0000   0000</div>
                                        <div class="settings__bank-card-close"></div>
                                    </div>
                                    <div class="settings__bank-card settings__bank-card--visa">
                                        <div class="settings__bank-card-text">4160   0000   0000   0000</div>
                                        <div class="settings__bank-card-close"></div>
                                    </div>
                                </div>
                                <div class="settings__row">
                                    <button class="settings__btn" type="submit">Сохранить данные</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

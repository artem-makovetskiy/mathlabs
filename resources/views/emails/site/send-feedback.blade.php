@component('mail::message')
# Заявка с контакт формы

Имя: <b>{{ $name }}</b><br>
Email: <b>{{ $email }}</b><br>
Сообщение: {{ $message }}<br>

Thanks,<br>
{{ config('app.name') }}
@endcomponent

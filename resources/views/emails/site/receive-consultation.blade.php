@component('mail::message')
# Заявка на консультацию

Имя: <b>{{ $name }}</b><br>
Номер телефона: <b>{{ $phone_number }}</b><br>
Класс: <b>{{ $grade }}</b><br>

Спасибо,<br>
{{ config('app.name') }}
@endcomponent

<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Zoom WebSDK</title>
        <meta charset="utf-8" />
        <link type="text/css" rel="stylesheet" href="https://source.zoom.us/1.7.10/css/bootstrap.css" />
        <link type="text/css" rel="stylesheet" href="https://source.zoom.us/1.7.10/css/react-select.css" />
        <!-- <link type="text/css" rel="stylesheet" href="/vendor/zoomus/websdk/dist/css/bootstrap.css" />
        <link type="text/css" rel="stylesheet" href="/vendor/zoomus/websdk/dist/css/react-select.css" /> -->

        <meta name="format-detection" content="telephone=no">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    </head>

    <body>

        @yield('content')

        <!-- <script src="node_modules/react/umd/react.production.min.js"></script>
        <script src="node_modules/react-dom/umd/react-dom.production.min.js"></script>
        <script src="node_modules/redux/dist/redux.min.js"></script>
        <script src="node_modules/redux-thunk/dist/redux-thunk.min.js"></script>
        <script src="node_modules/lodash/lodash.min.js"></script>
        <script src="node_modules/jquery/dist/jquery.min.js"></script> -->

        <!-- Zoom SDK JS -->
        <!-- <script src="https://source.zoom.us/1.7.9/lib/vendor/react.min.js"></script>
        <script src="https://source.zoom.us/1.7.9/lib/vendor/react-dom.min.js"></script>
        <script src="https://source.zoom.us/1.7.9/lib/vendor/redux.min.js"></script>
        <script src="https://source.zoom.us/1.7.9/lib/vendor/redux-thunk.min.js"></script>
        <script src="https://source.zoom.us/1.7.9/lib/vendor/jquery.min.js"></script>
        <script src="https://source.zoom.us/1.7.9/lib/vendor/lodash.min.js"></script> -->
        <!-- <script src="/vendor/jquery/dist/jquery.min.js"></script> -->
        
        <script src="https://source.zoom.us/1.7.10/lib/vendor/react.min.js"></script>
        <script src="https://source.zoom.us/1.7.10/lib/vendor/react-dom.min.js"></script>
        <script src="https://source.zoom.us/1.7.10/lib/vendor/redux.min.js"></script>
        <script src="https://source.zoom.us/1.7.10/lib/vendor/redux-thunk.min.js"></script>
        <script src="https://source.zoom.us/1.7.10/lib/vendor/jquery.min.js"></script>
        <script src="https://source.zoom.us/1.7.10/lib/vendor/lodash.min.js"></script>
        <script src="https://source.zoom.us/zoom-meeting-1.7.10.min.js"></script>

        <!-- import ZoomMtg -->
        <!-- <script src="https://source.zoom.us/zoom-meeting-1.7.9.min.js"></script> -->

        <!-- <script src="{{ mix('js/zoom-stream.js') }}"></script> -->
        <script src="{{ asset('js/zoom-stream-test.js') }}"></script>
    </body>
</html>

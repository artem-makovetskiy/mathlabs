@extends('layouts.base')

@section('body')
    <header class="header header--inner">
        <div class="header__container">
            <div class="header__col header__col--left">
                <a class="header__logo" href="/"></a>
                @if(auth()->user()->hasRole('client'))
                    <div class="hidden header__reminder">
                        <div class="header__reminder-item header__reminder-item--active">Твоё следующее занятие завтра в&nbsp;18:00! Не&nbsp;пропусти! :)</div>
                        <div class="hidden header__reminder-item header__reminder-item--urgent">Срочно! Завтра в&nbsp;18:00! Не&nbsp;пропусти! :)</div>
                    </div>
                @endif
            </div>
            <div class="header__col header__col--right">
                @if(auth()->user()->hasRole('client'))
                    <a class="header__btn-number-of-paid" href="#"><span class="hidden sm:inline-block">Оплачено занятий:</span><div class="header__btn-text-bold">{{ \Illuminate\Support\Facades\Auth::user()->available_lessons }}</div></a>
                @endif
                <a class="header__user" href="#"><img class="header__user-avatar" src="{{ asset('img/icons/icon-avatar-example.svg') }}">
                <div class="header__user-text">Привет, {{ auth()->user()->name }}!</div></a>
                <a class="header__exit" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();"></a>
                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    @csrf
                </form>
            </div>
        </div>
    </header>
    @if(!Auth::user()->isAdmin())
        @livewire('account.nav-dropdown')
    @endif
    @yield('content')
    <div class="bg-secondary">
        <div class="container width-adaptive mx-auto text-white py-8">
            <div class="flex flex-col sm:flex-row justify-between space-y-8 sm:space-y-0 sm:space-x-8 mb-12 md:mb-32">
                <div class="w-full sm:w-1/2 lg:w-2/5">
                    <h3 class="text-xl sm:text-2xl font-bold mb-6">Команда MathLabs</h3>
                    <div class="text-sm md:text-xl opacity-75">Мы — преподаватели и единомышленники, которые помогают в изучении математики.</div>
                </div>
                <div class="w-full sm:w-1/2 lg:w-2/5">
                    <h3 class="text-xl sm:text-2xl font-bold mb-6">Если у вас есть вопросы – напишите нам!</h3>
                    <div class="footer__list-social"><a class="footer__list-social-item footer__list-social-item--telegram" href="#"></a><a class="footer__list-social-item footer__list-social-item--whatsapp" href="#"></a><a class="footer__list-social-item footer__list-social-item--viber" href="#"></a><a class="footer__list-social-item footer__list-social-item--facebook" href="#"></a></div>
                </div>
            </div>
            <div class="flex flex-col lg:flex-row justify-between space-y-8 lg:space-y-0 lg:space-x-8">
                <div class="text-base">© 2020 MathLabs. Все права защищиены</div>
                <div class="text-base"><a class="underline" href="#">Политика конфиденциальности</a></div>
                <div class="text-base"><a class="underline" href="#">Пользовательское соглашение</a></div>
            </div>
        </div>
    </div>
    <div class="hidden footer">
        <div class="footer__container">
            <div class="footer__row footer__row--top">
                <div class="footer__col">
                    <div class="footer__title">Команда MathLabs</div>
                    <div class="footer__descr">Мы&nbsp;&mdash; преподаватели и&nbsp;единомышленники, которые&nbsp;<br>помогают в&nbsp;изучении математики.</div>
                </div>
                <div class="footer__col">
                    <div class="footer__title footer__title--big-indent">Если у&nbsp;вас есть&nbsp;<br>вопросы&nbsp;&mdash; напишите нам!</div>
                    <div class="footer__list-social"><a class="footer__list-social-item footer__list-social-item--telegram" href="#"></a><a class="footer__list-social-item footer__list-social-item--whatsapp" href="#"></a><a class="footer__list-social-item footer__list-social-item--viber" href="#"></a><a class="footer__list-social-item footer__list-social-item--facebook" href="#"></a></div>
                </div>
            </div>
            <div class="footer__row footer__row--bottom">
                <div class="footer__col">
                    <div class="footer__text">&copy;&nbsp;2020&nbsp;MathLabs. Все права защищены</div>
                </div>
                <div class="footer__col"><a class="footer__link" href="#">Политика конфиденциальности</a></div>
                <div class="footer__col"><a class="footer__link" href="#">Пользовательское соглашение</a></div>
            </div>
        </div>
    </div>
@endsection

@extends('layouts.base')

@section('body')
    <div x-data="siteData()" x-on:scroll.window="atTop = (window.pageYOffset > 10) ? false : true;" class="">
        <header id="m-header" class="m-header--fixed" :class="{ 'm-header--scroll': !atTop }">
            <div class="container-fluid mx-auto">
                <div class="flex justify-between items-center px-4 sm:px-6 py-4 md:justify-start">
                    <div>
                        <a href="/" class="header__logo"></a>
                    </div>
                    <nav class="hidden xl:flex space-x-10 ml-8 sm:mt-1">
                        <a href="#online-classes" class="text-base whitespace-no-wrap js-smooth-scroll">Онлайн-занятия</a>
                        <a href="#mentors" class="text-base whitespace-no-wrap js-smooth-scroll">Преподаватели</a>
                        <a href="#pricing" class="text-base whitespace-no-wrap js-smooth-scroll">Стоимость</a>
                    </nav>
                    <div class="inline-flex items-center space-x-4 sm:space-x-8 ml-auto sm:mt-1">
                        <a href="tel:+74994904467" class="hidden xl:block text-lg font-bold whitespace-no-wrap">+7 (499) 490-44-67</a>
                        <a href="{{ route('site.sign-up-online') }}" class="btn-primary-lg w-auto hidden sm:block">Записаться</a>
                        <a href="{{ route('site.sign-up-online') }}" class="btn-primary-sm w-auto sm:hidden">Записаться</a>
                        <!-- TODO: hide later according to auth state -->
                        @guest
                            <a x-on:click.prevent="openAuthModal" class="header__link hidden sm:flex" href="#" data-modal="0"><span class="header__link-text">Войти</span></a>
                        @endguest
                        @if(Auth::user() && Auth::user()->hasRole('client'))
                            <a class="header__link hidden sm:flex" href="{{ route('account.myLessons') }}" data-modal="0"><span class="header__link-text">Кабинет</span></a>
                        @endif
                        @if(Auth::user() && Auth::user()->hasRole('admin'))
                            <a class="header__link hidden sm:flex" href="{{ route('admin.group-lessons.index') }}" data-modal="0"><span class="header__link-text">Кабинет</span></a>
                        @endif
                    </div>
                    <div class="mt-2 ml-2 sm:ml-8 xl:hidden">
                        <button @click="mobileMenuOpen = true" type="button" class="inline-flex items-center justify-center p-2 rounded-md text-gray-400 hover:text-gray-500 hover:bg-gray-100 focus:outline-none focus:bg-gray-100 focus:text-gray-500 transition duration-150 ease-in-out">
                            <svg class="h-6 w-6" stroke="currentColor" fill="none" viewBox="0 0 24 24">
                                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M4 6h16M4 12h16M4 18h16"/>
                            </svg>
                        </button>
                    </div>
                </div>
            </div>
            <div x-show="mobileMenuOpen" class="fixed inset-0 overflow-hidden" style="display: none;">
                <div class="absolute inset-0 overflow-hidden">
                    <section class="absolute inset-y-0 right-0 w-full xs:w-auto max-w-full flex">
                    <!--
                        Slide-over panel, show/hide based on slide-over state.

                        Entering: "transform transition ease-in-out duration-500 sm:duration-700"
                        From: "translate-x-full"
                        To: "translate-x-0"
                        Leaving: "transform transition ease-in-out duration-500 sm:duration-700"
                        From: "translate-x-0"
                        To: "translate-x-full"
                    -->
                    <div class="w-full xs:max-w-sm" x-show="mobileMenuOpen" x-transition:enter="transform transition ease-in-out duration-500 sm:duration-700" x-transition:enter-start="translate-x-full" x-transition:enter-end="translate-x-0" x-transition:leave="transform transition ease-in-out duration-500 sm:duration-700" x-transition:leave-start="translate-x-0" x-transition:leave-end="translate-x-full">
                        <div class="h-full flex flex-col space-y-6 pt-8 sm:pt-12 pb-6 bg-white shadow-xl overflow-y-scroll">
                            <header class="px-4 sm:px-6">
                                <div class="flex items-start justify-end space-x-3">
                                    {{-- <a href="/" class="header__logo"></a> --}}
                                    <div class="h-7 flex items-center">
                                        <button @click="mobileMenuOpen = false" type="button" class="inline-flex items-center justify-center p-2 rounded-md text-gray-400 hover:text-gray-500 hover:bg-gray-100 focus:outline-none focus:bg-gray-100 focus:text-gray-500 transition duration-150 ease-in-out">
                                            <svg class="h-6 w-6" stroke="currentColor" fill="none" viewBox="0 0 24 24">
                                                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M6 18L18 6M6 6l12 12"/>
                                            </svg>
                                        </button>
                                    </div>
                                </div>
                            </header>
                            <div class="relative flex-1 px-4 sm:px-6">
                                <div class="stack stack-lg">
                                    <nav class="stack stack-lg">
                                        <a @click="mobileMenuOpen = false" href="#online-classes" class="text-2xl whitespace-no-wrap js-smooth-scroll">Онлайн-занятия</a>
                                        <a @click="mobileMenuOpen = false" href="#mentors" class="text-2xl whitespace-no-wrap js-smooth-scroll">Преподаватели</a>
                                        <a @click="mobileMenuOpen = false" href="#pricing" class="text-2xl whitespace-no-wrap js-smooth-scroll">Стоимость</a>
                                    </nav>
                                    @guest
                                        <a x-on:click.prevent="openAuthModal" class="header__link" href="#" data-modal="0"><span class="header__link-text text-2xl">Войти</span></a>
                                    @endguest
                                    @if(Auth::user() && Auth::user()->hasRole('client'))
                                        <a class="header__link" href="{{ route('account.myLessons') }}" data-modal="0"><span class="header__link-text text-2xl">Кабинет</span></a>
                                    @endif
                                    @if(Auth::user() && Auth::user()->hasRole('admin'))
                                        <a class="header__link" href="{{ route('admin.group-lessons.index') }}" data-modal="0"><span class="header__link-text text-2xl">Кабинет</span></a>
                                    @endif
                                    <a href="tel:+74994904467" class="text-2xl font-bold whitespace-no-wrap">+7 (499) 490-44-67</a>
                                    <div class="text-center">
                                        <!-- <button type="button" class="btn-primary-lg" style="max-width: 22em;">Записаться</button> -->
                                        <a href="{{ route('site.sign-up-online') }}" class="btn-primary-lg" style="max-width: 22em;">Записаться</a>
                                    </div>
                                </div>
                            </div>
                            <div class="w-full px-4 sm:px-6 mt-auto">
                                <h3 class="text-xl sm:text-2xl font-bold mb-6">Если у вас есть вопросы – напишите нам!</h3>
                                <div class="footer__list-social"><a class="footer__list-social-item footer__list-social-item--telegram" href="#"></a><a class="footer__list-social-item footer__list-social-item--whatsapp" href="#"></a><a class="footer__list-social-item footer__list-social-item--viber" href="#"></a><a class="footer__list-social-item footer__list-social-item--facebook" href="#"></a></div>
                            </div>
                        </div>
                    </div>
                    </section>
                </div>
            </div>
        </header>
        <div class="header-placeholder"></div>
        @yield('content')
        <div class="bg-secondary">
            <div class="container width-adaptive mx-auto text-white py-8">
                <div class="flex flex-col sm:flex-row justify-between space-y-8 sm:space-y-0 sm:space-x-8 mb-12 md:mb-32">
                    <div class="w-full sm:w-1/2 lg:w-2/5">
                        <h3 class="text-xl sm:text-2xl font-bold mb-6">Команда MathLabs</h3>
                        <div class="text-sm md:text-xl opacity-75">Мы — преподаватели и единомышленники, которые помогают в изучении математики.</div>
                    </div>
                    <div class="w-full sm:w-1/2 lg:w-2/5">
                        <h3 class="text-xl sm:text-2xl font-bold mb-6">Если у вас есть вопросы – напишите нам!</h3>
                        <div class="footer__list-social"><a class="footer__list-social-item footer__list-social-item--telegram" href="#"></a><a class="footer__list-social-item footer__list-social-item--whatsapp" href="#"></a><a class="footer__list-social-item footer__list-social-item--viber" href="#"></a><a class="footer__list-social-item footer__list-social-item--facebook" href="#"></a></div>
                    </div>
                </div>
                <div class="flex flex-col lg:flex-row justify-between space-y-8 lg:space-y-0 lg:space-x-8">
                    <div class="text-base">© 2020 MathLabs. Все права защищиены</div>
                    <div class="text-base"><a class="underline" href="#">Политика конфиденциальности</a></div>
                    <div class="text-base"><a class="underline" href="#">Пользовательское соглашение</a></div>
                </div>
            </div>
        </div>
        <div x-show.transition.opacity="isOpenAuthModal()" class="" style="display: none;">
            <livewire:site.auth-modal/>
        </div>
        <div x-show.transition.opacity="isOpenGetHelpModal()" class="" style="display: none;">
            <livewire:site.receive-consultation-modal/>
        </div>
        <script>
            (function () {
                /* var header = document.getElementById('m-header');
                console.log(header);
                window.addEventListener('scroll', function(event) {
                    header.classList.add('m-header--fixed');
                }); */
            })();
        </script>
    </div>
@endsection

@push('scripts')
    <script src="https://cdn.jsdelivr.net/gh/alpinejs/alpine@v2.x.x/dist/alpine.min.js" defer></script>
    <script src="{{ asset('vendor/jquery/dist/jquery.min.js') }}"></script>
    <script src="{{ asset('js/utils/util.js') }}"></script>
    <script src="{{ asset('js/utils/smooth-scrolling.js') }}"></script>
    <script src="{{ asset('vendor/owl-carousel/owl.carousel.min.js') }}"></script>
    <script>
        function siteData() {
            return {
                mobileMenuOpen: false,
                authModalOpened: false,
                getHelpModalOpened: false,
                activePanel: 'login', // register
                activeClassMapper: {
                    'login': ''
                },
                panelActiveClass: 'modal-auth-reg__nav-item--active',
                atTop: window.pageYOffset < 10,

                openAuthModal() { this.authModalOpened = true },
                closeAuthModal() { this.authModalOpened = false },
                isOpenAuthModal() { return this.authModalOpened === true },
                checkAuthModal() { console.log(this.isOpenAuthModal()) },

                isOpenGetHelpModal() { return this.getHelpModalOpened === true },
                openGetHelpModal() { this.getHelpModalOpened = true },
                closeGetHelpModal() { this.getHelpModalOpened = false },
                checkGetHelpModal() { console.log(this.isOpenGetHelpModal()) },

                setActivePanel(panelName) { this.activePanel = panelName },
            }
        }

        $(document).ready(function(){
            $(".owl-carousel").owlCarousel({
                items: 1,
                margin: 25,
                nav: true,
                navContainerClass: 'custom_slider__arrows',
                navClass: ['custom_slider__arr', 'custom_slider__arr']
            });
        });

        // (function() {
        //     $(".owl-carousel").owlCarousel();
        // }());
    </script>
@endpush
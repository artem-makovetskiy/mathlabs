require("./bootstrap");

// require('jquery/dist/jquery.min');
// var $ = require( "jquery" );
// import { $ } from 'jquery'
import { ZoomMtg } from "@zoomus/websdk";

console.log(signature);
console.log("checkSystemRequirements");
console.log(JSON.stringify(ZoomMtg.checkSystemRequirements()));

// ZoomMtg.setZoomJSLib(appUrl + "/vendor/zoomus/websdk/dist/lib", "/av");
// ZoomMtg.setZoomJSLib("https://dmogdx0jrul3u.cloudfront.net/1.7.10/lib", "/av");
ZoomMtg.setZoomJSLib("https://source.zoom.us/1.7.10/lib", "/av");

ZoomMtg.preLoadWasm();
ZoomMtg.prepareJssdk();

console.log(zoomConfig);

const meetConfig = {
    apiKey: zoomConfig.api_key,
    apiSecret: zoomConfig.api_secret,
    meetingNumber: meetingNumber,
    leaveUrl: leaveUrl,
    userName: userName,
    // userEmail: zoomConfig.user_email, // required for webinar
    passWord: meetingPassword, // if required
    role: meetingRole // 1 for host; 0 for attendee or webinar
};

console.log(meetConfig);

// const SIGNATURE_ENDPOINT = '';

ZoomMtg.init({
    leaveUrl: meetConfig.leaveUrl,
    success() {
        ZoomMtg.join({
            meetingNumber: meetConfig.meetingNumber,
            userName: meetConfig.userName,
            signature: signature,
            apiKey: meetConfig.apiKey,
            passWord: meetConfig.passWord,
            success() {
                ZoomMtg.showJoinAudioFunction({
                    show: false
                });
                // $('#nav-tool').hide();
                console.log("join meeting success");
            },
            error(res) {
                console.log(res);
            }
        });
    },
    error(res) {
        console.log(res);
    }
});

// ZoomMtg.generateSignature({
//     meetingNumber: meetConfig.meetingNumber,
//     apiKey: meetConfig.apiKey,
//     apiSecret: meetConfig.apiSecret,
//     role: meetConfig.role,
//     success(res) {
//         console.log('signature', res.result);
//         ZoomMtg.init({
//             leaveUrl: 'http://mathlabs.test/admin/webinars',
//             success() {
//                 ZoomMtg.join(
//                     {
//                         meetingNumber: meetConfig.meetingNumber,
//                         userName: meetConfig.userName,
//                         signature: res.result,
//                         apiKey: meetConfig.apiKey,
//                         passWord: meetConfig.passWord,
//                         success() {
//                             $('#nav-tool').hide();
//                             console.log('join meeting success');
//                         },
//                         error(res) {
//                             console.log(res);
//                         }
//                     }
//                 );
//             },
//             error(res) {
//                 console.log(res);
//             }
//         });
//     }
// });

// function getSignature(meetConfig) {
//     fetch(`${SIGNATURE_ENDPOINT}`, {
//         method: 'POST',
//         body: JSON.stringify({ meetingData: meetConfig })
//     })
//         .then(result => result.text())
//         .then(response => {
//             ZoomMtg.init({
//                 leaveUrl: meetConfig.leaveUrl,
//                 isSupportAV: true,
//                 success: function() {
//                     ZoomMtg.join({
//                         signature: response,
//                         apiKey: meetConfig.apiKey,
//                         meetingNumber: meetConfig.meetingNumber,
//                         userName: meetConfig.userName,
//                         // Email required for Webinars
//                         userEmail: meetConfig.userEmail,
//                         // password optional; set by Host
//                         password: meetConfig.passWord,
//                         error(res) {
//                             console.log(res)
//                         }
//                     })
//                 }
//             })
//         })
// }

// ZoomMtg.setZoomJSLib('https://dmogdx0jrul3u.cloudfront.net/1.7.7/lib', '/av');

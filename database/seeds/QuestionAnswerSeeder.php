<?php

use Illuminate\Database\Seeder;

class QuestionAnswerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \Illuminate\Support\Facades\DB::table('question_answers')->insert([
            [
                'answer_text' => 'Question answer text',
                'question_id' => 1,
            ],
            [
                'answer_text' => 'Question answer text',
                'question_id' => 2,
            ],
            [
                'answer_text' => 'Question answer text',
                'question_id' => 3,
            ],
        ]);
    }
}

<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
       $this->call(RoleSeeder::class);
       $this->call(AdminSeeder::class);
       $this->call(PaymentPlanSeeder::class);
        // $this->call(GroupLessonsSeeder::class);
        // $this->call(WebinarsSeeder::class);

        // Question/Answer seeders
        $this->call(QuestionTypeSeeder::class);
        // $this->call(QuestionSeeder::class);
        // $this->call(QuestionOptionSeeder::class);
        // $this->call(QuestionAnswerSeeder::class);
        
    }
}

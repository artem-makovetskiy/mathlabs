<?php

use Illuminate\Database\Seeder;

class WebinarsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \Illuminate\Support\Facades\DB::table('webinars')->insert([
            [
                'uuid' => (string) \Illuminate\Support\Str::uuid(),
                'title' => 'Интенсив по высшей математике',
                'brief_desc' => 'Интенсив прорабатывает необходимую теорию, учит решать сложные задания из олимпиады Физтех по математике, правильно выстраивая логику рассуждений.',
                'price' => '500',
                'grade' => '11 класс',
                'event_date' => now()->addDays(4),
                'author_id' => \App\User::where('email', 'admin@gmail.com')->first()->id,
                'created_at' => now()
            ],
            [
                'uuid' => (string) \Illuminate\Support\Str::uuid(),
                'title' => 'Подготовка к ЕГЭ по базовой математике',
                'brief_desc' => 'Курс прорабатывает необходимую теорию, учит решать сложные задания из олимпиады Физтех по математике, правильно выстраивая логику рассуждений. На занятиях обсуждаются олимпиадные задачи прошлых лет по математике и разбираются методы их решения.',
                'price' => '1190',
                'grade' => '11 класс',
                'event_date' => now()->addDays(7),
                'author_id' => \App\User::where('email', 'admin@gmail.com')->first()->id,
                'created_at' => now()
            ],
        ]);
    }
}

<?php

use Illuminate\Database\Seeder;

class QuestionOptionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \Illuminate\Support\Facades\DB::table('question_options')->insert([
            // Options for Question #1
            [
                'name' => 'The only solution.',
                'alias' => 'sol-1',
                'is_solution' => true,
                'question_id' => 1,
            ],
            // Options for Question #2
            [
                'name' => 'Solution #1',
                'alias' => 'sol-1',
                'is_solution' => false,
                'question_id' => 2,
            ],
            [
                'name' => 'Solution #2',
                'alias' => 'sol-2',
                'is_solution' => true,
                'question_id' => 2,
            ],
            [
                'name' => 'Solution #2',
                'alias' => 'sol-3',
                'is_solution' => false,
                'question_id' => 2,
            ],
            // Options for Question #3
            [
                'name' => 'Solution #1',
                'alias' => 'sol-1',
                'is_solution' => true,
                'question_id' => 3,
            ],
            [
                'name' => 'Solution #2',
                'alias' => 'sol-2',
                'is_solution' => false,
                'question_id' => 3,
            ],
            [
                'name' => 'Solution #3',
                'alias' => 'sol-3',
                'is_solution' => true,
                'question_id' => 3,
            ],
            [
                'name' => 'Solution #4',
                'alias' => 'sol-4',
                'is_solution' => false,
                'question_id' => 3,
            ],
        ]);
    }
}

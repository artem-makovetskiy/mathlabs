<?php

use Illuminate\Database\Seeder;

class PaymentPlanSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \Illuminate\Support\Facades\DB::table('payment_plans')->insert([
            [
                'lessons_amount' => 1,
                'price' => '1190',
                'desc' => 'Хорошо для старта',
                'savings_amount' => '0.00',
                'featured' => false,
            ],
            [
                'lessons_amount' => 4,
                'price' => '990',
                'desc' => '2 неделя учёбы (два занятия в неделю)',
                'savings_amount' => '700',
                'featured' => false,
            ],
            [
                'lessons_amount' => 8,
                'price' => '890',
                'desc' => '1 месяц учёбы (два занятия в неделю)',
                'savings_amount' => '2100',
                'featured' => false,
            ],
            [
                'lessons_amount' => 16,
                'price' => '790',
                'desc' => '1 месяц учёбы (два занятия в неделю)',
                'savings_amount' => '4200',
                'featured' => false,
            ],
            [
                'lessons_amount' => 32,
                'price' => '690',
                'desc' => '1 месяц учёбы (два занятия в неделю)',
                'savings_amount' => '8400',
                'featured' => false,
            ],
        ]);
    }
}

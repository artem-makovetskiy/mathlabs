<?php

use Illuminate\Database\Seeder;

class QuestionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \Illuminate\Support\Facades\DB::table('questions')->insert([
            [
                'title' => 'Question #1',
                'body' => 'Question description',
                'is_required' => true,
                'is_enabled' => true,
                'sort_order' => 1,
                'question_type_id' => 1,
            ],
            [
                'title' => 'Question #2',
                'body' => 'Question description 2',
                'is_required' => true,
                'is_enabled' => true,
                'sort_order' => 2,
                'question_type_id' => 2,
            ],
            [
                'title' => 'Question #3',
                'body' => 'Question description 3',
                'is_required' => false,
                'is_enabled' => true,
                'sort_order' => 3,
                'question_type_id' => 3,
            ],
        ]);
    }
}

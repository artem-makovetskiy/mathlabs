<?php

use Illuminate\Database\Seeder;

class QuestionTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \Illuminate\Support\Facades\DB::table('question_types')->insert([
            [
                'alias' => 'simple',
                'name' => 'simple',
                'has_options' => false,
                'allow_multiple' => false,
            ],
            [
                'alias' => 'complex',
                'name' => 'complex',
                'has_options' => true,
                'allow_multiple' => false,
            ],
            [
                'alias' => 'complex_plus',
                'name' => 'complex_plus',
                'has_options' => true,
                'allow_multiple' => true,
            ],
        ]);
    }
}

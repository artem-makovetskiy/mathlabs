<?php

use Illuminate\Database\Seeder;
use App\Role;
use App\User;

class AdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $admin = Role::where('slug','admin')->first();

        $admin_user = new User();
//        $admin_user->name = 'John Smith';
        $admin_user->email = 'admin@gmail.com';
        $admin_user->password = bcrypt('uJ#d@jnP88aHR3MN');
        // $admin_user->password = bcrypt('secret');
        $admin_user->save();
        $admin_user->roles()->attach($admin);
    }
}

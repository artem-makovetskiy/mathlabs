<?php

use Illuminate\Database\Seeder;

class GroupLessonsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \Illuminate\Support\Facades\DB::table('group_lessons')->insert([
            [
                'uuid' => (string) \Illuminate\Support\Str::uuid(),
                'title' => 'Подготовка к ЕГЭ по базовой математике',
                'brief_desc' => 'Курс прорабатывает необходимую теорию, учит решать сложные задания из олимпиады Физтех по математике, правильно выстраивая логику рассуждений. На занятиях обсуждаются олимпиадные задачи прошлых лет по математике и разбираются методы их решения.',
                'price' => '1190',
                'grade' => '11 класс',
                'author_id' => \App\User::where('email', 'admin@gmail.com')->first()->id,
                'created_at' => now()
            ],
            [
                'uuid' => (string) \Illuminate\Support\Str::uuid(),
                'title' => 'Подготовка к ЕГЭ #2 по базовой математике',
                'brief_desc' => 'Курс прорабатывает необходимую теорию, учит решать сложные задания из олимпиады Физтех по математике, правильно выстраивая логику рассуждений. На занятиях обсуждаются олимпиадные задачи прошлых лет по математике и разбираются методы их решения.',
                'price' => '1290',
                'grade' => '11 класс',
                'author_id' => \App\User::where('email', 'admin@gmail.com')->first()->id,
                'created_at' => now()
            ],
        ]);
    }
}

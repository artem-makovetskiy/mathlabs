<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePaymentChargesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payment_charges', function (Blueprint $table) {
            $table->id();
//            $table->unsignedSmallInteger('lessons_amount');
            $table->decimal('charge_amount', 13, 2);
            $table->unsignedBigInteger('payment_id')->nullable();
            $table->string('status', 50)->nullable(); // NEW, REJECTED
            $table->unsignedBigInteger('user_id');
            $table->foreign('user_id')
                ->references('id')->on('users')
                ->onDelete('cascade');
            $table->unsignedBigInteger('payment_plan_id');
            $table->foreign('payment_plan_id')
                ->references('id')->on('payment_plans')
                ->onDelete('cascade');
//            $table->morphs('lessonable');
            $table->timestamp('payment_verified_at')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payment_charges');
    }
}

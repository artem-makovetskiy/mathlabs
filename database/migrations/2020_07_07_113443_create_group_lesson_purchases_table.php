<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGroupLessonPurchasesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('group_lesson_purchases', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('user_id');
            $table->foreign('user_id')
                ->references('id')->on('users')
                ->onDelete('cascade');
            $table->unsignedBigInteger('group_lesson_id');
            $table->foreign('group_lesson_id')
                ->references('id')->on('group_lessons')
                ->onDelete('cascade');
            $table->unsignedBigInteger('group_id')->nullable();
            // $table->foreign('group_id')->references('id')->on('groups')
            //     ->onDelete('set null');
            $table->json('scheduled_time');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('group_lesson_purchases');
    }
}

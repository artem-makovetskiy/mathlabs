<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGroupsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('groups', function (Blueprint $table) {
            $table->id();
            $table->string('lesson_time');
            $table->string('lesson_weekday');
            $table->boolean('active')->default(true);
            $table->boolean('schedule_meeting')->default(false);
            $table->unsignedBigInteger('group_lesson_id');
            $table->foreign('group_lesson_id')
                ->references('id')->on('group_lessons')
                ->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('groups');
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateQuizStatusTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('quiz_status', function (Blueprint $table) {
            $table->id();
            $table->smallInteger('right_answers')->nullable();
            $table->smallInteger('wrong_answers')->nullable();
            $table->smallInteger('current_step');
            $table->string('result_grade')->nullable();
            $table->timestamp('quiz_passed_at')->nullable();
            $table->timestamp('quiz_skipped_at')->nullable();
            $table->unsignedBigInteger('user_id');
            $table->foreign('user_id')
                ->references('id')->on('users')
                ->onDelete('cascade');
            $table->timestamps();

//            - score integer
//            - right_answers integer
//            - wrong_answers integer
//            - result_grade varchar
//            - current_step integer
//            - quiz_passed_at timestamp nullable
//            - user_id
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('quiz_status');
    }
}

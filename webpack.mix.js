const mix = require("laravel-mix");

require("laravel-mix-tailwind");

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js("resources/js/app.js", "public/js/app.js")
    .js("resources/js/zoom-stream.js", "public/js/zoom-stream.js")
    .extract()
    // .copyDirectory("resources/js/utils", "public/js/utils")
    .sass("resources/sass/app.scss", "public/css")
    .sass("resources/sass/tailwind-utilities.scss", "public/css")
    .combine(
        [`public/css/app.css`, `public/css/tailwind-utilities.css`],
        `public/css/app-build.css`
    )
    .tailwind("./tailwind.config.js")
    // .options({
    //     watchOptions: {
    //         ignored: /node_modules/
    //     }
    // });
    // .extract(['jquery', 'moment', 'alpinejs', 'react', 'react-dom', '@zoomus/websdk'])
    .sourceMaps();

// mix.copyDirectory('node_modules/@zoomus/websdk/dist', 'public/vendor/zoomus/websdk/dist')
//     .copyDirectory('node_modules/jquery/dist', 'public/vendor/jquery/dist');


if (mix.inProduction()) {
    mix.version();
}

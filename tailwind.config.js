const defaultTheme = require('tailwindcss/defaultTheme');

module.exports = {
    theme: {
        extend: {
            screens: {
                'xs': '480px', // 512px
                '2xl': '1440px'
            },
            fontFamily: {
                sans: ['Inter var', ...defaultTheme.fontFamily.sans],
            },
            colors: {
                'primary': '#6D59FB',
                'secondary': '#141D30',
                'blue-lighter': 'rgba(100, 75, 236, 0.05)',
                'blue-light': 'rgba(100, 75, 236, 0.1)'
            },
            spacing: {
                '5%': '5%',
                '10%': '10%',
                '15%': '15%',
                '20%': '205%'
                // sm: '8px',
                // md: '16px',
                // lg: '24px',
                // xl: '48px',
            }
        },
    },
    variants: {},
    purge: {
        content: [
            './app/**/*.php',
            './resources/**/*.html',
            './resources/**/*.js',
            './resources/**/*.jsx',
            './resources/**/*.ts',
            './resources/**/*.tsx',
            './resources/**/*.php',
            './resources/**/*.vue',
            './resources/**/*.twig',
        ],
        options: {
            defaultExtractor: (content) => content.match(/[\w-/.:]+(?<!:)/g) || [],
            whitelistPatterns: [/-active$/, /-enter$/, /-leave-to$/, /show$/],
        },
    },
    plugins: [
        require('@tailwindcss/custom-forms'),
        require('@tailwindcss/ui'),
    ],
};

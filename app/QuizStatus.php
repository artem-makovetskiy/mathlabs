<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class QuizStatus extends Model
{
    protected $table = 'quiz_status';

    protected $fillable = [
        'right_answers',
        'wrong_answers',
        'current_step',
        'result_grade',
        'quiz_passed_at',
        'quiz_skipped_at',
        'user_id',
    ];

    protected $casts = [
    ];

    public function isPassed() : bool
    {
        return !is_null($this->quiz_passed_at);
    }

    public function markAsPassed()
    {
        $this->quiz_passed_at = now();
        $this->save();
        return $this;
    }

    public function isSkipped() : bool
    {
        return !is_null($this->quiz_skipped_at);
    }

    public function markAsSkipped()
    {
        $this->quiz_skipped_at = now();
        $this->save();
        return $this;
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }
}

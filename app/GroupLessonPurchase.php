<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GroupLessonPurchase extends Model
{
    protected $table = 'group_lesson_purchases';

    protected $fillable = [
        'scheduled_time',
        'group_lesson_id',
        'user_id',
    ];

    protected $casts = [
        'scheduled_time' => 'array',
    ];

    public static function getScheduledTimeDefaults()
    {
        return [
            'weekdays' => ['Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб', 'Вс'],
            'time' => ['9:00', '10:00', '11:00', '12:00', '13:00', '14:00', '15:00', '16:00', '17:00', '18:00', '19:00', '20:00']
        ];
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function lesson()
    {
        return $this->belongsTo('App\GroupLesson', 'group_lesson_id');
    }

    public function group()
    {
        return $this->belongsTo('App\Group');
    }
}

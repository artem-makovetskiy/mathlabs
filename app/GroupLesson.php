<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;
use Illuminate\Support\Str;

class GroupLesson extends Model
{
    protected $table = 'group_lessons';

    protected $fillable = [
        'title',
        'brief_desc',
        'full_desc',
        'price',
        'grade',
        'goal',
        'individual',
        'published',
        'author_id'
    ];

    protected $casts = [
        'published' => 'boolean',
        'individual' => 'boolean',
        'price' => 'integer',
    ];

    protected $dates = [
    ];

    protected $appends = ['type_label', 'type', 'color', 'tags'];

    protected static function boot()
    {
        parent::boot();

        static::creating(function ($lesson) {
            $lesson->uuid = (string) Str::uuid();
        });
    }

    public function users()
    {
        return $this->belongsToMany('App\User', 'group_lesson_purchases', 'group_lesson_id', 'user_id');
    }

    public function purchases()
    {
        return $this->hasMany('App\GroupLessonPurchase');
    }

    public function payment_charges()
    {
        return $this->morphOne('App\PaymentCharge', 'lessonable');
    }

    public function meetings()
    {
        return $this->morphMany('App\Meeting', 'lesson');
    }

    public function groups()
    {
        return $this->hasMany('App\Group');
    }

    public function getTypeAttribute()
    {
        return $this->individual ? 'individual' : 'group';
    }

    public function getTypeLabelAttribute() {
        return $this->typeLabelsMapper()[$this->type];
    }

   public function getColorAttribute() {
       return $this->colorsTypeMapper()[$this->type];
   }

//     public function hasGroup()
//    {
//        return $this->users()->whereNotNull('group_lesson_purchases.group_id')->count() > 0;
//        // return $this->groups()->count() > 0;
//    }

    public function colorsTypeMapper() {
        return [
            'webinar' => 'orange',
            'group' => 'purple',
            'individual' => 'blue'
        ];
    }

    public function typeLabelsMapper() {
        return [
            'webinar' => 'Интенсив',
            'group' => 'В группе',
            'individual' => 'Индивидуальное'
        ];
    }

    public function getTagsAttribute()
    {
        return collect([
            'type' => $this->type_label,
            'grade' => $this->grade,
        ]);
    }

    public static function getGradeOptions() {
        return [
            '1 класс',
            '2 класс',
            '3 класс',
            '4 класс',
            '5 класс',
            '6 класс',
            '11 класс',
        ];
    }

    public static function getLabel()
    {
        return 'В группе';
    }
}

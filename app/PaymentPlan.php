<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PaymentPlan extends Model
{
    protected $table = 'payment_plans';

    protected $fillable = [
        'lessons_amount',
        'price',
        'savings_amount',
        'item_name',
        'desc',
        'featured',
    ];

    protected $casts = [
        'price' => 'float',
        'lessons_amount' => 'integer',
        'savings_amount' => 'integer',
        'published' => 'boolean',
    ];

    public function payment_charges()
    {
        return $this->hasMany('App\PaymentCharge');
    }

    public function getFullPriceAttribute()
    {
        return $this->lessons_amount * $this->price;
    }

    public function getPriceInCentsAttribute()
    {
        return bcmul($this->price, 100);
    }

    public function getFullPriceInCentsAttribute()
    {
        return $this->lessons_amount * bcmul($this->price, 100);
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class QuestionOption extends Model
{
    protected $table = 'question_options';

    protected $fillable = [
        'name',
        'alias',
        'is_solution',
        'question_id',
    ];

    protected $casts = [
        'is_solution' => 'boolean',
    ];

    public function question()
    {
        return $this->belongsTo('App\Question');
    }
}

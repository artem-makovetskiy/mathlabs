<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Question extends Model
{
    protected $table = 'questions';

    protected $fillable = [
        'title',
        'body',
        'grade',
        'is_required',
        'is_enabled',
        'sort_order',
        'question_type_id',
    ];

    protected $casts = [
        'is_required' => 'boolean',
        'is_enabled' => 'boolean',
    ];

//    protected $appends = ['type'];

    public function type()
    {
        return $this->belongsTo('App\QuestionType', 'question_type_id');
    }

    public function options()
    {
        return $this->hasMany('App\QuestionOption');
    }

    public static function getLabel()
    {
        return 'Вопросы для теста';
    }

    public static function getTypeOptions() {
        return QuestionType::all()->map(function ($item) {
            return [
                'name' => $item->name,
                'value' => $item->id
            ];
        })->toArray();
    }

    public static function getMaxOrderNum()
    {
        return self::query()->max('sort_order');
    }
}

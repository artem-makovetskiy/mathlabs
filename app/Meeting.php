<?php

namespace App;

use App\Notifications\LessonTimeApproved;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Notification;
use Illuminate\Support\Carbon;

class Meeting extends Model
{
    protected $table = 'meetings';

    protected $fillable = [
        'topic',
        'meeting_id',
        'password',
        'meeting_time',
        'recurring_weekday',
        'recurring_end_date',
        'recurring_end_times',
        'user_id',
        'group_id',
    ];

    protected $casts = [
    ];

    protected $dates = [
        'meeting_time',
        'recurring_end_date'
    ];

    protected $appends = ['next_meeting_time', 'recurring_time', 'upcoming_event_dates'];

    // protected static function booted()
    // {
    //     static::created(function ($meeting) {
    //         if ($meeting->lesson->type === 'group' && $meeting->lesson->hasGroup()) {
    //             Notification::send(new LessonTimeApproved($meeting));
    //         }
    //     });
    // }

    public function lesson()
    {
        return $this->morphTo();
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function group()
    {
        return $this->belongsTo('App\Group');
    }

    public function getMeetingTimeAttribute($value)
    {
        return Carbon::parse($value)->format(self::getDateTimeFormat());
    }

    public function getNextMeetingTimeAttribute()
    {
        return now()->next($this->getWeekdayNumber($this->recurring_weekday))->setTimeFrom(Carbon::parse($this->meeting_time))->format($this->getDateTimeFormat());
    }

    public function getRecurringTimeAttribute()
    {
        return $this->recurring_weekday . ', ' . Carbon::parse($this->meeting_time)->format('H:i');
    }

    public function getUpcomingEventDatesAttribute()
    {
        $dates = collect([]);
        if (!is_null($this->recurring_weekday)) {
            $dates->push(now()->next($this->getWeekdayNumber($this->recurring_weekday))->format('d-m-Y'));
        } else {
            $dates->push(Carbon::parse($this->meeting_time)->format('d-m-Y'));
        }
        return $dates;
    }

    public function getWeekdayNumber($weekday)
    {
        $weekdaysMapper = [
            'Пн' => 1,
            'Вт' => 2,
            'Ср' => 3,
            'Чт' => 4,
            'Пт' => 5,
            'Сб' => 6,
            'Вс' => 7,
        ];
        return $weekdaysMapper[$weekday];
    }

    public static function getDateTimeFormat()
    {
        return 'd.m.Y H:i';
    }

    public static function getNumOfLessonsOptions()
    {
        return [1,2,3,4,5,6,7,8,9,10];
    }
}

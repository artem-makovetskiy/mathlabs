<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Group extends Model
{
    protected $table = 'groups';

    protected $fillable = [
        'lesson_time',
        'lesson_weekday',
        'active',
        'schedule_meeting',
        'group_lesson_id'
    ];

    protected $casts = [
        'active' => 'boolean',
        'schedule_meeting' => 'boolean',
    ];

    protected $appends = ['scheduled_time'];

    protected $with = ['meetings'];

    public function users()
    {
        return $this->belongsToMany('App\User', 'user_group', 'group_id', 'user_id');
    }

    public function lesson()
    {
        return $this->belongsTo('App\GroupLesson', 'group_lesson_id');
    }

    public function group_lesson_purchases()
    {
        return $this->hasMany('App\GroupLessonPurchase');
    }

    public function meetings()
    {
        return $this->hasMany('App\Meeting');
    }

    public function getScheduledTimeAttribute()
    {
        return $this->lesson_weekday . ': ' . $this->lesson_time;
    }

//    public function getUpcomingEventDatesAttribute()
//     {
//         $dates = collect([]);
//         $dates->push(now()->next($this->getWeekdayNumber($this->lesson_weekday))->format('d-m-Y'));
//         return $dates;
//     }
    
    public function getWeekdayNumber($weekday)
    {
        $weekdaysMapper = [
            'Пн' => 1,
            'Вт' => 2,
            'Ср' => 3,
            'Чт' => 4,
            'Пт' => 5,
            'Сб' => 6,
            'Вс' => 7,
        ];
        return $weekdaysMapper[$weekday];
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class QuestionAnswer extends Model
{
    protected $table = 'question_answers';

    protected $fillable = [
        'answer_text',
        'question_id',
    ];

    protected $casts = [
    ];
}

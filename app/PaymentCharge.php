<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PaymentCharge extends Model
{
    protected $table = 'payment_charges';

    protected $fillable = [
        'charge_amount',
        'payment_id',
        'status',
        'payment_verified_at',
        'user_id',
        'payment_plan_id'
    ];

    protected $casts = [
        'charge_amount' => 'float',
        'payment_verified_at' => 'datetime',
    ];

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function payment_plan()
    {
        return $this->belongsTo('App\PaymentPlan');
    }

//    public function lesson()
//    {
//        return $this->morphTo('lessonable');
//    }

    public function isVerified() : bool
    {
        return !is_null($this->payment_verified_at);
    }

    public function markAsVerified()
    {
        $this->payment_verified_at = now();
        $this->save();
        return $this;
    }
}

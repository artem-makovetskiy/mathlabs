<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class QuestionType extends Model
{
    protected $table = 'question_types';

    protected $fillable = [
        'alias',
        'name',
        'has_options',
        'allow_multiple',
    ];

    protected $casts = [
        'has_options' => 'boolean',
        'allow_multiple' => 'boolean',
    ];

    public function questions()
    {
        return $this->HasMany('App\Question');
    }
}

<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use App\Traits\HasRolesAndPermissions;

class User extends Authenticatable
{
    use Notifiable, HasRolesAndPermissions;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'parent_name', 'child_name', 'email', 'password', 'phone_number', 'grade'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function webinars()
    {
        return $this->belongsToMany('App\Webinar', 'user_webinar', 'user_id', 'webinar_id');
    }

    public function payment_charges()
    {
        return $this->hasMany('App\PaymentCharge');
    }

    public function meetings()
    {
        return $this->hasMany('App\Meeting');
    }

    public function group_lesson_purchases()
    {
        return $this->hasMany('App\GroupLessonPurchase');
    }

    public function quiz_status()
    {
        return $this->hasOne('App\QuizStatus');
    }

    public function isAdmin()
    {
        return $this->hasRole('admin');
    }

    public function notificationCount()
    {
        return $this->notifications->count();
    }

    public function getNameAttribute()
    {
        return $this->isAdmin() ? 'Admin' : $this->child_name;
    }

    public static function getGradeList()
    {
        return [
            '1' => '1 класс',
            '2' => '2 класс',
            '3' => '3 класс',
            '4' => '4 класс',
            '5' => '5 класс',
            '6' => '6 класс',
            '7' => '7 класс',
            '8' => '8 класс',
            '9' => '9 класс',
            '10' => '10 класс',
            '11' => '11 класс',
        ];
    }
}

<?php

namespace App\Helpers;

use App\GroupLesson;
use App\Webinar;

class Lesson
{
  public static function getByTypeAndUuid($type, $uuid)
  {
    switch ($type) {
        case 'group':
            $query = GroupLesson::query();
            break;
        case 'individual':
            $query = GroupLesson::query();
            break;
        case 'webinar':
            $query = Webinar::query();
            break;
    }
    return $query->whereUuid($uuid)->first();
  }

  public static function getQueryByType($type)
  {
    switch ($type) {
        case 'in_group':
            $query = GroupLesson::query()->where('individual', false);
            break;
        case 'individual':
            $query = GroupLesson::query()->where('individual', true);
            break;
        case 'webinar':
            $query = Webinar::query();
            break;
    }
    return $query;
  }

  public static function lessonTypeMapper()
  {
    return [
      'all' => 'Все занятия',
      'webinar' => 'Интенсивы',
      'in_group' => 'В группе',
      'individual' => 'Индивидуальные',
      // 'course' => 'Курс',
    ];
  }
}
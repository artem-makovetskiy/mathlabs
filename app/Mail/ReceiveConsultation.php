<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class ReceiveConsultation extends Mailable
{
    use Queueable, SerializesModels;

    public $name;
    public $phone_number;
    public $grade;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($name, $phone_number, $grade)
    {
        $this->name = $name;
        $this->phone_number = $phone_number;
        $this->grade = $grade === '-' ? '-' : $grade . ' класс';
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from(env('MAIL_FROM_ADDRESS'))->subject('Заявка на консультацию')
                    ->markdown('emails.site.receive-consultation')->with(['name' => $this->name, 'phone_number' => $this->phone_number, 'grade' => $this->grade]);
    }
}

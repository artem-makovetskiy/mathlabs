<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;
use Illuminate\Support\Str;

class Webinar extends Model
{
    protected $table = 'webinars';

    protected $fillable = [
        'title',
        'brief_desc',
        'full_desc',
        'price',
        'grade',
        'goal',
        'event_date',
        'published',
        'archived_at',
        'author_id'
    ];

    protected $casts = [
        'published' => 'boolean',
        'price' => 'integer',
//        'event_date' => 'datetime:d/m/y H:i',
    ];

    protected $dates = [
        'event_date',
    ];

    protected $appends = ['type_label', 'type', 'color', 'tags', 'upcoming_event_dates'];

    protected static function boot()
    {
        parent::boot();

        static::creating(function ($webinar) {
            $webinar->uuid = (string) Str::uuid();
        });
    }

    public function scopeNotArchived($query)
    {
        return $query->whereNull('archived_at');
    }

//    public function getIncrementing()
//    {
//        return false;
//    }
//
//    public function getKeyType()
//    {
//        return 'string';
//    }

    public function users()
    {
        return $this->belongsToMany('App\User', 'user_webinar', 'webinar_id', 'user_id');
    }

    public function payment_charges()
    {
        return $this->morphOne('App\PaymentCharge', 'lessonable');
    }

    public function meetings()
    {
        return $this->morphMany('App\Meeting', 'lesson');
    }

    public function setEventDateAttribute($value)
    {
        $this->attributes['event_date'] = Carbon::createFromFormat(self::getDateTimeFormat(), $value);
    }

    public function getEventDateAttribute($value)
    {
        return Carbon::parse($value)->format(self::getDateTimeFormat());
    }

    public static function getDateTimeFormat()
    {
        return 'd-m-Y H:i';
    }

    public function getTypeAttribute()
    {
        return 'webinar';
    }

    public function getTypeLabelAttribute() {
        return $this->typeLabelsMapper()[$this->type];
    }

    public function getColorAttribute() {
        return $this->colorsTypeMapper()[$this->type];
    }

    public function getUpcomingEventDatesAttribute()
    {
        return [$this->event_date];
    }

    // public function getClosestEventDate()
    // {
    //     return $this->event_date->next();
    // }

    public function colorsTypeMapper() {
        return [
            'webinar' => 'orange',
            'group' => 'purple'
        ];
    }

    public function typeLabelsMapper() {
        return [
            'webinar' => 'Интенсив',
            'group' => 'В группе'
        ];
    }

    public function getTagsAttribute()
    {
        return collect([
            'type' => $this->type_label,
            'grade' => $this->grade,
        ]);
    }

    public static function getGradeOptions() {
        return [
            '1 класс',
            '2 класс',
            '3 класс',
            '4 класс',
            '5 класс',
            '6 класс',
            '11 класс',
        ];
    }

    public static function getLabel()
    {
        return 'Интенсивы';
    }

    public function isArchived() : bool
    {
        return !is_null($this->archived_at);
    }

    public function markAsArchived()
    {
        $this->archived_at = now();
        $this->save();
        return $this;
    }
}

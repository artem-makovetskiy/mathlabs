<?php

namespace App\Http\Controllers;

use App\PaymentCharge;
use App\Services\Payment\TinkoffMerchantAPI;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;

class AccountController extends Controller
{
    public function paymentConfirmation(Request $request)
    {
        $data = $request->all();
        Log::debug('Payment confirmation notification', $data);

        $payment_charge = PaymentCharge::where('payment_id', $data['PaymentId'])->first();
        if ($data['Success'] === true && $data['Status'] === TinkoffMerchantAPI::STATUS_CONFIRMED) {
            $payment_charge->status = TinkoffMerchantAPI::STATUS_CONFIRMED;
            $payment_charge->save();
            $payment_charge->markAsVerified();

            $user = $payment_charge->user;
            $user->increment('available_lessons', $payment_charge->payment_plan->lessons_amount);
            $user->save();
            
            Log::debug('Payment confirmation CONFIRMED');
            
            return response('OK', 200)->header('Content-Type', 'text/plain');
        } else {
            if (in_array($data['Status'], TinkoffMerchantAPI::$confirm_statuses)) {
                $payment_charge->status = $data['Status'];
                $payment_charge->save();
                Log::debug('Payment confirmation Failed', ['status' => $data['Status']]);
                return response('OK', 200)->header('Content-Type', 'text/plain');
            }
        }
    }
}

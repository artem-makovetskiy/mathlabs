<?php

namespace App\Http\Livewire\Account;

use App\GroupLesson;
use App\Webinar;
use Livewire\Component;

class LessonDetails extends Component
{
    public $uuid;
    public $type;

    public function mount($type, $uuid)
    {
        $this->uuid = $uuid;
        $this->type = $type;
    }

    public function getLessonProperty()
    {
        switch ($this->type) {
            case 'group':
                $query = GroupLesson::query();
                break;
            case 'individual':
                $query = GroupLesson::query();
                break;
            case 'webinar':
                $query = Webinar::query();
                break;
        }
        return $query->whereUuid($this->uuid)->first();
    }

    public function getContainerClassProperty()
    {
        $classes = [
            'individual' => 'more-about--classes-in-group',
            'group' => 'more-about--classes-in-group',
            'webinar' => 'more-about--intensives'
        ];
        return $classes[$this->type];
    }

    public function getLabelProperty()
    {
        return $this->getLessonProperty()->type_label;
    }

    public function render()
    {
        return view('livewire.account.lesson-details');
    }
}

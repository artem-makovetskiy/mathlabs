<?php

namespace App\Http\Livewire\Account;

use App\GroupLesson;
use App\QuizStatus;
use App\Webinar;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\Auth;
use Livewire\Component;

class MyLessons extends Component
{
    public $quiz_status;
    // public $upcoming_event_dates = ["20-08-2020"];
    public $upcoming_event_dates;
    public $show_welcome_msg = false;
    public $items_per_page = 5;

    public function mount()
    {
        if (session()->has('show_welcome_msg')) {
            $this->show_welcome_msg = true;
        }
//        $this->quiz_status = auth()->user()->quiz_status;
        $this->quiz_status = Auth::user()->quiz_status;
        $this->upcoming_event_dates = $this->getUpcomingEventDatesList();
    }

    public function getMyLessonsProperty()
    {
        // return Webinar::query()->whereHas('users', function (Builder $query) {
        //     $query->where('id', Auth::user()->getKey());
        // })->orderByDesc('id')->paginate(5);
        $webinars = Webinar::query()->whereHas('users', function (Builder $query) {
            $query->where('users.id', Auth::user()->getKey());
        })->get();
        $group_lessons = GroupLesson::query()->whereHas('purchases', function (Builder $query) {
            $query->where('user_id', Auth::user()->getKey());
            // $query->whereNotNull('group_id');
        })->with(['meetings' => function ($query) {
            // $query->where('group_id', '');
        }, 'groups', 'purchases'])->get();
        // $group_lessons = $group_lessons->map(function($item) {
        //     // $group_id = $item->purchases->first()->group_id;
        //     return collect($item->toArray())->concat(['upcoming_event_dates' => $item->purchases->first()->group->meetings->first()->upcoming_event_dates]);
        // });
        // dd($group_lessons);
        // return collect([]);
        return $webinars->concat($group_lessons)->sortByDesc('created_at')->take($this->items_per_page);
    }

    public function getUpcomingEventDatesList()
    {
        return $this->mylessons->map(function($item) {
            if ($item->type === 'group') {
                $group = $item->purchases->first()->group;
                return !empty($group) ? $group->meetings->first()->upcoming_event_dates : '';
            } elseif ($item->type === 'webinar') {
                return $item->upcoming_event_dates;
            }
        })->flatten()->toArray();
        // dd($this->mylessons->pluck('groups.upcoming_event_dates')->all());
        // return $this->mylessons->pluck('groups.upcoming_event_dates')->flatten()->all();
    }

    public function closeWelcomeModal()
    {
        $this->show_welcome_msg = false;
        $this->emit('closeWelcomeModal');
    }

    public function render()
    {
        return view('livewire.account.my-lessons');
    }
}

<?php

namespace App\Http\Livewire\Account;

use App\GroupLesson;
use App\Webinar;
use Carbon\Carbon;
use Livewire\Component;

class WatchLesson extends Component
{
    public $uuid;
    public $type;

    public function mount($type, $uuid)
    {
        $this->uuid = $uuid;
        $this->type = $type;
    }

    public function getLessonProperty()
    {
        switch ($this->type) {
            case 'group':
                $query = GroupLesson::query();
                break;
            case 'individual':
                $query = GroupLesson::query();
                break;
            case 'webinar':
                $query = Webinar::query();
                break;
        }
        return $query->whereUuid($this->uuid)->first();
    }

    public function getJoinLinkProperty()
    {
        return $this->lesson->meetings()->count() > 0 ?
            route('account.streamLesson', [$this->lesson->type, $this->lesson->uuid, $this->lesson->meetings[0]['meeting_id']]) : null;
    }

    public function getMeetingUpcomingSoonProperty()
    {
        // return Carbon::parse($this->lesson->meetings[0]['next_meeting_time'])->diffInHours(now());
        // return now()->format('H:i');
        return true;
        // make link available only in one hour before stream
        // return now()->diffInMinutes(Carbon::parse($this->lesson->meetings[0]['next_meeting_time'])) < 60;
        //return now()->diffInMinutes(Carbon::parse('13.08.2020 8:50')) < 60;
    }

    public function getNextMeetingTimeProperty()
    {
        return $this->lesson->meetings[0]['next_meeting_time'];
    }

    public function render()
    {
        return view('livewire.account.watch-lesson');
    }
}

<?php

namespace App\Http\Livewire\Account;

use App\User;
use Illuminate\Support\Facades\Hash;
use Livewire\Component;

class Settings extends Component
{
    public $user;
    public $userData;

    // /** @var string */
    // public $currentPassword;
    /** @var string */
    public $oldPassword;
    /** @var string */
    public $password;
    

    public function mount()
    {
        $this->user = auth()->user();
        $this->userData = $this->user->toArray();
        // $this->currentPassword = $this->user->password;
        // dd($this->userData);
    }

    public function updateSettings()
    {
        // dd(Hash::check($this->oldPassword, $this->user->password));
        $this->validate([
            'userData.parent_name' => ['required'],
            'userData.child_name' => ['required', 'min:3'],
            'userData.email' => ['required', 'email', 'unique:users,email,'.$this->userData['id']],
            'userData.phone_number' => ['required', config('validation_rules.phone_number')],
            'userData.grade' => ['required'],
            // 'oldPassword' => 'required|min:4|same:currentPassword',
            'oldPassword' => [function ($attribute, $value, $fail) {
                                if (!empty($value) && !Hash::check($value, $this->user->password)) {
                                    $fail('Старый пароль не совпадает.');
                                // $fail($this->password);
                                } elseif (!empty($value) && empty($this->password)) {
                                    $fail('Введите новый пароль.');
                                }
                             }],
            'password' => 'nullable|min:4',
            'password' => [function ($attribute, $value, $fail) {
                                if (!empty($value) && empty($this->oldPassword)) {
                                    $fail('Введите старый пароль.');
                                }
                          }],
        ]);

        $message = 'Профиль успешно обновлен.';
        $this->user->fill($this->userData);
        if (!empty($this->password)) {
            $this->user->password = Hash::make($this->password);
            $message .= ' Пароль изменен.';
        }
        $this->user->save();

        session()->flash('message', $message);
    }

    public function getGradesProperty()
    {
        return User::getGradeList();
    }

    public function render()
    {
        return view('livewire.account.settings');
    }
}

<?php

namespace App\Http\Livewire\Account;

use App\PaymentCharge;
use App\PaymentPlan;
use App\Services\Payment\TinkoffMerchantAPI;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Livewire\Component;

class PayForClasses extends Component
{
//    public $user;
    public $payment_success = 0;
    public $payment_rejected = 0;
    public $payment_api_enabled = 1; // for turning on/off payment processing
    public $logging_enabled = 1;

    public function mount()
    {
//        $this->user = Auth::user();
    }

    public function getPaymentPlansProperty()
    {
        return PaymentPlan::query()->orderBy('id')->get();
    }

    public function getAuthUserProperty()
    {
        return Auth::user();
    }

    public function buyLessons($payment_plan_id)
    {
        $payment_plan = PaymentPlan::find($payment_plan_id);
        $user = Auth::user();

        if ($this->logging_enabled) {
            Log::debug('Payment start for user', $user->toArray());
        }

        $payment_charge = PaymentCharge::create([
            'charge_amount' => $payment_plan->full_price,
            // 'status' => TinkoffMerchantAPI::STATUS_NEW,
            'payment_plan_id' => $payment_plan->getKey(),
            'user_id' => $user->getKey(),
        ]);

        // $user->payment_charges()->save($payment_charge);
        // $payment_plan->payment_charges()->save($payment_charge);

        if ($this->payment_api_enabled) {
            $payment_conf = config('payment');
            $payment_api = new TinkoffMerchantAPI($payment_conf['prod_terminal']['terminal_key'], $payment_conf['prod_terminal']['secret_key']);
            $params = [
                'OrderId' => $payment_charge->id + 20, // Temporary hack, remove later
                'Amount'  => $payment_plan->full_price_in_cents,
                'DATA'    => [
                    'Email' => $user->email,
                    //'Phone' => $user->phone_number,
                ],
                'Receipt' => [
                    "Email" => $user->email,
                    //"Phone" => $user->phone_number,
                    "Taxation" => "usn_income_outcome",
                    "Items" => [[
                        "Name" => !empty($payment_plan->item_name) ? $payment_plan->item_name : "Доступ к занятиям математикой",
                        "Price" => $payment_plan->full_price_in_cents,
                        "Quantity" => 1,
                        "Amount" => $payment_plan->full_price_in_cents,
                        "Tax" => "none",
                    ]]
                ]
            ];

            if ($this->logging_enabled) {
                Log::debug('Amount type', ['type' => gettype($payment_plan->full_price_in_cents)]);
                Log::debug('Amount num', ['num' => $payment_plan->full_price_in_cents]);
                Log::debug('Payment params', $params);
            }

            $payment_api->init($params);

            if ($this->logging_enabled) {
                Log::debug('Payment result', ['response' => $payment_api->response]);
            }
            if ($payment_api->error) {
                if ($this->logging_enabled) {
                    Log::debug('Payment error', ['error' => $payment_api->error]);
                }
                if ($payment_api->status === TinkoffMerchantAPI::STATUS_REJECTED) {
                    $payment_charge->status = TinkoffMerchantAPI::STATUS_REJECTED;
                    $payment_charge->save();
                }
                $this->payment_rejected = 1;
            } else {
                if ($this->logging_enabled) {
                    Log::debug('Payment status', ['status' => $payment_api->status]);
                    Log::debug('Payment paymentUrl', ['paymentUrl' => $payment_api->paymentUrl]);
                    Log::debug('Payment paymentId', ['paymentId' => $payment_api->paymentId]);
                }
                if ($payment_api->status === TinkoffMerchantAPI::STATUS_NEW) {
                    $payment_charge->status = TinkoffMerchantAPI::STATUS_NEW;
                    $payment_charge->payment_id = $payment_api->paymentId;
                    $payment_charge->save();
                }
                return redirect()->away($payment_api->paymentUrl);
            }
        } else {
            $payment_charge->status = TinkoffMerchantAPI::STATUS_CONFIRMED;
            $payment_charge->markAsVerified();
            // $payment_charge->save();

            $user = $payment_charge->user;
            $user->increment('available_lessons', $payment_charge->payment_plan->lessons_amount);
            $user->save();
            
            $this->payment_success = 1;
        }

        // $user->increment('available_lessons', $payment_plan->lessons_amount);
        // $user->save();

        // $payment_charge->markAsVerified();

        // $this->payment_success = 1;
    }

    public function closeModal()
    {
        $this->payment_success = 0;
        $this->payment_rejected = 0;
    }

    public function render()
    {
        return view('livewire.account.pay-for-classes');
    }
}

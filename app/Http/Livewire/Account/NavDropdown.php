<?php

namespace App\Http\Livewire\Account;

use Illuminate\Support\Facades\Route;
use Livewire\Component;

class NavDropdown extends Component
{
    
    public function getActivePageNameProperty()
    {
        $current_route = Route::currentRouteName();
        $page_names = [
            'account.buyLessons' => 'Оплатить занятия',
            'account.allLessons' => 'Доступные занятия',
            'account.lessonDetails' => 'Доступные занятия',
            'account.myLessons' => 'Мои уроки',
            'account.watchLesson' => 'Мои уроки',
            'account.quiz' => 'Тестирование',
            'account.settings' => 'Настройки',
            'account.notifications' => 'Уведомления',
        ];
        return $page_names[$current_route];
    }

    public function render()
    {
        return view('livewire.account.nav-dropdown');
    }
}

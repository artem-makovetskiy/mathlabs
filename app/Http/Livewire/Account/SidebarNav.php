<?php

namespace App\Http\Livewire\Account;

use Livewire\Component;

class SidebarNav extends Component
{
    public $banner_type;
    public $active_page;

    public function mount($banner_type = null, $active_page = null)
    {
        $this->banner_type = $banner_type;
        $this->active_page = $active_page;
    }

    public function render()
    {
        return view('livewire.account.sidebar-nav');
    }
}

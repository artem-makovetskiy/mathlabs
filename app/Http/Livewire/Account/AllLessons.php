<?php

namespace App\Http\Livewire\Account;

use App\GroupLesson;
use App\GroupLessonPurchase;
use App\Helpers\Lesson;
use App\Webinar;
use App\Notifications\PaymentNeeded;
use App\User;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\Auth;
use Livewire\Component;

class AllLessons extends Component
{
    public $lessons_filter_type = 'all';
    public $filter_grade;
    public $payment_success = 0;
    public $payment_failure = 0;
    public $show_scheduled_time = 0;
    public $scheduled_time_defaults;
    public $scheduled_time;
    public $items_per_page = 5;
    public $selected_lesson_uuid;

    public function mount()
    {
        $this->scheduled_time_defaults = GroupLessonPurchase::getScheduledTimeDefaults();
        $this->scheduled_time = [
            'weekdays' => [],
            'time' => []
        ];
        $this->filter_grade = Auth::user()->grade;
    }

    public function getWebinarsProperty()
    {
        return Webinar::query()->whereDoesntHave('users', function (Builder $query) {
            $query->where('id', Auth::user()->getKey());
        })->orderByDesc('id')->paginate(5);
    }

    public function getLessonsProperty()
    {
        $lessons = collect([]);
        if ($this->lessons_filter_type === 'all') {
            $webinars = Webinar::query()->whereDoesntHave('users', function (Builder $query) {
                $query->where('users.id', Auth::user()->getKey());
            })->where('grade', $this->filter_grade . ' класс')->get();
            $group_lessons = GroupLesson::query()->whereDoesntHave('users', function (Builder $query) {
                $query->where('users.id', Auth::user()->getKey());
            })->where('grade', $this->filter_grade . ' класс')->get();
            $lessons = $webinars->concat($group_lessons);
        } else {
            $lesson_query = Lesson::getQueryByType($this->lessons_filter_type);
            $lessons = $lesson_query->whereDoesntHave('users', function (Builder $query) {
                $query->where('users.id', Auth::user()->getKey());
            })->where('grade', $this->filter_grade . ' класс')->get();
        }
        return $lessons->sortByDesc('created_at')->take($this->items_per_page);
        // return $webinars->concat($group_lessons)->sortByDesc('created_at')->take($this->items_per_page);
    }

    public function buyLesson($lesson_uuid, $type)
    {
        $user = Auth::user();
        // if ($user->available_lessons === 0) {
        //     $this->payment_failure = 1;
        //     return;
        // } elseif ($user->available_lessons < 2) {
        //     $user->notify(new PaymentNeeded());
        // }
        
        switch ($type) {
            case 'group':
                $lesson = GroupLesson::whereUuid($lesson_uuid)->first();
                $this->show_scheduled_time = 1;
                break;
            case 'individual':
                $lesson = GroupLesson::whereUuid($lesson_uuid)->first();
                $this->show_scheduled_time = 1;
                break;
            case 'webinar':
                $lesson = Webinar::whereUuid($lesson_uuid)->first();
                $lesson->users()->attach($user);
                // $user->decrement('available_lessons', 1);
                // $user->save();
                $this->payment_success = 1;
                break;
        }

        $this->selected_lesson_uuid = $lesson->uuid;

        // $user = Auth::user();
        // if ($user->available_lessons > 0) {
        //     $lesson->users()->attach($user);
        //     $user->decrement('available_lessons', 1);
        //     $user->save();
        //     $this->payment_success = 1;
        //     if ($user->available_lessons < 2) {
        //         $user->notify(new PaymentNeeded());
        //     }
        // } else {
        //     $this->payment_failure = 1;
        // }
    }

    public function addWeekday($weekday)
    {
        $weekdays = collect($this->scheduled_time['weekdays']);
        if ($weekdays->contains($weekday)) {
            $weekdays->forget($weekdays->search($weekday));
        } else {
            $weekdays->push($weekday);
        }
        $this->scheduled_time['weekdays'] = $weekdays->toArray();
    }

    public function addTime($time)
    {
        $times = collect($this->scheduled_time['time']);
        if ($times->contains($time)) {
            $times->forget($times->search($time));
        } else {
            $times->push($time);
        }
        $this->scheduled_time['time'] = $times->toArray();
    }

    public function saveScheduledTime()
    {
        $user = Auth::user();
        $lesson = GroupLesson::whereUuid($this->selected_lesson_uuid)->first();
        GroupLessonPurchase::create([
            'scheduled_time' => $this->scheduled_time,
            'group_lesson_id' => $lesson->getKey(),
            'user_id' => $user->getKey(),
        ]);
        // $user->decrement('available_lessons', 1);
        // $user->save();
        $this->closeModal();
    }

    public function getLessonTypesProperty()
    {
        return Lesson::lessonTypeMapper();
    }

    public function setLessonsFilter($lesson_name)
    {
        $this->lessons_filter_type = $lesson_name;
    }

    public function setGradeFilter($grade)
    {
        $this->filter_grade = $grade;
    }

    public function closeModal()
    {
        $this->payment_success = 0;
        $this->payment_failure = 0;
        $this->show_scheduled_time = 0;
        $this->resetModalsData();
    }

    // public function updatedShowScheduledTime($value)
    // {
    //     $this->resetModalsData();
    // }

    public function resetModalsData()
    {
        $this->scheduled_time = [
            'weekdays' => [],
            'time' => []
        ];
        $this->selected_lesson_uuid = null;
    }

    public function getGradesProperty()
    {
        return User::getGradeList();
    }

    public function render()
    {
        return view('livewire.account.all-lessons');
    }
}

<?php

namespace App\Http\Livewire\Account;

use App\Helpers\Lesson;
use App\Meeting;
use App\Notifications\LessonIsLive;
use App\Webinar;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Notification;
use Livewire\Component;

class StreamLesson extends Component
{
    public $uuid;
    public $lesson;
    public $meeting_number;
    public $lesson_type;
    public $password;
    public $role;
    public $user_name;
    public $leave_url;

    public function mount($type, $uuid, $meeting_number)
    {
        $this->uuid = $uuid;
        $this->lesson_type = $type;
        $this->meeting_number = $meeting_number;
        $meeting = Meeting::where('meeting_id', $meeting_number)->first();
        $this->lesson = Lesson::getByTypeAndUuid($type, $uuid);
        $this->password = $meeting->password;
        if (auth()->user()->hasRole('client')) {
            $this->user_name = auth()->user()->child_name;
            $this->role = config('zoom-api.roles.attendee');
            $this->leave_url = route('account.watchLesson', [$this->lesson_type, $this->uuid]);
        } elseif(auth()->user()->hasRole('admin')) {
            $this->user_name = 'Host';
            $this->role = config('zoom-api.roles.host');
            $this->leave_url = $type === 'group' ? route('admin.user-groups.edit', $meeting->group_id) : route('admin.webinars.schedule', $this->lesson->id);
            Notification::send($meeting->lesson->users, new LessonIsLive($meeting));
        }
    }

    public function getSignatureProperty()
    {
        $meet_config = config('zoom-api');
        $meet_config['meeting_number'] = $this->meeting_number;
        $meet_config['role'] = $this->role;

        $time = time() * 1000 - 30000;//time in milliseconds (or close enough)
        $data = base64_encode($meet_config['api_key'] . $meet_config['meeting_number'] . $time . $meet_config['role']);
        $hash = hash_hmac('sha256', $data, $meet_config['api_secret'], true);
        $_sig = $meet_config['api_key'] . "." . $meet_config['meeting_number'] . "." . $time . "." . $meet_config['role'] . "." . base64_encode($hash);

        //return signature, url safe base64 encoded
        return rtrim(strtr(base64_encode($_sig), '+/', '-_'), '=');
    }

    public function getLabelProperty()
    {
        return Webinar::getLabel();
    }

    public function render()
    {
        return view('livewire.account.stream-lesson');
    }
}

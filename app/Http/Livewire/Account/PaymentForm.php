<?php

namespace App\Http\Livewire\Account;

use Livewire\Component;

class PaymentForm extends Component
{
    public function render()
    {
        return view('livewire.account.payment-form');
    }
}

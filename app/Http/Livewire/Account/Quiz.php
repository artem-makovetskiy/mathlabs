<?php

namespace App\Http\Livewire\Account;

use App\Question;
use App\QuizStatus;
use Illuminate\Support\Facades\Auth;
use Livewire\Component;

class Quiz extends Component
{
    public $current_step = 1;
    public $questionsData;
    public $quiz_finished = false;
    public $quiz_status;
    public $right_answers = 0;
    public $wrong_answers = 0;
    public $selected_opts;

    public function mount()
    {   
        $this->questionsData = Question::query()->where('grade', Auth::user()->grade)->with(['type', 'options'])->get()->map(function ($item) {
            return [
                'id' => $item->id,
                'body' => $item->body,
                'type' => $item->type,
                'options' => $item->options,
                'original_options' => $item->options()->select('id')->where('is_solution', true)->get()->toArray(),
                'selected_options' => [],
                'has_multiple_options' => $item->options->count() > 1,
                'right_answer' => $item->options->count() === 1 ? $item->options[0]['name'] : '',
                'given_answer' => '',
                'is_active' => $item->id === $this->current_step,
                'is_solved' => false
            ];
        })->toArray();
        if ($this->hasquestions) {
            $this->currentQuestionData = $this->questionsData[$this->current_step - 1];
        }
        $this->quiz_status = Auth::user()->quiz_status;
    }

    public function getHasQuestionsProperty()
    {
        return collect($this->questionsData)->count() > 0;
    }

    public function getQuestionsProperty()
    {
        return Question::query()->with(['type', 'options'])->get()->map(function ($item) {
            return [
                'id' => $item->id,
                'body' => $item->body,
                'type' => $item->type,
                'options' => $item->options,
                'is_active' => $item->id === $this->current_step,
                'is_solved' => false
            ];
        });
    }

    public function getCurrentQuestionProperty()
    {
        return $this->questionsData[$this->current_step - 1];
    }

    public function changeStep($step)
    {
        $this->current_step = $step;
//        $this->questionsData[$this->current_step - 1]['given_answer'] = $this->currentQuestionData['given_answer'];
//        $this->currentQuestionData = $this->questionsData[$this->current_step - 1];
    }

    public function nextStep()
    {
        if ($this->current_step < count($this->questionsData)) {
            $this->current_step += 1;
//            $this->questionsData[$this->current_step - 1]['given_answer'] = $this->currentQuestionData['given_answer'];
//            $this->currentQuestionData = $this->questionsData[$this->current_step - 1];
        } else {
            $this->finishQuiz();
        }
    }

    public function finishQuiz()
    {
        $this->calculateResult();
        $this->quiz_finished = true;
    }

    public function skipQuiz()
    {
        $quiz_status = $this->quiz_status;
        $quiz_status->markAsSkipped();
        $quiz_status->save();
        return redirect()->route('account.myLessons');
    }

    public function calculateResult()
    {
        $this->right_answers = collect($this->questionsData)->reduce(function ($carry, $item) use (&$arr) {
            return $carry + ($item['is_solved'] ? 1 : 0);
        });
        $this->wrong_answers = count($this->questionsData) - $this->right_answers;
        $this->storeResult();
    }

    public function storeResult()
    {
        $quiz_status = $this->quiz_status;
        $quiz_status->right_answers = $this->right_answers;
        $quiz_status->wrong_answers = $this->wrong_answers;
        $quiz_status->current_step = $this->current_step;
        $quiz_status->result_grade = $this->calcGrade();
        $quiz_status->markAsPassed();
        $quiz_status->save();
    }

    public function calcGrade()
    {
        // TODO: add proper calculate grade logic
        return $this->wrong_answers < (count($this->questionsData) / 2) ? '11' : '8';
    }

    public function submitAnswer()
    {
//        $this->currentquestion['is_solved'] = $this->current_option['is_solution'];
        $curr_question = &$this->questionsData[$this->current_step - 1];
//        dd($curr_question);
        if ($curr_question['has_multiple_options']) {
//            $curr_question['is_solved'] = collect($curr_question['original_options'])->diff($curr_question['selected_options'])->count() === 0;
//            dd(collect($curr_question['original_options'])->diff($curr_question['selected_options']));
            //            dd($curr_question['is_solved']);
            $curr_question['is_solved'] = $curr_question['selected_options'] === $curr_question['original_options'];
        }
        $this->nextStep();
    }

    public function selectOption($option_id, $isChecked)
    {
        $sel_opts =  collect($this->questionsData[$this->current_step - 1]['selected_options']);

        // if (gettype($option_id) === 'string' || gettype($option_id) === 'integer') {
        if (!empty($option_id)) {
            if ($isChecked) {
                $sel_opts->push(['id' => $option_id]);
            } else {
                $sel_opts->pull($sel_opts->search(function ($item, $key) use ($option_id) {
                    return $item['id'] === $option_id;
                }));
            }
            $this->questionsData[$this->current_step - 1]['selected_options'] = $sel_opts->values()->toArray();
            $this->selected_opts = $this->questionsData[$this->current_step - 1]['selected_options'];
        }
    }

    public function compareOption($value)
    {
        $curr_question = &$this->questionsData[$this->current_step - 1];
        if ($curr_question['right_answer'] === $value) {
            $curr_question['is_solved'] = true;
        } else {
            $curr_question['is_solved'] = false;
        }
        $curr_question['given_answer'] = $value;
    }

    public function stepPillsClasses($question_index)
    {
        if ($this->quiz_finished) {
            return $this->questionsData[$question_index]['is_solved'] ? ' test-content__header-list-item--true' : ' test-content__header-list-item--false';
        } else {
            return ($question_index + 1) === $this->current_step ? ' test-content__header-list-item--active' : '';
        }
    }

    public function render()
    {
        return view('livewire.account.quiz');
    }
}

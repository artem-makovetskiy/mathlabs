<?php

namespace App\Http\Livewire\Admin\Webinars;

use App\User;
use App\Webinar;
use Illuminate\Support\Facades\Auth;
use Livewire\Component;

class Edit extends Component
{
    public $webinar_id;
    public $webinar;
    public $webinarData;

    public function mount($id)
    {
        $this->webinar_id = $id;
        $this->webinar = Webinar::findOrFail($id);
        $this->webinarData = $this->webinar->toArray();
//        $this->webinarData['author_id'] = Auth::user()->getKey();
    }

    public function updateWebinar()
    {
        $this->validate([
            'webinarData.title' => 'required|max:100',
            'webinarData.price' => 'required|numeric',
        ]);

        $this->webinar->fill($this->webinarData);
        $this->webinar->save();

        session()->flash('message', 'Интенсив успешно обновлен.');
    }

    public function destroyWebinar()
    {
        $webinar_name = $this->webinar->name;
        $this->webinar->delete();
        session()->flash('message', "Интенсив '$webinar_name' удален.");
        return redirect()->route('admin.webinars.index');
    }

    // public function getGradeOptionsProperty()
    // {
    //     return Webinar::getGradeOptions();
    // }

    public function getGradeOptionsProperty()
    {
        return User::getGradeList();
    }

    public function getLabelProperty()
    {
        return Webinar::getLabel();
    }

    public function render()
    {
        return view('livewire.admin.webinars.edit');
    }
}

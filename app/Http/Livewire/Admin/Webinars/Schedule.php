<?php

namespace App\Http\Livewire\Admin\Webinars;

use App\Meeting;
use App\Notifications\LessonStartSoon;
use App\Webinar;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Notification;
use Livewire\Component;

class Schedule extends Component
{
    public $topic;
    public $meeting_date;
    public $meeting_duration = 60;
    public $meeting_type = 2;
//    public $show_join_link = false;
    public $webinar_id;
    public $webinar;
    public $webinarData;
    public $meetings;

    public function mount($id)
    {
        $this->webinar_id = $id;
        $this->webinar = Webinar::findOrFail($id);
        $this->webinarData = $this->webinar->toArray();
        $this->meeting_date = now($this->getTimezone())->format($this->getMeetingDateFormatProperty());
//        $this->getUpcomingEvents();
        $this->getMeetings();
    }

    public function submitForm()
    {
        $this->validate([
            'topic' => 'required',
            'meeting_date' => 'required'
        ]);

        $this->createMeeting();

//        $this->reset();
    }

    public function getUpcomingEvents()
    {
        $users_resp = Http::withToken(config('zoom-api')['api_token'])->get('https://api.zoom.us/v2/users/');
//        dd($users->json());
        $user_id = $users_resp->json()['users'][0]['id'];
        $response = Http::withToken(config('zoom-api')['api_token'])->get(config('zoom-api')['api_base_url'] . "/users/$user_id/meetings");
//        dd($response->json());
        $this->meetings = collect($response->json()['meetings'])->map(function ($item) {
            return [
                'id' => $item['id'],
                'topic' => $item['topic'],
                'start_time' => date('d-m-Y H:i', strtotime($item['start_time']))
            ];
        })->toArray();
    }

    public function getMeetings()
    {
        $this->meetings = Meeting::whereHasMorph('lesson', ['App\Webinar'])->get()->toArray();
    }

    public function createMeeting()
    {
        $users_resp = Http::withToken(config('zoom-api')['api_token'])->get('https://api.zoom.us/v2/users/');
        $user_id = $users_resp->json()['users'][0]['id'];
        $response = Http::withToken(config('zoom-api')['api_token'])
                      ->post(config('zoom-api')['api_base_url'] . "/users/$user_id/meetings", [
                          'topic' => $this->topic,
                          'start_time' => \DateTime::createFromFormat($this->getMeetingDateFormatProperty(), $this->meeting_date, $this->getTimezone())->format('Y-m-d\TH:i:s'), // format: '2020-06-15T22:00:00Z'
                          'duration' => $this->meeting_duration,
                          'status' => 'waiting',
                          'timezone' => 'Europe/Kiev', // TODO: set proper Timezone later
                          'type' => $this->meeting_type,
                      ]);

        if ($response->failed()) {
            session()->flash('message', 'Что-то пошло не так.');
            return;
        }

        $meeting = $response->json();
//        dd($meeting);
//        $meeting_record = new Meeting([
//            'topic' => $meeting['topic'],
//            'meeting_id' => $meeting['id'],
//            'meeting_time' => date('d-m-Y H:i', strtotime($meeting['start_time'])),
//            'password' => $meeting['password'],
//        ]);
//        Auth::user()->meetings()->save($meeting_record);
//        $meeting_record->lesson()->save($this->webinar);
        $meeting = $this->webinar->meetings()->create([
            'topic' => $meeting['topic'],
            'meeting_id' => trim(preg_replace('/\s+/', '', $meeting['id'])),
            // TODO: fix hours format
            'meeting_time' => Carbon::parse($this->meeting_date, $this->getTimezone()),
            'password' => $meeting['password'],
            'user_id' => Auth::user()->id
        ]);

        if (Carbon::parse($meeting->meeting_time)->diffInDays(now()) <= 1) {
            Notification::send($meeting->lesson->users, new LessonStartSoon($meeting));
        }

//        $meeting_record->save();
//        Meeting::create([
//            'topic' => $meeting['topic'],
//            'meeting_id' => $meeting['id'],
//            'meeting_time' => date('d-m-Y H:i', strtotime($meeting['start_time'])),
//            'password' => $meeting['password'],
//            'user_id' => Auth::user()->id,
//        ]);
//        if ($response->successful()) {
//            session()->flash('message', 'Занятие запланировано.');
//        }
        session()->flash('message', 'Занятие запланировано.');
//        $this->getUpcomingEvents();
        $this->getMeetings();
    }

//    public function updatedMeetingDate()
//    {
//        dd(\DateTime::createFromFormat($this->getMeetingDateFormatProperty(), $this->meeting_date, new \DateTimeZone('GMT'))->format('Y-m-dTH:m:s'));
//    }

    public function getFormatDateProperty()
    {
        return $this->meeting_date ? \DateTime::createFromFormat($this->getMeetingDateFormatProperty(), $this->meeting_date, $this->getTimezone())->format('Y-m-d\TH:i:s') : '';
    }

    public function getLabelProperty()
    {
        return Webinar::getLabel();
    }

    public function getMeetingDateFormatProperty()
    {
        return "d-m-Y H:i";
    }

    public function getTimezone()
    {
        return new \DateTimeZone('Europe/Kiev');
    }

    public function render()
    {
        return view('livewire.admin.webinars.schedule');
    }
}

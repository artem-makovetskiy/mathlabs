<?php

namespace App\Http\Livewire\Admin\Webinars;

use App\User;
use App\Webinar;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Livewire\Component;

class Create extends Component
{
    public $title;
    public $brief_desc;
    public $price;
    public $grade;
    public $event_date;
    public $message;

    public function mount()
    {
        $this->event_date = now()->format('d-m-Y H:i');
    }

//    public function updatingEventDate($value)
//    {
//        dd(Carbon::parse($value)->format(Webinar::getDateTimeFormat()));
//        return date('d/m/y H:i', strtotime($value));
//    }

    public function submitForm()
    {
        $this->validate([
            'title' => 'required|max:100',
            'price' => 'required|numeric',
        ]);


        Webinar::create([
            'title' => $this->title,
            'price' =>  $this->price,
            'brief_desc' =>  $this->brief_desc,
            'grade' => $this->grade,
//            'event_date' => (new \DateTime($this->event_date))->format('d/m/y H:i'),
            'event_date' => $this->event_date,
            'author_id' => Auth::user()->getKey(),
            'published' => true,
        ]);

        $this->reset();

        session()->flash('message', 'Занятие создано.');
    }

    // public function getGradeOptionsProperty()
    // {
    //     return Webinar::getGradeOptions();
    // }

    public function getGradeOptionsProperty()
    {
        return User::getGradeList();
    }

    public function getLabelProperty()
    {
        return Webinar::getLabel();
    }

    public function render()
    {
        return view('livewire.admin.webinars.create');
    }
}

<?php

namespace App\Http\Livewire\Admin\Webinars;

use App\Meeting;
use App\Webinar;
use Livewire\Component;

class Stream extends Component
{
    public $meeting_number;
    public $password;
    public $role;

    public function mount($meeting_number)
    {
        $this->meeting_number = $meeting_number;
        $meeting = Meeting::where('meeting_id', $meeting_number)->first();
        $this->password = $meeting->password;
        if (auth()->user()->hasRole('client')) {
            $this->role = config('zoom-api.roles.attendee');
        } else {
            $this->role = config('zoom-api.roles.host');
        }
    }

    public function getSignatureProperty()
    {
        $meet_config = config('zoom-api');
        $meet_config['meeting_number'] = $this->meeting_number;
        $meet_config['role'] = $this->role;
//        $meet_config = [
//            'api_key' => 'kjQIGLUtQ5u5x941S3VIBQ',
//            'api_secret' => 'rktQB2zxUtNPFbhKOYswhLaAbnsOOkGuwDZd',
//            'meeting_number' => $this->meeting_number,
//            'role' => 1,
//        ];

        $time = time() * 1000 - 30000;//time in milliseconds (or close enough)
        $data = base64_encode($meet_config['api_key'] . $meet_config['meeting_number'] . $time . $meet_config['role']);
        $hash = hash_hmac('sha256', $data, $meet_config['api_secret'], true);
        $_sig = $meet_config['api_key'] . "." . $meet_config['meeting_number'] . "." . $time . "." . $meet_config['role'] . "." . base64_encode($hash);

        //return signature, url safe base64 encoded
        return rtrim(strtr(base64_encode($_sig), '+/', '-_'), '=');
    }

    public function getLabelProperty()
    {
        return Webinar::getLabel();
    }

    public function render()
    {
        return view('livewire.admin.webinars.stream');
    }
}

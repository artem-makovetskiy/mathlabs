<?php

namespace App\Http\Livewire\Admin\Webinars;

use App\Webinar;
use Livewire\Component;

class Index extends Component
{
    public function getLabelProperty()
    {
        return Webinar::getLabel();
    }

    public function getWebinarsProperty()
    {
        return Webinar::query()->orderByDesc('id')->paginate(5);
    }

    public function paginationView()
    {
        return 'livewire.components.pagination-links';
    }

    public function render()
    {
        return view('livewire.admin.webinars.index');
    }
}

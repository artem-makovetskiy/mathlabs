<?php

namespace App\Http\Livewire\Admin\Questions;

use App\Question;
use App\QuestionOption;
use App\QuestionType;
use App\User;
use Livewire\Component;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\DB;

class Edit extends Component
{
    public $question_id;
    public $question;
    public $questionData;
    public $question_options = [];
    public $option_single = ['name' => ''];

    public function mount($id)
    {
        $this->question_id = $id;
        $this->question = Question::with('type')->findOrFail($id);
        $this->questionData = $this->question->toArray();
        $this->questionData['type_selected'] = $this->question->type->id;
        if (!$this->question->has_options && $this->question->options()->count() === 1) {
            $this->option_single = $this->question->options[0]->toArray();
        } else {
            $this->question_options = $this->question->options()->count() > 0 ? $this->question->options->toArray() : [];
        }
    }

    public function updateQuestion()
    {
        $this->validate([
            'questionData.title' => 'required|max:100',
            'questionData.body' => 'required',
            'questionData.type' => 'required',
            'questionData.grade' => 'required'
        ], [
            'required' => 'Поле :attribute обязательно к заполнению.'
        ], [
            'questionData.title' => 'Название',
            'questionData.body' => 'Описание',
            'questionData.type' => 'Тип',
            'questionData.grade' => 'Класс'
        ]);

        $this->question->fill($this->questionData);
        $this->question->save();

        $new_type = QuestionType::find($this->questionData['type_selected']);
        $new_type->questions()->save($this->question);

        // TODO: fix type changing
        // $this->question->type()->attach(QuestionType::find($this->questionData['type_selected']));

        // TODO: handle option deletion if needed

        if ($this->questionData['type_selected'] != $this->question->type->id) {
            QuestionOption::where('question_id',  $this->question->id)->delete();
        }

        // $options = QuestionOption::query()->whereHas('question', function (Builder $query) {
        //     $query->where('question_type_id', '!=', $this->questionData['type_selected'])->where('id',  $this->question->id);
        // })->delete();

        // $options = DB::table('question_options')
        //     ->leftJoin('questions', 'question_options.question_id', '=', 'questions.id')
        //     ->where('question_options.question_id',  $this->question->id)
        //     ->where('questions.question_type_id', '<>', $this->questionData['type_selected'])
        //     ->get();

        if ($this->questionData['type_selected'] == 1) {
            if (!empty($this->option_single['created_at'])) {
                $option = QuestionOption::find($this->option_single['id']);
                $option->fill($this->option_single);
                $option->save();
            } else {
                $this->question->options()->create([
                    'name' => $this->option_single['name'],
                    'alias' => 'opt-1',
                    'is_solution' => true,
                ]);
            }
        } else if (in_array($this->questionData['type_selected'], [2,3]) && count($this->question_options) > 0) {
            collect($this->question_options)->each(function ($item, $index) {
                if (!empty($item['created_at'])) {
                    $option = QuestionOption::find($item['id']);
                    $option->fill($item);
                    $option->save();
                } else {
                    $this->question->options()->create([
                        'name' => $item['name'],
                        'alias' => 'opt-' . ($index + 1),
                        'is_solution' => $item['is_solution'],
                    ]);
                }
            });
        }

        session()->flash('message', 'Вопрос успешно обновлен.');
    }

    public function addOption()
    {
        $this->question_options = collect($this->question_options)->push([
            'name' => '',
            'is_solution' => false,
        ])->toArray();
    }

    public function destroyQuestion()
    {
        $question_name = $this->question->title;
        $this->question->delete();
        session()->flash('message', "Вопрос '$question_name' удален.");
        return redirect()->route('admin.questions.index');
    }

    public function getLabelProperty()
    {
        return Question::getLabel();
    }

    public function getTypesProperty()
    {
        return Question::getTypeOptions();
    }

    public function getGradesProperty()
    {
        return User::getGradeList();
    }

    public function render()
    {
        return view('livewire.admin.questions.edit');
    }
}

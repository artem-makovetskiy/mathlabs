<?php

namespace App\Http\Livewire\Admin\Questions;

use App\Question;
use Livewire\Component;

class Index extends Component
{
    public function getLabelProperty()
    {
        return Question::getLabel();
    }

    public function getQuestionsProperty()
    {
        return Question::query()->orderByDesc('id')->paginate(5);
    }

    public function render()
    {
        return view('livewire.admin.questions.index');
    }
}

<?php

namespace App\Http\Livewire\Admin\Questions;

use App\Question;
use App\User;
use Livewire\Component;
use Illuminate\Support\Str;

use function PHPSTORM_META\type;

class Create extends Component
{
    public $title;
    public $body;
    public $type;
    public $grade;
    public $question_options = [];
    public $option_single = ['name' => ''];

    public function mount()
    {
        
    }

    public function submitForm()
    {
        $this->validate([
            'title' => 'required|max:100',
            'body' => 'required',
            'type' => 'required',
            'grade' => 'required'
        ]);

        $question = Question::create([
            'title' => $this->title,
            'body' =>  $this->body,
            'grade' => $this->grade,
            'question_type_id' => $this->type,
            'sort_order' => Question::getMaxOrderNum() + 1,
        ]);

        if ($this->type == 1) {
            $question->options()->create([
                'name' => $this->option_single['name'],
                'alias' => 'opt-1',
                'is_solution' => true,
            ]);
        } else if (in_array($this->type, [2,3]) && count($this->question_options) > 0) {
            $question->options()->createMany(collect($this->question_options)->map(function($item, $index) {
                return [
                    'name' => $item['name'],
                    'alias' => 'opt-' . ($index + 1),
                    'is_solution' => $item['is_solution'],
                ];
            })->toArray());
        }

        $this->reset();

        session()->flash('message', 'Вопрос создан.');
    }

    public function addOption()
    {
        $this->question_options = collect($this->question_options)->push([
            'name' => '',
            'is_solution' => false,
        ])->toArray();
    }

    public function getLabelProperty()
    {
        return Question::getLabel();
    }

    public function getTypesProperty()
    {
        return Question::getTypeOptions();
    }

    public function getGradesProperty()
    {
        return User::getGradeList();
    }

    public function render()
    {
        return view('livewire.admin.questions.create');
    }
}

<?php

namespace App\Http\Livewire\Admin;

use Livewire\Component;

class SidebarNav extends Component
{
    public $active_page;

    public function mount($active_page = null)
    {
        $this->active_page = $active_page;
    }
    
    public function render()
    {
        return view('livewire.admin.sidebar-nav');
    }
}

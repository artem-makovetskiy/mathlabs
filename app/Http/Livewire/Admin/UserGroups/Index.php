<?php

namespace App\Http\Livewire\Admin\UserGroups;

use App\Group;
use Livewire\Component;

class Index extends Component
{
    public function getGroupsProperty()
    {
        return Group::query()->orderByDesc('created_at')->paginate(10);
    }

    public function render()
    {
        return view('livewire.admin.user-groups.index');
    }
}

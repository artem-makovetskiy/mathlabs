<?php

namespace App\Http\Livewire\Admin\UserGroups;

use App\Group;
use App\GroupLesson;
use App\GroupLessonPurchase;
use App\Meeting;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Http;
use App\Notifications\LessonTimeApproved;
use Illuminate\Support\Facades\Notification;
use Livewire\Component;

class Edit extends Component
{
    public $group_id;
    public $group;
    public $groupData;
    public $group_members = [];
    public $lesson;
    public $lessonData = ['id' => 0, 'title' => '-'];
    public $lessons_poll = [];
    public $meeting;
    public $new_members_poll = [];
    public $weekdays_options = [];
    public $num_of_lessons_opts;
    public $settings = ['weekday' => '', 'lesson_time' => '', 'end_times' => 1, 'schedule_meeting' => false];
    public $meeting_options = [
        'type' => 8,
        'duration' => 60,
        'start_time' => '',
        'status' => 'waiting',
        'timezone' => 'Europe/Kiev',
        'recurrence' => [
            'type' => 2, // Weekly
            'repeat_interval' => 1,
            'weekly_days' => '', // Day of the week
            'timezone' => 'Europe/Kiev',
            'end_times' => 1
        ]
    ];

    public function mount($id)
    {
        $this->group_id = $id;
        $this->group = Group::findOrFail($id);
        $this->groupData = $this->group->toArray();
        $this->lesson = GroupLesson::find($this->groupData['group_lesson_id']);
        $this->lessonData = $this->lesson->toArray();
        $this->lessons_poll = GroupLesson::all()->toArray();
        $this->new_members_poll = GroupLessonPurchase::where('group_lesson_id', $this->lessonData['id'])->where(function($query) {
                $query->orWhereNull('group_id')
                      ->orWhere('group_id', $this->groupData['id']);
            })->with('user', 'lesson')->get()->toArray();
        $this->group_members = $this->group->users()->get()->map(function($item) {
            $user_purchase = $item->group_lesson_purchases()->where('group_lesson_id', $this->lessonData['id'])->first();
            return collect($item->toArray())->merge([
                'purchase_id' => $user_purchase->id,
                'lesson' => $this->lessonData,
                'scheduled_time' => $user_purchase->scheduled_time,
            ])->toArray();
        })->toArray();
        $this->weekdays_options = GroupLessonPurchase::getScheduledTimeDefaults()['weekdays'];
        $this->num_of_lessons_opts = Meeting::getNumOfLessonsOptions();
        $this->settings['weekday'] = $this->groupData['lesson_weekday'];
        $this->settings['lesson_time'] = $this->groupData['lesson_time'];
        if ($this->groupData['schedule_meeting']) {
            $this->settings['end_times'] = $this->group->meetings->first()->recurring_end_times;
        }
        $this->getMeetings();
    }

    public function updateGroup()
    {
        $this->validate([
            'settings.lesson_time' => 'required',
            'settings.weekday' => 'required'
        ]);

        if ($this->membersValid()) {
            $member_ids = collect($this->group_members)->pluck('id')->values()->toArray();
            GroupLessonPurchase::whereIn('id', $member_ids)->update(['group_id' => $this->group->id]);

            $this->group->users()->sync(collect($this->group_members)->pluck('id')->toArray());

            $this->group->lesson_weekday = $this->settings['weekday'];
            $this->group->lesson_time = $this->settings['lesson_time'];
            $this->group->schedule_meeting = $this->settings['schedule_meeting'];
            $this->group->save();

            if ($this->settings['schedule_meeting']) {
                $meeting = $this->scheduleMeeting($this->group);
                Notification::send($this->group->users, new LessonTimeApproved($meeting));
            }

            session()->flash('message', 'Группа успешно обновлена.');
        }
    }

    public function scheduleMeeting(Group $group)
    {
        $users_resp = Http::withToken(config('zoom-api')['api_token'])->get('https://api.zoom.us/v2/users/');
        $user_id = $users_resp->json()['users'][0]['id'];

        // if (collect($this->meetings)->count() > 0) {
        if ($this->meeting) {
            // $meeting = $this->meetings[0];
            Meeting::find($this->meeting['id'])->delete();
            Http::withToken(config('zoom-api')['api_token'])
                      ->delete(config('zoom-api')['api_base_url'] . "meetings/".$this->meeting['meeting_id']);
        }

        $this->meeting_options['start_time'] = now($this->getTimezone())->setTimeFromTimeString($this->settings['lesson_time'])->format('Y-m-d\TH:i:s');
        $this->meeting_options['recurrence']['weekly_days'] = $this->formatWeekdaysForApi($this->settings['weekday']);
        $this->meeting_options['recurrence']['end_times'] = $this->settings['end_times'];

        $response = Http::withToken(config('zoom-api')['api_token'])
                      ->post(config('zoom-api')['api_base_url'] . "users/$user_id/meetings", $this->meeting_options);

        if ($response->failed()) {
            session()->flash('message', 'Что-то пошло не так. Трансляция не запланирована.');
            return;
        }

        $meeting = $response->json();

        $lesson = GroupLesson::find($this->lessonData['id']);

        $meeting = $lesson->meetings()->create([
            'topic' => $meeting['topic'],
            'meeting_id' => trim(preg_replace('/\s+/', '', $meeting['id'])),
            'meeting_time' => now($this->getTimezone())->setTimeFromTimeString($this->settings['lesson_time']),
            'recurring_weekday' => $this->settings['weekday'],
            'recurring_end_times' => $this->settings['end_times'],
            'password' => $meeting['password'],
            'user_id' => Auth::user()->id,
            'group_id' => $group->id
        ]);
        return $meeting;
    }

    public function changeLesson($lesson_id)
    {
        $items_collection = collect($this->lessons_poll);
        if (($found_key = $items_collection->search(function($item) use ($lesson_id) {
            return $item['id'] === $lesson_id;
        })) !== false) {
            $this->lessonData = $items_collection->get($found_key);
        }
        $this->updateMembersPoll();
        $this->group_members = [];
    }

    public function updateMembersPoll()
    {
        $this->new_members_poll = GroupLessonPurchase::where('group_lesson_id', $this->lessonData['id'])->with('user', 'lesson')->get()->toArray();
    }

    public function formatDateForApi($date)
    {
        return \DateTime::createFromFormat($this->getMeetingDateFormatProperty(), $date, $this->getTimezone())->format('Y-m-d\TH:i:s');
    }

    public function formatWeekdaysForApi($weekday)
    {
        $weekdaysMapper = [
            'Вс' => '1',
            'Пн' => '2', 
            'Вт' => '3', 
            'Ср' => '4', 
            'Чт' => '5', 
            'Пт' => '6', 
            'Сб' => '7',
        ];
        return $weekdaysMapper[$weekday];
    }

    public function addNewMember($member_id, $member_key)
    {
        $items_collection = collect($this->group_members);
        if (($found_key = $items_collection->search(function($item) use ($member_id) {
            return $item['purchase_id'] === $member_id;
        })) === false) {
            $items_collection->push(collect($this->new_members_poll[$member_key])->merge([
                'id' => $this->new_members_poll[$member_key]['user']['id'],
                'child_name' => $this->new_members_poll[$member_key]['user']['child_name'],
                'purchase_id' => $member_id,
            ])->toArray());
            // $items_collection->push($this->new_members_poll[$member_key]);
        }
        $this->group_members = $items_collection->toArray();
    }

    public function removeItem($item_id)
    {
        $items_collection = collect($this->group_members);
        if (($found_key = $items_collection->search(function($item) use ($item_id) {
            return $item['id'] === $item_id;
        })) !== false) {
            $items_collection->forget($found_key);
        }
        $this->group_members = $items_collection->toArray();
    }

    public function getMeetings()
    {
        $meeting = Meeting::whereHasMorph('lesson', ['App\GroupLesson'], function(Builder $query) { return $query->where('id', $this->lessonData['id']); })->where('group_id', $this->group->id)->first();
        if ($meeting) {
            $this->meeting = $meeting->toArray();
            $meeting_details = $this->getMeetingDetails($this->meeting['meeting_id']);
            // dd($meeting_details);
            $this->meeting['end_times_left'] = count($meeting_details['occurrences']);
        }
    }

    public function getMeetingDetails($meeting_id)
    {
        return Http::withToken(config('zoom-api')['api_token'])
                      ->get(config('zoom-api')['api_base_url'] . "meetings/".$meeting_id)->json();
    }

    public function membersHasSameLesson()
    {
        $items_collection = collect($this->group_members);
        return $items_collection->pluck('lesson.id')->unique()->values()->count() === 1;
    }

    public function membersValid()
    {
        $items_collection = collect($this->group_members);
        return $items_collection->count() > 0 && $this->membersHasSameLesson();
    }

    public function getMeetingDateFormatProperty()
    {
        return "d-m-Y H:i";
    }

    public function getTimezone()
    {
        return new \DateTimeZone('Europe/Kiev'); // old: 'UTC'
    }
    
    public function render()
    {
        return view('livewire.admin.user-groups.edit');
    }
}

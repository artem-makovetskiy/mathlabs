<?php

namespace App\Http\Livewire\Admin\UserGroups;

use App\Group;
use App\GroupLesson;
use App\GroupLessonPurchase;
use App\Meeting;
use App\Notifications\LessonTimeApproved;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Notification;
use Illuminate\Support\Facades\Auth;
use Livewire\Component;

class Create extends Component
{
    public $group_members = [];
    public $new_members_poll = [];
    public $lessons_poll = [];
    // public $lesson;
    public $lessonData = ['id' => 0, 'title' => '-'];
    // public $selected_lesson = ['title' => '-'];
    public $weekdays_options = [];
    public $settings = ['weekday' => '', 'lesson_time' => '', 'end_times' => 1, 'schedule_meeting' => false];
    public $num_of_lessons_opts;
    public $meeting_options = [
        'type' => 8,
        'duration' => 60,
        'start_time' => '',
        'status' => 'waiting',
        'timezone' => 'Europe/Kiev',
        'recurrence' => [
            'type' => 2, // Weekly
            'repeat_interval' => 1,
            'weekly_days' => '', // Day of the week
            'timezone' => 'Europe/Kiev',
            'end_times' => 1
        ]
    ];

    public function mount()
    {
        // session()->forget('new_group');
        // dd(Carbon::parse(Meeting::find(2)->next_meeting_time)->diffForHumans());
        if (session()->has('new_group')) {
            $this->group_members = GroupLessonPurchase::with('user', 'lesson')->whereIn('id', session('new_group'))->get()->toArray();
            $this->lessonData = $this->group_members[0]['lesson'];
            // $this->lesson = GroupLesson::find($this->lessonData['id']);
            // $this->new_members_poll = GroupLessonPurchase::where('group_lesson_id', $this->lessonData['id'])->with('user', 'lesson')->get()->toArray();
            $this->updateMembersPoll();
            if (!$this->membersHasSameLesson()) {
                session()->flash('warning_message', 'Участники имеют разные занятия.');
            }
        }
        $this->lessons_poll = GroupLesson::all()->toArray();
        $this->weekdays_options = GroupLessonPurchase::getScheduledTimeDefaults()['weekdays'];
        $this->num_of_lessons_opts = Meeting::getNumOfLessonsOptions();
    }

    // public function getLessonsPollProperty()
    // {
    //     return GroupLesson::all();
    // }

    public function changeLesson($lesson_id)
    {
        // dd($lesson_id);
        $items_collection = collect($this->lessons_poll);
        if (($found_key = $items_collection->search(function($item) use ($lesson_id) {
            return $item['id'] === $lesson_id;
        })) !== false) {
            $this->lessonData = $items_collection->get($found_key);
            // dd($this->lessonData);
        }
        $this->updateMembersPoll();
        $this->group_members = [];
    }

    public function updateMembersPoll()
    {
        $this->new_members_poll = GroupLessonPurchase::where('group_lesson_id', $this->lessonData['id'])->with('user', 'lesson')->get()->toArray();
        // dd(collect($this->new_members_poll)->pluck('user')->toArray());
    }

    public function submitForm()
    {
        $this->validate([
            'settings.lesson_time' => 'required',
            'settings.weekday' => 'required'
        ]);
        
        if ($this->membersValid()) {
            $group = Group::create([
                'lesson_time' => $this->settings['lesson_time'],
                'lesson_weekday' =>  $this->settings['weekday'],
                'active' =>  true,
                'schedule_meeting' => $this->settings['schedule_meeting'],
                'group_lesson_id' => $this->lessonData['id'],
            ]);

            $member_ids = collect($this->group_members)->pluck('id')->values()->toArray();
            GroupLessonPurchase::whereIn('id', $member_ids)->update(['group_id' => $group->id]);

            $group->users()->attach(collect($this->group_members)->pluck('user_id')->toArray());

            if ($this->settings['schedule_meeting']) {
                $meeting = $this->scheduleMeeting($group);
                Notification::send($group->users, new LessonTimeApproved($meeting));
            }

            // $this->reset();

            session()->flash('message', 'Группа создана.');
            return redirect()->route('admin.user-groups.edit', $group->id);
        }
    }

    public function scheduleMeeting(Group $group)
    {
        $users_resp = Http::withToken(config('zoom-api')['api_token'])->get('https://api.zoom.us/v2/users/');
        $user_id = $users_resp->json()['users'][0]['id'];

        $this->meeting_options['start_time'] = now($this->getTimezone())->setTimeFromTimeString($this->settings['lesson_time'])->format('Y-m-d\TH:i:s');
        $this->meeting_options['recurrence']['weekly_days'] = $this->formatWeekdaysForApi($this->settings['weekday']);
        $this->meeting_options['recurrence']['end_times'] = $this->settings['end_times'];

        $response = Http::withToken(config('zoom-api')['api_token'])
                      ->post(config('zoom-api')['api_base_url'] . "/users/$user_id/meetings", $this->meeting_options);

        if ($response->failed()) {
            session()->flash('message', 'Что-то пошло не так. Трансляция не запланирована.');
            return;
        }

        $meeting = $response->json();

        $lesson = GroupLesson::find($this->lessonData['id']);

        $meeting = $lesson->meetings()->create([
            'topic' => $meeting['topic'],
            'meeting_id' => trim(preg_replace('/\s+/', '', $meeting['id'])),
            'meeting_time' => now($this->getTimezone())->setTimeFromTimeString($this->settings['lesson_time']),
            'recurring_weekday' => $this->settings['weekday'],
            'recurring_end_times' => $this->settings['end_times'],
            'password' => $meeting['password'],
            'user_id' => Auth::user()->id,
            'group_id' => $group->id
        ]);
        return $meeting;
    }

    public function formatDateForApi($date)
    {
        return \DateTime::createFromFormat($this->getMeetingDateFormatProperty(), $date, $this->getTimezone())->format('Y-m-d\TH:i:s');
    }

    public function formatWeekdaysForApi($weekday)
    {
        $weekdaysMapper = [
            'Вс' => '1',
            'Пн' => '2', 
            'Вт' => '3', 
            'Ср' => '4', 
            'Чт' => '5', 
            'Пт' => '6', 
            'Сб' => '7',
        ];
        return $weekdaysMapper[$weekday];
    }

    public function membersHasSameLesson()
    {
        $items_collection = collect($this->group_members);
        return $items_collection->pluck('lesson.id')->unique()->values()->count() === 1;
        // $lesson_ids = $items_collection->pluck('lesson.id');
        // dd($lesson_ids);
    }

    public function membersValid()
    {
        $items_collection = collect($this->group_members);
        if ($this->lessonData['type'] === 'individual' && $items_collection->count() > 1) {
            session()->flash('warning_message', 'При индивидуальном занятии может быть только один участник.');
            return false;
        }
        return $items_collection->count() > 0 && $this->membersHasSameLesson();
    }

    public function removeItem($item_id)
    {
        $items_collection = collect($this->group_members);
        if (($found_key = $items_collection->search(function($item) use ($item_id) {
            return $item['id'] === $item_id;
        })) !== false) {
            $items_collection->forget($found_key);
        }
        $this->group_members = $items_collection->toArray();
    }

    public function addNewMember($member_id, $member_key)
    {
        // var_dump($member_id);
        // var_dump($member_key);
        // dd(array_key_exists($member_key, $this->new_members_poll));
        $items_collection = collect($this->group_members);
        if (($found_key = $items_collection->search(function($item) use ($member_id) {
            return $item['id'] === $member_id;
        })) === false) {
            $items_collection->push($this->new_members_poll[$member_key]);
            // var_dump('added item', $this->new_members_poll[$member_key]);
        }
        $this->group_members = $items_collection->toArray();
    }

    public function getMeetingDateFormatProperty()
    {
        return "d-m-Y H:i";
    }

    public function getTimezone()
    {
        return new \DateTimeZone('Europe/Kiev'); // old: 'UTC'
    }

    public function render()
    {
        return view('livewire.admin.user-groups.create');
    }
}

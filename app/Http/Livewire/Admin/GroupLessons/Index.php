<?php

namespace App\Http\Livewire\Admin\GroupLessons;

use App\GroupLesson;
use Livewire\Component;

class Index extends Component
{
    public function getGroupLessonsProperty()
    {
        return GroupLesson::query()->orderByDesc('id')->paginate(5);
    }

    public function paginationView()
    {
        return 'livewire.components.pagination-links';
    }
    
    public function render()
    {
        return view('livewire.admin.group-lessons.index');
    }
}

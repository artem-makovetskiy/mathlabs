<?php

namespace App\Http\Livewire\Admin\GroupLessons;

use App\GroupLesson;
use App\GroupLessonPurchase;
use App\Meeting;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Notification;
use Illuminate\Support\Facades\Auth;
use App\Notifications\LessonStartSoon;
use Illuminate\Database\Eloquent\Builder;
use Livewire\Component;

class Schedule extends Component
{
    public $topic;
    public $meeting_date_start;
    public $meeting_date_end;
    public $meeting_duration = 60;
    public $meeting_type = 8; // recurring meeting
    public $recurrence_options = [
        'type' => 2, // Weekly
        'repeat_interval' => 1,
        'weekly_days' => '', // Day of the week
        'timezone' => 'Europe/Kiev',
        'end_times' => 1
        // 'end_day_time' => '' // Final date
    ];
    public $weekdays_options = [];
    public $weekday;
    public $lesson_id;
    public $lesson;
    public $lessonData;
    public $meetings;
    public $num_of_lessons_opts;

    public function mount($id)
    {
        $this->lesson_id = $id;
        $this->lesson = GroupLesson::findOrFail($id);
        $this->lessonData = $this->lesson->toArray();
        $this->meeting_date_start = now($this->getTimezone())->format($this->getMeetingDateFormatProperty());
        $this->meeting_date_end = now($this->getTimezone())->format($this->getMeetingDateFormatProperty());
        $this->weekdays_options = GroupLessonPurchase::getScheduledTimeDefaults()['weekdays'];
        $this->num_of_lessons_opts = Meeting::getNumOfLessonsOptions();
        $this->getMeetings();
    }

    public function submitForm()
    {
        $this->validate([
            'topic' => 'required',
            'meeting_date_start' => 'required',
            'meeting_date_end' => 'required'
        ]);

        $this->createMeeting();
    }

    public function getMeetings()
    {
        $this->meetings = Meeting::whereHasMorph('lesson', ['App\GroupLesson'], function(Builder $query) { return $query->where('id', $this->lesson->id); })->get()->toArray();
    }

    public function createMeeting()
    {
        $users_resp = Http::withToken(config('zoom-api')['api_token'])->get('https://api.zoom.us/v2/users/');
        $user_id = $users_resp->json()['users'][0]['id'];
        // $this->recurrence_options['end_day_time'] = \DateTime::createFromFormat($this->getMeetingDateFormatProperty(), $this->meeting_date_end, $this->getTimezone())->format('Y-m-d\TH:i:s\Z');
        $this->recurrence_options['weekly_days'] = $this->formatWeekdaysForApi($this->weekday);
        // dd($this->recurrence_options);
        $response = Http::withToken(config('zoom-api')['api_token'])
                      ->post(config('zoom-api')['api_base_url'] . "/users/$user_id/meetings", [
                          'topic' => $this->topic,
                          'start_time' => $this->formatDateForApi($this->meeting_date_start), // format: '2020-06-15T22:00:00Z'
                          'duration' => $this->meeting_duration,
                          'status' => 'waiting',
                          'timezone' => 'Europe/Kiev', // TODO: set proper Timezone later, old: 'Europe/Kiev' 'UTC'
                          'type' => $this->meeting_type,
                          'recurrence' => $this->recurrence_options
                      ]);

        if ($response->failed()) {
            session()->flash('message', 'Что-то пошло не так.');
            return;
        }

        $meeting = $response->json();

        $meeting = $this->lesson->meetings()->create([
            'topic' => $meeting['topic'],
            'meeting_id' => trim(preg_replace('/\s+/', '', $meeting['id'])),
            // TODO: fix hours format
            'meeting_time' => Carbon::parse($this->meeting_date_start, $this->getTimezone()),
            // 'recurring_end_date' => Carbon::parse($this->meeting_date_end, $this->getTimezone()),
            'recurring_weekday' => $this->weekday,
            'recurring_end_times' => $this->recurrence_options['end_times'],
            'password' => $meeting['password'],
            'user_id' => Auth::user()->id
        ]);

        // if (Carbon::parse($meeting->meeting_time)->diffInDays(now()) <= 1) {
        //     Notification::send($meeting->lesson->users, new LessonStartSoon($meeting));
        // }

        session()->flash('message', 'Занятие запланировано.');
        $this->getMeetings();
    }

    public function formatDateForApi($date)
    {
        return \DateTime::createFromFormat($this->getMeetingDateFormatProperty(), $date, $this->getTimezone())->format('Y-m-d\TH:i:s');
    }

    public function formatWeekdaysForApi($weekday)
    {
        $weekdaysMapper = [
            'Вс' => '1',
            'Пн' => '2', 
            'Вт' => '3', 
            'Ср' => '4', 
            'Чт' => '5', 
            'Пт' => '6', 
            'Сб' => '7',
        ];
        return $weekdaysMapper[$weekday];
    }

    public function getFormatDateProperty()
    {
        return $this->meeting_date_start ? \DateTime::createFromFormat($this->getMeetingDateFormatProperty(), $this->meeting_date_start, $this->getTimezone())->format('Y-m-d\TH:i:s') : '';
    }

    public function getLabelProperty()
    {
        return GroupLesson::getLabel();
    }

    public function getMeetingDateFormatProperty()
    {
        return "d-m-Y H:i";
    }

    public function getTimezone()
    {
        return new \DateTimeZone('Europe/Kiev'); // old: 'UTC'
    }

    public function render()
    {
        return view('livewire.admin.group-lessons.schedule');
    }
}

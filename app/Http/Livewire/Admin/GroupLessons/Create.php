<?php

namespace App\Http\Livewire\Admin\GroupLessons;

use App\GroupLesson;
use App\User;
use Illuminate\Support\Facades\Auth;
use Livewire\Component;

class Create extends Component
{
    public $title;
    public $brief_desc;
    public $price;
    public $grade;
    public $message;
    public $individual = false;

    public function mount()
    {
    }

//    public function updatingEventDate($value)
//    {
//        dd(Carbon::parse($value)->format(Webinar::getDateTimeFormat()));
//        return date('d/m/y H:i', strtotime($value));
//    }

    public function submitForm()
    {
        $this->validate([
            'title' => 'required|max:100',
            'price' => 'required|numeric',
        ]);


        GroupLesson::create([
            'title' => $this->title,
            'price' =>  $this->price,
            'brief_desc' =>  $this->brief_desc,
            'grade' => $this->grade,
            'author_id' => Auth::user()->getKey(),
            'individual' => $this->individual,
            'published' => true,
        ]);

        $this->reset();

        session()->flash('message', 'Занятие создано.');
    }

    // public function getGradeOptionsProperty()
    // {
    //     return GroupLesson::getGradeOptions();
    // }

    public function getGradeOptionsProperty()
    {
        return User::getGradeList();
    }

    public function render()
    {
        return view('livewire.admin.group-lessons.create');
    }
}

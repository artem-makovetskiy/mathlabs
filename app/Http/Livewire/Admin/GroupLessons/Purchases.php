<?php

namespace App\Http\Livewire\Admin\GroupLessons;

use App\GroupLesson;
use App\GroupLessonPurchase;
use Livewire\Component;

class Purchases extends Component
{
    public $selected_items = [];
    public $sort_by = 'created_at';

    public function getGroupLessonPurchasesProperty()
    {
        switch ($this->sort_by) {
            case 'created_at':
                return GroupLessonPurchase::with('user', 'lesson')->whereNull('group_id')->orderByDesc($this->sort_by)->get();
                break;
            
            case 'lesson':
                return GroupLessonPurchase::with('user', 'lesson')->whereNull('group_id')->orderByDesc(
                                GroupLesson::select('title')
                                            ->whereColumn('group_lesson_id', 'group_lessons.id')
                                            ->orderBy('title', 'desc')
                                            ->limit(1)
                                )->get();
                break;
        }
    }

    public function addItem($item)
    {
        $items_collection = collect($this->selected_items);
        if ($items_collection->contains($item)) {
            $items_collection->forget($items_collection->search($item));
        } else {
            $items_collection->push($item);
        }
        $this->selected_items = $items_collection->toArray();
    }

    public function uncheckAll()
    {
        $this->selected_items = [];
    }

    public function createGroup()
    {
        // session(['new_group' => $this->selected_items]);
        session()->flash('new_group', $this->selected_items);
        // session(['new_group' => $this->selected_items]);
        return redirect()->route('admin.user-groups.create');
    }

    public function changeSortOrder($sort_by)
    {
        $this->sort_by = $sort_by;
    }

    public function render()
    {
        return view('livewire.admin.group-lessons.purchases');
    }
}

<?php

namespace App\Http\Livewire\Admin\GroupLessons;

use App\GroupLesson;
use App\User;
use Livewire\Component;

class Edit extends Component
{
    public $group_lesson_id;
    public $group_lesson;
    public $groupLessonData;

    public function mount($id)
    {
        $this->group_lesson_id = $id;
        $this->group_lesson = GroupLesson::findOrFail($id);
        $this->groupLessonData = $this->group_lesson->toArray();
//        $this->webinarData['author_id'] = Auth::user()->getKey();
    }

    public function updateLesson()
    {
        $this->validate([
            'groupLessonData.title' => 'required|max:100',
            'groupLessonData.price' => 'required|numeric',
        ]);

        $this->group_lesson->fill($this->groupLessonData);
        $this->group_lesson->save();

        session()->flash('message', 'Занятие успешно обновлено.');
    }

    public function destroyLesson()
    {
        $group_lesson_name = $this->group_lesson->name;
        $this->group_lesson->delete();
        session()->flash('message', "Занятие '$group_lesson_name' удалено.");
        return redirect()->route('admin.group-lessons.index');
    }

    // public function getGradeOptionsProperty()
    // {
    //     return GroupLesson::getGradeOptions();
    // }

    public function getGradeOptionsProperty()
    {
        return User::getGradeList();
    }

    public function render()
    {
        return view('livewire.admin.group-lessons.edit');
    }
}

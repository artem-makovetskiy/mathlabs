<?php

namespace App\Http\Livewire\Auth;

use App\Providers\RouteServiceProvider;
use App\QuizStatus;
use App\Role;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Livewire\Component;

class Register extends Component
{
    /** @var string */
    public $parent_name = '';

    /** @var string */
    public $child_name = '';

    /** @var string */
    public $email = '';

    /** @var string */
    public $phone_number = '';

    /** @var string */
    public $grade = '-';

    /** @var string */
    public $password = '';

    /** @var string */
    public $passwordConfirmation = '';

    public function register()
    {
        $this->validate([
            'parent_name' => ['required'],
            'child_name' => ['required', 'min:3'],
            'email' => ['required', 'email', 'unique:users'],
//            'password' => ['required', 'min:8', 'same:passwordConfirmation'],
            'password' => ['required', 'min:4'],
//            'phone_number' => ['required', 'regex:/^[+7][0-9]{9}[0-9]*$/i'],
            'phone_number' => ['required', 'regex:/^(\+7|7|8)?[\s\-]?\(?[489][0-9]{2}\)?[\s\-]?[0-9]{3}[\s\-]?[0-9]{2}[\s\-]?[0-9]{2}$/i'],
            'grade' => ['required'],
        ]);

        $user = User::create([
            'email' => $this->email,
            'parent_name' => $this->parent_name,
            'child_name' => $this->child_name,
            'phone_number' => $this->phone_number,
            'grade' => $this->grade,
            'password' => Hash::make($this->password),
        ]);

        $client_role = Role::where('slug','client')->first();
        $user->roles()->attach($client_role);

        QuizStatus::create([
            'current_step' => 1,
            'user_id' => $user->id,
        ]);

        $user->sendEmailVerificationNotification(); // TODO uncomment before uploading

        Auth::login($user, true);

        redirect(route('account.myLessons'));
    }

    public function render()
    {
        return view('livewire.auth.register');
    }
}

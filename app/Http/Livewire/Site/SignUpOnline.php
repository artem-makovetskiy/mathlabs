<?php

namespace App\Http\Livewire\Site;

use Livewire\Component;
use App\QuizStatus;
use App\Role;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class SignUpOnline extends Component
{
    /** @var string */
    public $parent_name = '';

    /** @var string */
    public $child_name = '';

    /** @var string */
    public $email = '';

    /** @var string */
    public $phone_number = '';

    /** @var string */
    public $grade = '8';

    /** @var string */
    public $password = '';

    /** @var string */
    public $passwordConfirmation = '';

    public function register()
    {
        $this->validate([
            'parent_name' => ['required'],
            'child_name' => ['required', 'min:3'],
            'email' => ['required', 'email', 'unique:users'],
            'password' => ['required', 'min:4'],
            'phone_number' => ['required', config('validation_rules.phone_number')],
            'grade' => ['required'],
        ]);

        $user = User::create([
            'email' => $this->email,
            'parent_name' => $this->parent_name,
            'child_name' => $this->child_name,
            'phone_number' => $this->phone_number,
            'grade' => $this->grade,
            'password' => Hash::make($this->password),
        ]);

        $client_role = Role::where('slug','client')->first();
        $user->roles()->attach($client_role);

        QuizStatus::create([
            'current_step' => 1,
            'user_id' => $user->id,
        ]);

        $user->sendEmailVerificationNotification(); // TODO uncomment before uploading

        Auth::login($user, true);

        redirect(route('account.myLessons'));
    }

    public function getGradesProperty()
    {
        return User::getGradeList();
    }
    
    public function render()
    {
        return view('livewire.site.sign-up-online');
    }
}

<?php

namespace App\Http\Livewire\Site;

use App\Mail\ReceiveConsultation;
use App\User;
use Illuminate\Support\Facades\Mail;
use Livewire\Component;

class ReceiveConsultationModal extends Component
{
    /** @var string */
    public $receive_consultation_name = '';

    /** @var string */
    public $receive_consultation_phone_number = '';

    /** @var string */
    public $receive_consultation_grade = '8';

    public $receive_consultation_show_success_msg = false;

    public function submitForm()
    {
        $this->validate([
            'receive_consultation_name' => ['required'],
            'receive_consultation_phone_number' => ['required', 'regex:/^(\+7|7|8)?[\s\-]?\(?[489][0-9]{2}\)?[\s\-]?[0-9]{3}[\s\-]?[0-9]{2}[\s\-]?[0-9]{2}$/i'],
            'receive_consultation_grade' => ['required'],
        ]);

        Mail::to(env('ADMIN_EMAIL'))->send(new ReceiveConsultation($this->receive_consultation_name, $this->receive_consultation_phone_number, $this->receive_consultation_grade));

        $this->receive_consultation_show_success_msg = true;

        // $this->reset();

        // $user->sendEmailVerificationNotification(); // TODO uncomment before uploading

        // redirect(route('account.myLessons'));
    }

    public function resetForm()
    {
        $this->reset();
    }

    public function getGradesProperty()
    {
        return User::getGradeList();
    }

    public function render()
    {
        return view('livewire.site.receive-consultation-modal');
    }
}

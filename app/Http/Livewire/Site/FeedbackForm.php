<?php

namespace App\Http\Livewire\Site;

use App\Mail\SendFeedback;
use Illuminate\Support\Facades\Mail;
use Livewire\Component;

class FeedbackForm extends Component
{
    /** @var string */
    public $name = '';

    /** @var string */
    public $email = '';

    /** @var string */
    public $message = '';

    public function submitForm()
    {
        $this->validate([
            'name' => ['required'],
            'email' => ['required', 'email', 'unique:users'],
            'message' => ['required', 'string', 'max:255'],
        ]);

        Mail::to(env('ADMIN_EMAIL'))->send(new SendFeedback($this->name, $this->email, $this->message));

        $this->resetForm();
    }

    public function resetForm()
    {
        $this->reset();
    }

    public function render()
    {
        return view('livewire.site.feedback-form');
    }
}

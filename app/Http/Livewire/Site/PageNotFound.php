<?php

namespace App\Http\Livewire\Site;

use Livewire\Component;

class PageNotFound extends Component
{
    public function render()
    {
        return view('livewire.site.page-not-found');
    }
}

<?php

namespace App\Http\Livewire\Site;

use Livewire\Component;
use App\QuizStatus;
use App\Role;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class AuthModal extends Component
{
    public $active_panel = 'login';

    /* Register params */
    /** @var string */
    public $parent_name = '';

    /** @var string */
    public $child_name = '';

    /** @var string */
    public $email = '';

    /** @var string */
    public $phone_number = '';

    /** @var string */
    public $grade = '8';

    /** @var string */
    public $password = '';

    /** @var string */
    public $passwordConfirmation = '';

    /* Login params */
    /** @var string */
    public $login_email = '';

    /** @var string */
    public $login_password = '';

    /** @var bool */
    public $login_remember = false;

    // public $processing = false;

    // protected $listeners = ['authenticate' => 'showLoadingState'];

    public function register()
    {
        $this->validate([
            'parent_name' => ['required'],
            'child_name' => ['required', 'min:3'],
            'email' => ['required', 'email', 'unique:users'],
//            'password' => ['required', 'min:8', 'same:passwordConfirmation'],
            'password' => ['required', 'min:4'],
//            'phone_number' => ['required', 'regex:/^[+7][0-9]{9}[0-9]*$/i'],
            'phone_number' => ['required', config('validation_rules.phone_number')],
            'grade' => ['required'],
        ]);

        $user = User::create([
            'email' => $this->email,
            'parent_name' => $this->parent_name,
            'child_name' => $this->child_name,
            'phone_number' => $this->phone_number,
            'grade' => $this->grade,
            'password' => Hash::make($this->password),
        ]);

        $client_role = Role::where('slug','client')->first();
        $user->roles()->attach($client_role);

        QuizStatus::create([
            'current_step' => 1,
            'user_id' => $user->id,
        ]);

        $user->sendEmailVerificationNotification(); // TODO uncomment before uploading

        Auth::login($user, true);

        session()->flash('show_welcome_msg', true);

        redirect(route('account.myLessons'));
    }

    public function authenticate()
    {
        // $this->emit('authenticate');
        // $this->processing = true;

        $credentials = $this->validate([
            'login_email' => ['required', 'email'],
            'login_password' => ['required'],
        ]);

        // try {
        //     $credentials = $this->validate([
        //         'login_email' => ['required', 'email'],
        //         'login_password' => ['required'],
        //     ]);
        // } catch (\Exception $ex) {
        //     $this->processing = false;
        //     // ValidationException
        //     // dd($ex);
        // }
        
        if (!Auth::attempt(['email' => $credentials['login_email'], 'password' => $credentials['login_password']], $this->login_remember)) {
            $this->addError('login_email', trans('auth.failed'));

            return;
        }

        if (auth()->user()->hasRole('admin')) {
            redirect(route('admin.group-lessons.index'));
        } else {
            redirect(route('account.myLessons'));
        }
    }

    // public function updated($name, $value)
    // {
    //     $this->processing = false;
    // }

    public function showLoadingState()
    {
        $this->processing = true;
    }

    public function setActivePanel($panel_name)
    {
        $this->active_panel = $panel_name;
    }

    public function getGradesProperty()
    {
        return User::getGradeList();
    }

    public function render()
    {
        return view('livewire.site.auth-modal');
    }
}

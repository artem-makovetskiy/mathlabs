<?php

namespace App\Http\Livewire\Site;

use App\Mail\ReceiveConsultation;
use Illuminate\Support\Facades\Mail;
use Livewire\Component;

class Home extends Component
{
    /** @var string */
    public $intro_form_name = '-';

    /** @var string */
    public $intro_form_grade = '';

    /** @var string */
    public $intro_form_phone_number = '';

    public $intro_form_show_success_msg = false;

    public function submitIntroForm()
    {
        $this->validate([
            'intro_form_grade' => 'required',
            'intro_form_phone_number' => ['required', config('validation_rules.phone_number')],
        ], [], ['intro_form_phone_number' => '"телефон"', 'intro_form_grade' => '"класс"']);

        Mail::to(env('ADMIN_EMAIL'))->send(new ReceiveConsultation($this->intro_form_name, $this->intro_form_phone_number, $this->intro_form_grade));

        $this->reset();

        $this->intro_form_show_success_msg = true;
    }

    public function render()
    {
        return view('livewire.site.home');
    }
}

<?php

namespace App\Http\Middleware;

use Closure;

class RoleMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $roles)
    {
        if(!auth()->user()->hasRole($roles)) {
            abort(403);
        }

//        if($permission !== null && !auth()->user()->can($permission)) {
//            abort(404);
//        }
        return $next($request);
    }
}

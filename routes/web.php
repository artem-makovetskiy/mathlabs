<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::view('/', 'welcome')->name('home');
// Route::redirect('/', 'login');



Route::middleware('guest')->group(function () {
    Route::livewire('/', 'site.home')->layout('layouts.site')->name('site.home');
    Route::livewire('/sign-up-online', 'site.sign-up-online')->section('body')->layout('layouts.base')->name('site.sign-up-online');
    // Route::view('login', 'auth.login')->name('login');
    // Route::view('register', 'auth.register')->name('register');
});

Route::view('password/reset', 'auth.passwords.email')->name('password.request');
Route::get('password/reset/{token}', 'Auth\PasswordResetController')->name('password.reset');

Route::post('/payment-confirmation', 'AccountController@paymentConfirmation')->name('payment-confirmation');

Route::middleware('auth')->group(function () {
    Route::livewire('/account/my-classes', 'account.my-lessons')->layout('layouts.account')->name('account.myLessons');
    Route::livewire('/account/all-classes', 'account.all-lessons')->layout('layouts.account')->name('account.allLessons');
    Route::livewire('/account/{type}/{uuid}', 'account.lesson-details')->layout('layouts.account')->name('account.lessonDetails');
    Route::livewire('/account/{type}/{uuid}/watch', 'account.watch-lesson')->layout('layouts.account')->name('account.watchLesson');
    Route::livewire('/account/{type}/{uuid}/join/{meeting_number}', 'account.stream-lesson')->layout('layouts.stream-room')->name('account.streamLesson');

    Route::livewire('/account/buy-classes', 'account.pay-for-classes')->layout('layouts.account')->name('account.buyLessons');
    Route::livewire('/account/settings', 'account.settings')->layout('layouts.account')->name('account.settings');
    Route::livewire('/account/notifications', 'account.notifications')->layout('layouts.account')->name('account.notifications');
    Route::livewire('/account/quiz', 'account.quiz')->layout('layouts.account')->name('account.quiz');


    Route::view('email/verify', 'auth.verify')->middleware('throttle:6,1')->name('verification.notice');
    Route::get('email/verify/{id}/{hash}', 'Auth\EmailVerificationController')->middleware('signed')->name('verification.verify');

    Route::post('logout', 'Auth\LogoutController')->name('logout');

    Route::view('password/confirm', 'auth.passwords.confirm')->name('password.confirm');
});

Route::middleware(['auth', 'role:admin'])->group(function () {

    // Group lessons
    Route::livewire('/admin/group-lessons', 'admin.group-lessons.index')->layout('layouts.account')->name('admin.group-lessons.index');
    Route::livewire('/admin/group-lessons/create', 'admin.group-lessons.create')->layout('layouts.account')->name('admin.group-lessons.create');
    Route::livewire('/admin/group-lessons/{id}/edit', 'admin.group-lessons.edit')->layout('layouts.account')->name('admin.group-lessons.edit');
    // Route::livewire('/admin/group-lessons/{id}/schedule', 'admin.group-lessons.schedule')->layout('layouts.account')->name('admin.group-lessons.schedule');
    Route::livewire('/admin/group-lessons/purchases', 'admin.group-lessons.purchases')->layout('layouts.account')->name('admin.group-lessons.purchases');
    // Groups
    Route::livewire('/admin/user-groups', 'admin.user-groups.index')->layout('layouts.account')->name('admin.user-groups.index');
    Route::livewire('/admin/user-groups/create', 'admin.user-groups.create')->layout('layouts.account')->name('admin.user-groups.create');
    Route::livewire('/admin/user-groups/{id}/edit', 'admin.user-groups.edit')->layout('layouts.account')->name('admin.user-groups.edit');
    // Webinars
    Route::livewire('/admin/webinars', 'admin.webinars.index')->layout('layouts.account')->name('admin.webinars.index');
    Route::livewire('/admin/webinars/create', 'admin.webinars.create')->layout('layouts.account')->name('admin.webinars.create');
    Route::livewire('/admin/webinars/{id}/edit', 'admin.webinars.edit')->layout('layouts.account')->name('admin.webinars.edit');
    Route::livewire('/admin/webinars/{id}/schedule', 'admin.webinars.schedule')->layout('layouts.account')->name('admin.webinars.schedule');
    //    Route::livewire('/admin/webinars/{id}/stream', 'admin.webinars.stream')->layout('layouts.account')->name('admin.webinars.stream');
//    Route::livewire('/admin/webinars/{id}/join/{meeting_number}', 'admin.webinars.stream')->layout('layouts.stream-room')->name('admin.webinars.stream');

    Route::livewire('/admin/questions', 'admin.questions.index')->layout('layouts.account')->name('admin.questions.index');
    Route::livewire('/admin/questions/create', 'admin.questions.create')->layout('layouts.account')->name('admin.questions.create');
    Route::livewire('/admin/questions/{id}/edit', 'admin.questions.edit')->layout('layouts.account')->name('admin.questions.edit');

});